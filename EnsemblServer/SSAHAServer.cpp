
// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : SSAHAServer
// File Name    : SSAHAServer.cpp
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Description:

// Includes:

#include "ClientServerUtils.h"
#include "SSAHAServer.h"
#include "HashTableGeneric.h"
#include "SequenceReaderLocal.h"
#include "QueryManager.h"
#include "MatchStoreUngapped.h"
#include <string.h>
#include <string>
#include <iomanip>
#include <sys/resource.h>
#include <cmath>

// ### Function Definitions ###

static Handshake hello;
static pid_t myPID;
static HashTableGeneric* pHashTable = NULL; 
static SourceReaderIndex* pSourceReader = NULL;
static double expectedNumHits;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
// static pthread_rwlock_t rwlock;

typedef MatchTaskSort
<SortByMatch>
SortByMatchType;

//typedef MatchTaskSort
//<CombineSort<SortByMatchSize,SortByDiff,SortByQueryStart> >
//SortByMatchType;

typedef MatchTaskSort
<CombineSort<SortByPercentMatch,SortByDiff,SortByQueryStart> >
SortByPercentType;

enum ServerModeType { eFork, eThreads, eSingle };
enum { tooLargeNameFileSize=100000000 }; // TBD change to 100 million

void resourceUsage( void )
{
  static struct rusage usage1, usage2;
  static rusage* pUsage(&usage1);

  getrusage (RUSAGE_SELF, pUsage);


  //  printf("%d: %d.%03du %d.%03ds %d+%dk %dpf %dsw\n",
  //	 myPID,
  // usage2.ru_utime.tv_sec,
  // usage2.ru_utime.tv_usec/1000,
  // usage2.ru_stime.tv_sec,
  // usage2.ru_stime.tv_usec/1000,
  // usage2.ru_ixrss,
  // usage2.ru_idrss + usage2.ru_isrss,
  // usage2.ru_majflt,
  // usage2.ru_nswap);

  if (pUsage==&usage2)
  {
    cout << myPID << ": CPU microseconds used: \t"
         <<   ((usage2.ru_utime.tv_sec * 1000000) + usage2.ru_utime.tv_usec)
      - ((usage1.ru_utime.tv_sec * 1000000) + usage1.ru_utime.tv_usec)
         << " (user) \t"
         <<   ((usage2.ru_stime.tv_sec * 1000000) + usage2.ru_stime.tv_usec)
      - ((usage1.ru_stime.tv_sec * 1000000) + usage1.ru_stime.tv_usec)
         << " (system)" << endl;
    pUsage=&usage1;
    
  }
 else pUsage=&usage2;


} // ~void resourceUsage( void )


void sig_chld( int signo )
{
  pid_t	pid;
  int		stat;
  
  while ( (pid = waitpid(-1, &stat, WNOHANG)) > 0)
    cout << myPID << ": terminated child " << pid << endl;
    //    printf("child %d terminated\n", pid);
  return;
}

void sig_pipe( int signo )
{
  cout << myPID << ": broken connection" << endl;
  //  throw BrokenSocketException();
}





class MatchTaskServer : public MatchTask
{
public:
  MatchTaskServer( void ) : sendSequence_(false) {} // SocketInterface&
  virtual void operator()(MatchStore& store);
  void sendMatches( SocketInterface& socket );
  void sendSequence( bool yesOrNo=true ) { sendSequence_ = yesOrNo; }
private:
  vector<MatchInfo> matches_;
  map<SequenceNumber,std::string> names_;
  bool sendSequence_;
};

void MatchTaskServer::operator()(MatchStore& store)
{

 for (MatchStore::iterator i( store.begin() ) ; i!=store.end() ; ++i )
 {
    matches_.push_back( MatchInfo( **i ) );
    names_.insert
    ( pair<SequenceNumber, string>
      ( (*i)->getSubjectNum(), (*i)->getSubjectName() ) );
 }
}

void MatchTaskServer::sendMatches( SocketInterface& socket )
{

  MatchHeader header;
  header.wasSuccessful=true; // TBD enum with more descriptive stuff
  header.numSubjectNames=names_.size();
  header.numMatches=matches_.size();
  cout << myPID << ": sending " << header.numMatches << " matches among "
       << header.numSubjectNames << " subject sequences." << endl; 
  
  socket.sendStruct(&header);

  char endString('\0');
  char* pSource;

  // Send the subject nums and names of all database sequences
  // which matched the query
  for ( map<SequenceNumber,std::string>::iterator i(names_.begin()); i != names_.end() ; i++  ) {
    cout << myPID << ": " << i->first << " " << i->second << endl;
    socket.sendStruct(&i->first);
    socket.sendString(i->second);
  } 

  // Send the match info itself
  for ( vector<MatchInfo>::iterator i( matches_.begin()); i != matches_.end() ; ++i ) {  
    
    socket.sendStruct((MatchInfo*)&(*i));
    
    if (sendSequence_==true) {
      //      cout << myPID << ": getting ready to send sequence " 
      //   << i->subjectNum << " " 
      //   << i->subjectStart << " " 
      //   << i->subjectEnd << endl;
      if(pSourceReader!=NULL) {
	pSourceReader->extractSource ( &pSource, i->subjectNum, i->subjectStart, i->subjectEnd );
	socket.sendChars( pSource, i->subjectEnd-i->subjectStart+1 );
	//cout << myPID << ": sent " 
	//   << i->subjectEnd-i->subjectStart+1 << " chars, "
	//   << pSource[0]
	//   << pSource[i->subjectEnd-i->subjectStart] << endl;
      } else {
	// send a null string if no index info is available
	socket.sendStruct(&endString);
	//	cout << myPID << ": sent null string" << endl; 
      }
    
    } 
  
  } 

}


void processQuery(int sockfd, HashTableGeneric& hashTable, const string& myPID)
{

  SocketInterface socket(sockfd,60);
  socket.sendStruct(&hello);

  //  cout << myPID << ": sent handshake " << sizeof(hello) << endl;
 
  QueryHeader qinfo;
  socket.receiveStruct(&qinfo);

  //  cout << myPID << ": received query header" << endl;

  cout << myPID << ": Client request: " << qinfo << endl;

  MatchHeader rejectQuery;
  rejectQuery.wasSuccessful=false; // TBD enum with more descriptive stuff
  rejectQuery.numSubjectNames=0;
  rejectQuery.numMatches=0;

  if( qinfo.numQueryWords>hello.maxBufferSize )
  {
    cout << myPID << ": Rejecting client request (too large)." << endl;
    socket.sendStruct(&rejectQuery);
    return;
  }

  int incomingWordLength;
  
  if( qinfo.bitsPerSymbol == hashTable.getBitsPerSymbol() )
  {
    incomingWordLength = hashTable.getWordLength();
  } 
  else if (    (qinfo.bitsPerSymbol==gBaseBits)
	    && (hashTable.getBitsPerSymbol()==gResidueBits) )
  {
    incomingWordLength = gMaxBasesPerWord;
  } // ~else if
  else
  {
    cout << myPID << ": Rejecting client request (can't accept queries with "
	 << qinfo.bitsPerSymbol << " bits per symbol)." << endl;
    socket.sendStruct(&rejectQuery);
    return;
  } // ~if

  cout << myPID << ": Expecting data with " << qinfo.bitsPerSymbol 
     << " bits per symbol, " << incomingWordLength 
     << " symbols per word." << endl;

  SequenceHeader sinfo;

  SequenceReaderLocal querySeqs
    ( incomingWordLength, qinfo.bitsPerSymbol, cerr );

  for ( int i(0) ; i < qinfo.numQuerySeqs ; i++ )
  {
    querySeqs.push_back();
    socket.receiveSequence(querySeqs.back().first);
    // TBD check we're not exceeding our limit
    //  cout << printWord(query.back(), hello.wordLength) << endl;
  }


  socket.checkSocketEmpty();
  //  cout << myPID << querySeqs.getNumSequencesInFile() << endl;

  // Now do the match
  
  // TBD next line is not thread safe!!! - move inside mutex!
  //  hashTable.setMaxNumHits
  //  (
  //   (qinfo.clipThreshold==0) 
  //   ? (1<<30) 
  //   : 1+(int)(qinfo.clipThreshold * expectedNumHits)	       
  //  );

  QueryManager queryManager( querySeqs,hashTable,cerr);
  //  MatchTaskPrint print(cout);

  MatchAlgorithm* pMatcher(NULL);

   if ( (qinfo.maxGap!=0)||(qinfo.maxInsert!=0) )
  {
    pMatcher = new MatchAlgorithmGapped
      ( qinfo.maxGap, qinfo.maxInsert, qinfo.minPrint, qinfo.numRepeats );
  } // ~if
  else
  {
    //    pMatcher = new MatchAlgorithmUngapped
    //   ( qinfo.minPrint, qinfo.numRepeats );
    pMatcher = new MatchAlgorithmGapped
      ( 0, 0, qinfo.minPrint, qinfo.numRepeats );
  } // ~else

  MatchTask* pTask(NULL);

  MatchTaskServer sendToClient;

  SortByMatchType sortByMatch(qinfo.maxMatches,0.25);
  SortByPercentType sortByPercent(qinfo.maxMatches,0.25);

  if (    (qinfo.maxMatches==0)
       || (    (qinfo.sortMode!=eSortByMatchLength)
	    && (qinfo.sortMode!=eSortByPercentMatch)
	    && (qinfo.sortMode!=eSortAndReturnSequence)  )  )
  {
    pTask = &sendToClient;
  }
  else if (qinfo.sortMode==eSortByMatchLength)
  {
    pTask=new CombineTask<SortByMatchType,MatchTaskServer>
      ( sortByMatch,sendToClient );
  }
  else if (qinfo.sortMode==eSortByPercentMatch)
  {
    pTask=new CombineTask<SortByPercentType,MatchTaskServer>
      ( sortByPercent,sendToClient );
  }
  else if (qinfo.sortMode==eSortAndReturnSequence)
  {
    pTask=new CombineTask<SortByMatchType,MatchTaskServer>
      ( sortByMatch,sendToClient );
    sendToClient.sendSequence(true);
  }

  assert(pTask!=NULL);


  //  CombineTask<MatchTaskSort<SortByMatchSize>,MatchTaskServer>
  //    task( sort, sendToClient );
  //  MatchTaskServer task;


  //  cout << myPID << ": awaiting mutex lock" << endl;

  if (pthread_mutex_lock(&mutex)!=0)
    throw SSAHAException("Couldn't lock mutex!!");

  //  if (pthread_rwlock_rdlock(&rwlock)!=0)
  //   throw SSAHAException("Couldn't acquire read write lock!!");

  //  cout << myPID << ": got mutex lock" << endl;

  hashTable.setMaxNumHits
  (
   (qinfo.clipThreshold==0) 
   ? (1<<30) 
   : 1+(int)(qinfo.clipThreshold * expectedNumHits)	       
  );

  hashTable.setSubstituteThreshold
  (
   (qinfo.substituteThreshold==0)
   ? 0
   : 1+(int)(qinfo.substituteThreshold * expectedNumHits)
  );



  queryManager.doQuery( *pMatcher, *pTask ); 

  if (pthread_mutex_unlock(&mutex)!=0)
    throw SSAHAException("Couldn't unlock mutex!!");

  //  if (pthread_rwlock_unlock(&rwlock)!=0)
  //   throw SSAHAException("Couldn't drop read write lock!!");

  //  cout << myPID << ": relinquished mutex lock" << endl;

  sendToClient.sendMatches(socket);

  if (pTask!=&sendToClient) delete pTask;

  delete pMatcher;

}

HashTableGeneric* generateHashTable
(const string& currentHashTableName, string& controlFileName )
{

  cout << myPID << ": attempting to read in hash table " 
       << currentHashTableName << endl;

   // create new source reader from index files
   {
     delete pSourceReader;

     ifstream filesFile( (currentHashTableName+(string)".files").c_str() );
     ifstream indexFile( (currentHashTableName+(string)".index").c_str() );
     if ((!filesFile.fail())&&(!indexFile.fail()))
     {
       pSourceReader = new SourceReaderIndex(currentHashTableName);
       cout << myPID 
	    << ": found a sequence source index for this hash table."
	    << endl;
     } // ~if  
     else
     {
       pSourceReader = NULL;
       cout << myPID 
	    << ": did not find a sequence source index for this hash table."
	    << endl;
     } // ~else
     filesFile.close();
     indexFile.close();
   } // ~scope of ifstreams

   HashTableFactory creator(cerr);

   HashTableGeneric* 
     pHashTable; // overrides global definition, bit naughty

   if (pSourceReader==NULL)
   {
     pHashTable = creator.loadHashTable(currentHashTableName); 
     // will throw if not successful
   } // ~if
   else
   {
     ifstream nameFile( (currentHashTableName+(string)".name").c_str() );
     nameFile.seekg(0, ios::end);
     if (nameFile.tellg()>tooLargeNameFileSize) // TBD change to 100 million
     {
       cout << myPID 
	    << ": sequence name file exceeds " << tooLargeNameFileSize
	    << " bytes, will not be stored locally"
	    << endl;
       pHashTable = creator.loadHashTable(currentHashTableName, pSourceReader); 
     } // ~if
     else
     {
       pHashTable = creator.loadHashTable(currentHashTableName); 
     } // ~else
   } // ~if




   pHashTable->setMaxNumHits(100000000);

   hello.wordLength = pHashTable->getWordLength();
   
   if ( pHashTable->getBitsPerSymbol() == gResidueBits )
   {
     hello.tableType = 
       ( ( pHashTable->getSourceDataType() == gDNAData )
	 ? e5bitTranslatedDNA
	 : e5bitProtein );
   } // ~if
   else hello.tableType = e2bitDNA;
     
   // hello.bitsPerSymbol = pHashTable->getBitsPerSymbol();

   cout << myPID << ": Hash table built using " 
	<< ((hello.tableType==e2bitDNA)?gBaseBits:gResidueBits)
	<< " bits per symbol, " << hello.wordLength 
	<< " symbols per word." << endl;

   cout << myPID << ": Hits are stored using " << 
     ((pHashTable->getHitListFormat()==gStandard)
      ?(string)"standard (64 bit per hit)"
      :(string)"compressed (32 bit per hit)")
	<< " format." << endl;

   expectedNumHits = pHashTable->getTotalNumWords();

   if (    (pHashTable->getHitListFormat()==gTranslated)
	   || (pHashTable->getHitListFormat()==g32BitPackedProtein) )
   {
     expectedNumHits 
       /= pow((double)gNumCodonEncodings,(int)pHashTable->getWordLength());
   } // ~if
   else
   {
     assert(pHashTable->getBitsPerSymbol()==gBaseBits);
     expectedNumHits /= 1<<(2*pHashTable->getWordLength());
   } // ~else


   //   expectedNumHits /= (hello.bitsPerSymbol==gBaseBits)
   // ? 1<<(2*hello.wordLength)
   // : pow(gNumCodonEncodings,hello.wordLength);

   cout << setprecision(8) << setiosflags(ios::fixed);

   cout << myPID << ": would expect " << expectedNumHits 
	 << " hits per word for a random database of this size." << endl;

   if (!controlFileName.empty())
   {
     cout << myPID << ": writing current hash table name to control file "
	   << controlFileName << endl;
     ofstream controlFile(controlFileName.c_str());
     controlFile << currentHashTableName;
     if (controlFile.fail())
     {
       cout << myPID 
	    << ": problem writing to control file, reverting to default mode."
	    << endl;
       controlFileName="";
     } // ~if
     controlFile.close();
   } // ~if

   return pHashTable;

} // ~generateHashTable

// Wrappers for thread functions

void Pthread_create
(pthread_t *tid, const pthread_attr_t *attr, void * (*func)(void *), void *arg)
{
  int n;
  if ( (n = pthread_create(tid, attr, func, arg)) == 0)
    return;
  errno = n;
  throw NetworkException("pthread_create error");
}

void Pthread_join(pthread_t tid, void **status)
{
  int		n;

  if ( (n = pthread_join(tid, status)) == 0)
    return;
  errno = n;
  throw NetworkException("pthread_join error");
}

void Pthread_detach(pthread_t tid)
{
  int		n;
  
  if ( (n = pthread_detach(tid)) == 0)
    return;
  errno = n;
  throw NetworkException("pthread_detach error");
}

void Pthread_kill(pthread_t tid, int signo)
{
  int		n;
  
  if ( (n = pthread_kill(tid, signo)) == 0)
    return;
  errno = n;
  throw NetworkException("pthread_kill error");
}


void* threadWrapper( void* arg )
{
  int connfd;
  connfd = *((int*)arg); 
  delete (int*) arg;
  pthread_t myThreadNum(pthread_self());
  Pthread_detach(myThreadNum);

  char buf[100];
  sprintf(buf,"%d/%p",myPID,myThreadNum);
  string myPIDstring(buf);

  //  cout << "Connection ID: " << connfd << endl;
  try
  {
    processQuery(connfd, *pHashTable, myPIDstring);
  }
  catch (const BrokenSocketException& err )
  {
    cout 
      << myPIDstring 
      << ": Caught BrokenSocketException in thread, returning without closing"
      << endl;
    return NULL;
  }
  catch (const NetworkException& err )
  {
    cout << myPIDstring << ": Caught NetworkException in thread: " 
	 << err.what() << "\n";
  }
  catch (const SSAHAException& err )
  {
    cout << myPIDstring << ": Caught SSAHA exception in thread: " 
	 << err.what() << "\n";
  }
  catch (const std::exception& err )
  {
    cout << myPIDstring << ": Caught exception in thread: " 
	 << err.what() << "\n";
  }

  Close(connfd);
  cout << myPIDstring << ": Exiting thread" << endl;;
  return NULL;
} // ~threadWrapper



int main( int numArgs, char* args[] )
{

  

  try
  {

    string controlFileName("");
    //    bool   doFork(true);
    ServerModeType serverMode(eFork);
    string modeString("fork");

    ofstream* pFile(NULL);

    myPID = getpid();
    char buf[100];
    sprintf(buf,"%d",myPID);
    string myPIDstring(buf);
    
    hello.ssahaversion = SERV_VERSION;
    
    // next bit only used in thread mode
    pthread_attr_t threadAttribs;
    size_t threadStackSize;
     
    if ( (numArgs>=4) && (numArgs<=7) )
    {
      if (numArgs==7)
      {
	modeString = args[6];
	if (modeString=="single")
	{
	  serverMode = eSingle;
	}
    	else if (modeString=="threads")
	{
	  serverMode = eThreads;
   	  if ( pthread_attr_init(&threadAttribs) != 0 )
	    throw SSAHAException("Problem setting thread attributes!");
	  
	  //	  if ( pthread_attr_getstacksize
	  //      (&threadAttribs, &threadStackSize) != 0 )
	  //   throw SSAHAException("Problem getting stacksize!");
	  
	  //	  cout << "got stack size of " << threadStackSize << endl;

	  //	  threadStackSize *= 4;

	  //	  if ( pthread_attr_setstacksize
	  //	       (&threadAttribs, threadStackSize) != 0 )
	  //   throw SSAHAException("Problem setting stacksize!");
	  
	  //	  if ( pthread_attr_getstacksize
	  //       (&threadAttribs, &threadStackSize) != 0 )
	  //    throw SSAHAException("Problem getting stacksize!");
	  
	  //	  cout << "got stack size of " << threadStackSize << endl;

	  //	  if ( pthread_rwlock_init( &rwlock, NULL ) != 0 )
	  //    throw SSAHAException("Could not initialize read/write lock!");


	}
	else if (modeString!="fork")
        {
	  cout << "syntax: " << args[0] 
	       << " hashTableFileName portNumber maxQueryWordsAllowed"
	       << " [logFileName|noLogFile]"
	       << " [controlFileName|noControlFile]" 
	       << " [fork|threads|single]" << endl;
	  throw SSAHAException("Invalid command line input to server");
	} // ~else
      }
      if (numArgs>=6)
      {
	controlFileName = std::string(args[5]);
	if (controlFileName=="noControlFile") controlFileName="";
      }
      if ((numArgs>=5)&&((string)args[4]!="noLogFile"))
      {
	pFile= new ofstream(args[4]);
	cout.rdbuf(pFile->rdbuf());
      }
    } // ~if
    else
    {
      cout << "syntax: " << args[0] 
	       << " hashTableFileName portNumber maxQueryWordsAllowed"
	       << " [logFileName|noLogFile]"
	       << " [controlFileName|noControlFile]" 
	       << " [fork|threads|single]" << endl;
      throw SSAHAException("Invalid command line input to server");
    }

    // next 2 lines `switch off' standard error
    NullBuffer db;
    cerr.rdbuf(&db);

    cout << myPID << ": Master process started " << getTimeNow();

    string currentHashTableName(args[1]);
    //    hashTable.loadHashTable((string)args[1]); 

    cout << myPID << ": Server will operate in mode " << modeString << endl;
	 


    int portNumber(atoi(args[2]));

    cout << myPID << ": Queries will be accepted via port number " 
	 << portNumber << "." << endl;

    hello.maxBufferSize = atoi(args[3]);

    cout << myPID << ": Any queries containing more than " 
	 << hello.maxBufferSize << " total words will be rejected." << endl;

    pHashTable 
      = generateHashTable(currentHashTableName, controlFileName);
    // generateHashTable now also populates pSourceReader TC 24.1.2

    
    //    int loopNum(0), checkControlInterval(10);

    int       listenfd, connfd;
    pid_t     childpid;
    socklen_t clilen;
    struct sockaddr_in	cliaddr, servaddr;
    void      sig_chld(int);
    void      sig_pipe(int);

    // next 2 vars only used in thread mode
    int* iptr;
    pthread_t tid;

    listenfd = Socket(AF_INET, SOCK_STREAM, 0);
    
    //	bzero(&servaddr, sizeof(servaddr));
    memset((void*)&servaddr, 0, sizeof(servaddr));
    
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(portNumber);
    
    Bind(listenfd, (SA *) &servaddr, sizeof(servaddr));
    
    Listen(listenfd, LISTENQ);
    
    Signal(SIGCHLD, sig_chld); // must call waitpid() 
    Signal(SIGPIPE, sig_pipe); // must call waitpid() 
    
    for ( ; ; ) 
    {

      clilen = sizeof(cliaddr);
      if ((connfd = accept(listenfd, (SA *) &cliaddr, &clilen))< 0)
      {
	if (errno == EINTR)
	  continue; // back to for() 
	else
	  err_sys("accept error");
      }
	  

      if (!controlFileName.empty())
      {
	  ifstream controlFile( controlFileName.c_str() );
	  string newHashTableName("");

	  if (controlFile.fail())
	  {
	    cout << myPID 
		 << ": could not read control file,"
		 << " assume reread of existing table is required"
		 << endl; 
	    newHashTableName = currentHashTableName;
	  } // ~if
	  else
	  {
	    controlFile >> newHashTableName;
	    controlFile.close();
	    //	    cout << myPID << ": " 
	    // << currentHashTableName << " " 
	    // << currentHashTableName.size() << " " 
	    // << newHashTableName << " " 
	    // << newHashTableName.size() << " " 
	    // << ((currentHashTableName==newHashTableName)?'T':'F') 
	    // << endl;


	    if (newHashTableName!=currentHashTableName)
	    {
	      cout << myPID 
		   << ": read new hash table name " << newHashTableName 
		   << " from control file." << endl;
	    } // ~if
	    else newHashTableName=""; // if same as old name don't reload
	  } // ~else

	  if (!newHashTableName.empty())
	  {
	    delete pHashTable;
	    currentHashTableName=newHashTableName;
	    pHashTable=generateHashTable
	      (currentHashTableName, controlFileName);
	  } // ~if

      } // ~if (!controlFileName

      if (serverMode==eFork)
      {
	if ( (childpid = Fork()) == 0) 
	  { // child process
	    Close(listenfd); // close listening socket 
	    myPID = getpid();
	    cout << myPID << ": child process started " << getTimeNow();
	    resourceUsage();
	    processQuery(connfd,*pHashTable,myPIDstring);	//
	    resourceUsage();
	    cout << myPID << ": child process ended " << getTimeNow();
	    exit(0);
	  }
	try
	{
	  Close(connfd); // parent closes connected socket 
	} // ~try
	catch (const NetworkException& err )
	{
	  cout << myPID 
	       << ": Caught NetworkException while closing connection: " 
	       << err.what() << endl;
	}


      } // ~if
      else if (serverMode==eThreads)
      {
	iptr= new int;
	*iptr=connfd;

	int numTries(0);
	
	while(1)
	{
	  if (pthread_create(&tid, &threadAttribs, 
			     &threadWrapper, iptr )==0) break;
	  sleep(30);
	  cout << myPID 
	       << ": failed to create thread, sleep 30 then try again"
	       << endl;
	  if (++numTries==10) 
	    throw NetworkException ("Could not create thread");
	} // ~while


      } // ~else if
      else // single process mode
      {
	cout << myPID << ": query started " << getTimeNow();
	resourceUsage();
	try
	{
	  processQuery(connfd,*pHashTable,myPIDstring);	//
	}
	catch(const BrokenSocketException& err)
	{
	  cout << myPID << ": caught BrokenSocketException, continuing"
	       << endl;
	}

	resourceUsage();
	cout << myPID << ": query ended " << getTimeNow();
      } // ~else

    } // ~for ( ; ; )

  } // ~try
  catch (const NetworkException& err )
  {
    cout << myPID << ": Caught NetworkException: " << err.what() << endl;
    exit(1);
  } // ~catch
  catch (const SSAHAException& err )
  {
    cout << myPID <<": Caught SSAHA exception: " << err.what() << endl;
    exit(1);
  } // ~catch
  catch (const std::exception& err )
  {
    cout << myPID << ": Caught exception: " << err.what() << endl;
    exit(1);
  } // ~catch



}


// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

// End of file SSAHAServer.cpp


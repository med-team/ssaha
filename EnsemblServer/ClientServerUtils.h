/*  Last edited: May 27 14:31 2002 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : ClientServerUtils
// File Name    : ClientServerUtils.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_ClientServerUtils
#define INCLUDED_ClientServerUtils

// Description:

// Includes:
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>

#include <arpa/inet.h>
#include <pthread.h>
#include <stdio.h>
#include <cstdlib>
#include <string>
#include <deque>
typedef   void    Sigfunc(int);
#define SERV_PORT 9877
#define SERV_VERSION 4
#define SA      struct sockaddr
#define LISTENQ         1024
#define MAXLINE         4096    /* max text line length */
#include<exception>
#include "GlobalDefinitions.h"
class Match;

// next line needed to get server to compile on ecs1h and hcs2f
#if OHNOITSADEC
typedef int socklen_t; 
#endif

/* If we're not using GNU C, elide __attribute__ */
#ifndef __GNUC__
#  define  __attribute__(x)  /*NOTHING*/
#endif

// ### Class Declarations ###

// Client/server interface protocol is as follows
// i) Server sends Handshake to client, to tell client the word size w it is
// expecting
// ii) Client reads in queries and converts them to bases using w bases
// per Word (TBD need to change slightly for protein)
// iii) Client sends QueryHeader to server, to tell server how many sequences 
// to expect. Also sends total number of words as a checksum, and parameters to
// use for the search.
// iv) For each query sequence, client sends a SequenceHeader and then the 
// Words of the query sequence
// v) Server does the match
// vi) Server sends a MatchHeader, to report whether the search was successful
// and, if so, how many MatchStructs to expect
// vii) Client reads a MatchStruct for each Match, and does what the hell it
// likes with 'em.


// TBD also include table type (DNA/protein)
// The `attribute' gibberish ensures sizeof(struct)= sum of sizeof what's in it
// (i.e no weird packing)

enum TableType
{
  e2bitDNA = 0,
  e5bitProtein = 1,
  e5bitTranslatedDNA = 2 
};



struct Handshake
{
  unsigned int ssahaversion;
  unsigned int wordLength;
  TableType tableType;
  unsigned int maxBufferSize;
} __attribute__ ((packed));

enum SortModeType
{
  eNoSort = 0,
  eSortByMatchLength = 1,
  eSortByPercentMatch = 2,
  eSortAndReturnSequence = 3 // return matched ASCII seq to client
};

struct QueryHeader
{
  unsigned int numQuerySeqs;
  unsigned int numQueryWords;
  unsigned int bitsPerSymbol;
  unsigned int minPrint;
  unsigned int maxGap;
  unsigned int maxInsert;
  unsigned int numRepeats;
  unsigned int clipThreshold;
  unsigned int maxMatches;
  unsigned int substituteThreshold;
  unsigned int bandExtension;
  SortModeType sortMode;
  friend ostream& operator<<( ostream& os, QueryHeader& q )
  {
    os << q.numQuerySeqs << " seqs " << q.numQueryWords << " words "
       << q.minPrint << "-" << q.maxGap << "-" << q.maxInsert << "-"
       << q.numRepeats << "-" << q.clipThreshold << "-" 
       << q.maxMatches << "-" << q.sortMode << "-"
       << q.substituteThreshold << "-" << q.bandExtension << endl;
  }
} __attribute__ ((packed));

struct SequenceHeader
{
  unsigned int size;
  unsigned int basesInLast;
} __attribute__ ((packed));

struct MatchHeader
{
  bool wasSuccessful; // TBD enum with more descriptive stuff
  int numSubjectNames; 
  int numMatches;
} __attribute__ ((packed));
// then expects numSubjectNames int/string pairs followed by numMatches 
// 

struct MatchInfo
{
  SequenceNumber subjectNum;
  SequenceOffset subjectStart;
  SequenceOffset subjectEnd;
  SequenceNumber queryNum;
  SequenceOffset queryStart;
  SequenceOffset queryEnd;
  int  numBases;
  bool isQueryForward;
  bool isSubjectForward;
  // simple constructors
  MatchInfo( void ) :
  subjectNum(0),
  subjectStart(0),
  subjectEnd(0),
  queryNum(0),
  queryStart(0),
  queryEnd(0),
  numBases(0),
  isQueryForward(true),
  isSubjectForward(true) {}
  MatchInfo( const Match& m );
} __attribute__ ((packed));




class NetworkException : public SSAHAException
{
    public:
    NetworkException( const string& message ) :
    SSAHAException(message) {}
};

class BrokenSocketException : public NetworkException
{
 public:
  BrokenSocketException( void ) :
    NetworkException("broken socket exception") {}
};


// ### Function Declarations ###

void err_sys(const char *fmt, ...);
void sig_chld(int signo);
struct sockaddr;
int Accept(int fd, sockaddr *sa, socklen_t *salenptr);
void Bind(int fd, const sockaddr* sa, socklen_t salen);
void Listen(int fd, int backlog);
Sigfunc * signal(int signo, Sigfunc *func);
Sigfunc * Signal(int signo, Sigfunc *func);
void Close(int fd);
pid_t Fork(void);
int Socket(int family, int type, int protocol);
ssize_t readn(int fd, void *vptr, size_t n);
ssize_t Readn(int fd, void *ptr, size_t nbytes);
ssize_t	writen(int fd, const void *vptr, size_t n); 
void Writen(int fd, void *ptr, size_t nbytes);
char * Fgets(char *ptr, int n, FILE *stream);
void Fputs(const char *ptr, FILE *stream);
void Connect(int fd, const sockaddr *sa, socklen_t salen);


class SocketInterface : public deque<char>
{
 public:
  SocketInterface( int portNum, int timeOutSeconds=10000 ) : 
    portNum_( portNum ), bytesRead_(0), bytesSent_(0) 
    {
      timeOut_.tv_sec  = timeOutSeconds;
      timeOut_.tv_usec = 0;
    }
  enum { MaxChunkSize=MAXLINE };
  
  void setTimeOut( int timeOutSeconds ) {
    timeOut_.tv_sec = timeOutSeconds;
  }

  void getAtLeast( int numBytes );

  template <typename T> void sendStruct( T* ptr )
  {
    bytesSent_+= sizeof(T);
    Writen(portNum_, (void*) ptr, sizeof(T) );
  }
  
  void copy( char* target, int numBytes);

  template <typename T> void receiveStruct( T* ptr )
  {
    copy( (char*)ptr, sizeof(T));
  }


  void checkSocketEmpty( void ); // else throws;

  void sendSequence( const WordSequence& seq );
  void receiveSequence( WordSequence& seq );

  void sendString( const string& s );
  void sendChars( const char* pChar, int numChars );

  void receiveString( string& s );
  void receiveChars( vector<char>& v );

 protected:
  int portNum_;
  int bytesRead_;
  int bytesSent_;
  // timeOut_ used in getAtLeast
  timeval timeOut_;
  char buffer_[MAXLINE];
};

#endif

// End of file ClientServerUtils.h

/*  Last edited: Feb 11 15:34 2002 (ac2) */
/*

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : SSAHAWrapper
// File Name    : SSAHAWrapper.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
*/

#ifdef __cplusplus
extern "C"
{
#endif

enum SSAHAQueryType
{
  eDNAQuery,
  eProteinQuery
};




typedef struct 
{
  unsigned int seqNum;
  int seqPos;
} SSAHAHit;

/* return name of the seqNum-th sequence in the table */
const char* getSubjectName( unsigned int seqNum );



/* load a set of SSAHA tables named tableName.* into RAM */
int loadTable( const char* const tableName );

/* perform a SSAHA search */
int doSearch
( 
 const char* const pWords,
 const int numWords,
 SSAHAHit** const pHits,
 int ** const pStarts
);

/* Clear search results from RAM */ 
/* always called by loadTable, but not by doSearch */
int clearSearch( void );
/* Clear SSAHA tables from RAM */
/* always called by loadTable */
int clearTable( void );


#ifdef __cplusplus
}
#endif





/* End of file SSAHAWrapper.h */

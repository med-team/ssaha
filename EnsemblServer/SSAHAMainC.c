/*  Last edited: Feb 11 16:06 2002 (ac2) */
/*

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : SSAHAMainC
// File Name    : SSAHAMainC.cpp
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Description:
// 
// Main demonstrating C interface to SSAHA
//
// Includes:
*/
#include "SSAHAWrapper.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main( int numArgs, char** args )
{
  char lineBuf[1000];
  FILE* fp;
  SSAHAHit* pThis;
  SSAHAHit* pTop;
  int* pStart;
  char* pSource;

  if (numArgs!=3)
  {
     printf
       ("Usage: %s queryFileName tableName\n",
	args[0]);
     exit(-1);
  } /* if */

  if ((fp=fopen(args[1],"r"))==NULL)
  {
    printf("Error: could not open query file %s", args[1]);
  } /* if */  

  loadTable( args[2] );

  while( fgets(lineBuf, 1000, fp) )
  {
    doSearch( lineBuf, (strlen(lineBuf)-1)/5, &pTop, &pStart );
    /* the `strlen...' bit just gives max num whole words */
    pThis=pTop;
    pSource=lineBuf;
    while(*pStart!=-1)
    {
      printf("%c%c%c%c%c\n", *pSource++, *pSource++, *pSource++, *pSource++, *pSource++);
      while (pThis!=&pTop[*pStart])
      {
	printf("%s %d %d\n", getSubjectName( pThis->seqNum), pThis->seqNum, pThis->seqPos);
	pThis++;
      } /* while */
      pStart++;
    } /* while */

  } /* while */

    /*  printf("%s %d",lineBuf, strlen(lineBuf));*/
  

  clearSearch();
  clearTable();
  return 0;
} /* main */




/* End of file SSAHAMainC.cpp */


/*  Last edited: Apr 19 11:09 2002 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : SSAHAClient
// File Name    : SSAHAClient.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_SSAHAClient
#define INCLUDED_SSAHAClient

// Description:

// Includes:
#include "GlobalDefinitions.h"
#include "MatchStore.h"
#include "SequenceReader.h"

// NB it is good practise for #include statements in header files to be
// replaced by forward declarations if at all possible
typedef pair< WordSequence, std::string> QueryInfo;

// ### Class Declarations ###

// Class Name :
// Description: 
class MatchRemote : public Match
{
 public:
  MatchRemote( vector<QueryInfo>& query );

  virtual ~MatchRemote();
  virtual SequenceNumber getSubjectNum( void ) const 
    { return data_.subjectNum; }
  virtual const char* getSubjectName( void ) const
    { return names_[ data_.subjectNum ].c_str(); }
  virtual SequenceOffset getSubjectStart( void ) const 
    { return data_.subjectStart; }
  virtual SequenceOffset getSubjectEnd( void ) const 
    { return data_.subjectEnd; }

  virtual SequenceNumber getQueryNum( void ) const
    { return data_.queryNum; }
  virtual inline string         getQueryName( void ) const;
  virtual SequenceOffset getQueryStart( void ) const 
    { return data_.queryStart; }

  virtual SequenceOffset getQueryEnd( void ) const 
    { return data_.queryEnd; }
  
  virtual inline int  getQuerySize( void ) const;
  virtual int  getNumBases(void ) const 
    { return data_.numBases; }
  virtual bool isQueryForward( void ) const 
    { return data_.isQueryForward; }
  virtual bool isSubjectForward( void ) const 
    { return data_.isSubjectForward; }
  
  virtual void print( void ) const {}
  MatchInfo data_;
  mutable map<SequenceNumber,std::string> names_;
private:
  vector<QueryInfo>& query_;
}; // ~class MatchRemote


class MatchStoreRemote : public MatchStore
{
public:
  MatchStoreRemote( vector<QueryInfo>& query ) :
  match_(query)
  { 
    push_back(&match_); 
  }
  MatchRemote match_;
};

class SourceReaderDummy : public SourceReader
{
public:
  virtual void extractSource
  ( char** pSource, //vector<char>& source, 
    SequenceNumber seqNum,
    SequenceOffset seqStart,
    SequenceOffset seqEnd ); 

  string source_;
};





// ### Function Declarations ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

// End of include guard:
#endif

// End of file SSAHAClient.h

#!/bin/csh

# script to test response of SSAHA executable to various argument sets
# not an exhaaustive set of tests, a bit out of date - TC 30.5.2

set FAIL="TEST: Test failed"

echo "TEST:"
echo "TEST: Checking help functions"
echo "TEST:"


echo "TEST: -h supplied - should print help message and pass"
./ssaha -h
set RETVAL=$status
if ( $RETVAL != 0 ) then 
echo $FAIL
exit (1)
endif

echo "TEST: -help supplied - should print help message and pass"
./ssaha -help
set RETVAL=$status
if ( $RETVAL != 0 ) then
echo $FAIL 
exit (1) 
endif

echo "TEST:"
echo "TEST: Checking main runs with default arguments"
echo "TEST:"

echo "TEST: run with default arguments - should pass"
./ssaha test.fasta test.fasta
set RETVAL=$status
if ( $RETVAL != 0 ) then
echo $FAIL 
exit (1) 
endif

echo "TEST:"
echo "TEST: Checking invalid arguments are caught by the main"
echo "TEST:"

echo "TEST: No arguments supplied - should print message and fail"
./ssaha 
set RETVAL=$status
if ( $RETVAL != 0 ) then
echo $FAIL 
exit (1) 
endif

echo "TEST: unknown argument supplied - should fail"
./ssaha junk
set RETVAL=$status
if ( $RETVAL != 1 ) then
echo $FAIL 
exit (1) 
endif

echo "TEST: invalid value supplied for queryType"
./ssaha test.fasta test.fasta -qt junk
set RETVAL=$status
if ( $RETVAL != 1 ) then
echo $FAIL 
exit (1) 
endif

echo "TEST: invalid value supplied for subjectType"
./ssaha test.fasta test.fasta -st junk
set RETVAL=$status
if ( $RETVAL != 1 ) then
echo $FAIL 
exit (1)
endif

echo "TEST: invalid value supplied for queryStart"
./ssaha test.fasta test.fasta -qs -5000
set RETVAL=$status
if ( $RETVAL != 1 ) then
echo $FAIL
exit (1)
endif

echo "TEST: invalid value supplied for queryEnd"
./ssaha test.fasta test.fasta -qe -5000
set RETVAL=$status
if ( $RETVAL != 1 ) then
echo $FAIL
exit (1)
endif

echo "TEST: invalid value supplied for wordLength - replaces with default"
./ssaha test.fasta test.fasta -wl -5000
set RETVAL=$status
if ( $RETVAL != 0 ) then
echo $FAIL
exit (1)
endif

echo "TEST: invalid value supplied for stepLength - sets to word length"
./ssaha test.fasta test.fasta -sl -5000
set RETVAL=$status
if ( $RETVAL != 0 ) then
echo $FAIL
exit (1)
endif

echo "TEST: invalid value supplied for maxStore"
./ssaha test.fasta test.fasta -ms -5000
set RETVAL=$status
if ( $RETVAL != 1 ) then
echo $FAIL
exit (1)
endif

echo "TEST: invalid value supplied for minPrint"
./ssaha test.fasta test.fasta -mp -5000
set RETVAL=$status
if ( $RETVAL != 1 ) then
echo $FAIL
exit (1)
endif

echo "TEST: non existent query file - should fail"
./ssaha junk test.fasta
set RETVAL=$status
if ( $RETVAL != 1 ) then
echo $FAIL
exit (1)
endif
./ssaha junk test.fasta -qt fasta
set RETVAL=$status
if ( $RETVAL != 1 ) then
echo $FAIL
exit (1)
endif
./ssaha junk test.fasta -qt fastq
set RETVAL=$status
if ( $RETVAL != 1 ) then
echo $FAIL
exit (1)
endif


echo "TEST: non existent subject file - should fail"
./ssaha test.fasta junk
set RETVAL=$status
if ( $RETVAL != 1 ) then
echo $FAIL
exit (1)
endif
./ssaha test.fasta junk -st fasta
set RETVAL=$status
if ( $RETVAL != 1 ) then
echo $FAIL
exit (1)
endif
./ssaha test.fasta junk -st fastq
set RETVAL=$status
if ( $RETVAL != 1 ) then
echo $FAIL
exit (1)
endif
./ssaha test.fasta junk -st hash
set RETVAL=$status
if ( $RETVAL != 1 ) then
echo $FAIL
exit (1)
endif

echo "TEST: Running DNA query against DNA hash table"
./ssaha test.fasta test.fasta -qt DNA -st DNA -wl 10 
set RETVAL=$status
if ( $RETVAL != 0 ) then
echo $FAIL
exit (1)
endif

echo "TEST: Running DNA query against protein hash table"
./ssaha test.fasta test_protein.fasta -st protein \
-wl 3 -mp 15
set RETVAL=$status
if ( $RETVAL != 0 ) then
echo $FAIL
exit (1)
endif

echo "TEST: Running protein query against codon hash table"
./ssaha test_protein.fasta test.fasta -qt protein -st codon \
-wl 3 -mp 13
set RETVAL=$status
if ( $RETVAL != 0 ) then
echo $FAIL
exit (1)
endif

echo "TEST: Running codon query against codon hash table"
./ssaha test.fasta test.fasta -st codon \
-wl 3 -mp 15
set RETVAL=$status
if ( $RETVAL != 0 ) then
echo $FAIL
exit (1)
endif

echo "TEST: Running protein query against protein hash table"
./ssaha test_protein.fasta test_protein.fasta -qt protein -st protein \
-wl 3 -mp 15 
set RETVAL=$status
if ( $RETVAL != 0 ) then
echo $FAIL
exit (1)
endif


echo "TEST: All commands executed successfully"

/*  Last edited: Apr 18 16:20 2002 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : GlobalDefinitions
// File Name    : GlobalDefinitions.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_GlobalDefinitions
#define INCLUDED_GlobalDefinitions

// Description:

// This module brings together various definitions that will be used throughout
// the SSAHA library.

// Macro definitions:

// The following preprocessor directives work thus:
// If compiled with 'g++ -DEBUG_LEVEL1 ...' any DEBUG_L1(...) commands
// in the source are compiled.
// If compiled with 'g++ -DEBUG_LEVEL2 ...' any DEBUG_L1(...) or DEBUG_L2(...)
// commands in the source are compiled.
// If compiled with 'g++ -DEBUG_LEVEL3 ...' any DEBUG_L1(...), DEBUG_L2(...)
// or DEBUG_L3(...) commands in the source are compiled.

#define DEBUG_L3(X)
#define DEBUG_L2(X)
#define DEBUG_L1(X)

#ifdef EBUG_LEVEL3
#undef  DEBUG_L3
#define DEBUG_L3(X) cout << "DE3: " << X << endl
#define EBUG_LEVEL2
#endif

#ifdef EBUG_LEVEL2
#undef  DEBUG_L2
#define DEBUG_L2(X) cout << "DE2: " << X << endl
#define EBUG_LEVEL1
#endif

#ifdef EBUG_LEVEL1
#undef  DEBUG_L1
#define DEBUG_L1(X) cout << "DE1: " << X << endl
#endif

// Includes:

using namespace std;
class HashTableGeneric;
#include <unistd.h>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <utility>
#include <sys/types.h>
#include <stdio.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <errno.h>

// ### Class Declarations ###

// All global variable names are preceded by g
static const char gBaseNames [] 
= "ACGT";

static const char gResidueNames[] 
//= "*ACDEFGHIKLMNPQRSTUVWY??????????"; // should probably remove U
  = "*ACDEFGHIKLMNPQRSTVWXY??????????"; 
// changed 20.3.2 AJC 
// coding for U removed, extra encoding for x added
// 0  *
// 1  A
// 2  C
// 3  D
// 4  E
// 5  F
// 6  G
// 7  H
// 8  I
// 9  K
// 10 L
// 11 M
// 12 N
// 13 P
// 14 Q
// 15 R
// 16 S
// 17 T
// 18 V
// 19 W
// 20 X
// 21 Y




static const char gCodonNames[] 
= "KNKNTTTTRSRSIIMIQHQHPPPPRRRRLLLLEDEDAAAAGGGGVVVV*Y*YSSSS*CWCLFLF";

typedef unsigned int Word; 
// All sequences of base pairs are split into equal size pieces of up to 16
// base pairs, each of which is encoded within a Word as follows:
// A=00, C=01, G=10, T=11 
// The last base pair of a sequence takes the least significant 2 bits of the 
// Word.
// NB:
// i) If the word length is not 16 then the most significant bits of the Word
// will not be used.
// ii) Knowledge of the word length is necessary to decode the Word
// successfully.

// MSB of a word may be set to indicate that it is `cursed', ie it is not
// to be used for hashing (used to deal with words containing 'n's & '-'s
// This limits maximum word length to 15 for DNA
// TC 23.10.1
static const Word gCursedWord = 1<<31;

typedef unsigned char uchar;
typedef unsigned short ushort;


enum
{
  gBaseBits = 2,
  gResidueBits = 5,
  gBasesPerCodon = 3,
  gCodonBits = gBaseBits*gBasesPerCodon,
  gBitsPerWord = 8*sizeof(Word),
  gMaxBasesPerWord = gBitsPerWord / gBaseBits,
  gNumReadingFrames = 3, 
  gNumDirections = 2,
  gNumCodonEncodings = 22 
  // = 20 amino acids + 1 stop codon + selenocysteine = 22
  // now 20 amino acids + stop codon + X - TC 27.3.2
};

enum SourceDataType
{
  gDNAData = 0,
  gProteinData = 1,
  gUnknownData = 2
};

enum HitListFormatType
{
  gStandard = 0,
  g32BitPacked = 1,
  g32BitPackedProtein =2,
  gTranslated = 3,
  gHybrid = 4,
  gNotSpecified = 5
};



// Class Name : WordSequence
// Description: This class holds a sequence of base pairs, expressed as a 
// sequence of Words

// Example: for word length 5, sequence AGCTTGCCTGCCT would be encoded as
// follows:
// AGCTTGCCTGCCT   MSB < -------------------------------- > LSB
// Word 1: AGCTT - xx xx xx xx xx xx xx xx xx xx xx 00 10 01 11 11 = 159
// Word 2: GCCTG - xx xx xx xx xx xx xx xx xx xx xx 10 01 01 11 10 = 606
// Word 3: CCT   - xx xx xx xx xx xx xx xx xx xx xx 01 01 11 00 00 = 368
// xx = not used, set to zero

// i) Need to either pass information that Word 3 contains only 3 base pairs
// (otherwise it gets decoded as CCTAA) or discard word 3 altogether (not
// a problem as in general we intend to deal only with much longer sequences
// than this example).
// NB:
// Idea of interface is to save users from having to learn the initially 
// somewhat daunting looking STL notation for manipulating vectors by hiding 
// them behind commands that are more intuitive to this application. Making 
// the definitions inline ensures little or no performance penalty for this.
// At the moment STL commands can still be used directly if desired, but
// using commands below will 'future proof' against implementation changes
// (i.e. if vectors prove too slow).
class WordSequence : public vector<Word>
{
  public:
  WordSequence( void )                 {} 
  // Next constructor produces a WordSequence containing numWords
  // Words all set to zero.
  WordSequence( int numWords ):
    vector<Word>(numWords), 
    numBasesInLast(0) 
    {} 
  WordSequence( const WordSequence& rhs ) :
    vector<Word>( rhs ), 
    numBasesInLast( rhs.numBasesInLast )
    {}
  WordSequence& operator=( const WordSequence& rhs )
  {
    numBasesInLast = rhs.numBasesInLast;
    vector<Word>::operator=(rhs);
    return *this;
  }

  void addWord( Word inputWord )       { push_back( inputWord ); }
  void removeLastWord( void )          { pop_back(); }
  void setMaxWords( int maxNumWords )  { reserve( maxNumWords ); }
  int  getMaxWords( void )       const { return capacity(); }    
  int  getNumWords( void )       const { return size(); }    
  Word getWord( int wordNumber ) const { return ((*this)[wordNumber]);  }   
  int  getNumBasesInLast( void ) const { return numBasesInLast; }
  void setNumBasesInLast( int nbil )   { numBasesInLast=nbil; }



  protected:
  int numBasesInLast;
};

typedef WordSequence::iterator WordSequenceIterator;
typedef WordSequence::reverse_iterator WordSequenceReverseIterator;

// The position of any single occurrence of any sequence of base pairs in the 
// subject sequence database may be held within an instance of the 
// PositionInDatabase struct.
typedef unsigned int SequenceNumber;
typedef int          SequenceOffset;

struct PositionInDatabase
{
  PositionInDatabase( SequenceNumber sequence_, SequenceOffset offset_ ) :
    sequence(sequence_), offset(offset_) {}
  PositionInDatabase(void) {}
  SequenceNumber sequence;
  // The sequences in the subject database are numbered in the order they
  // are read in from the file, starting at 1
  SequenceOffset offset;
  // The position of a sequence of base pairs in the database 
  // is measured by the offset in base pairs from the first base pair of 
  // a subject sequence. We have to allow for the offset being negative, 
  // because if the end base pairs of a query sequence match the start of one 
  // of the subject sequences, then the position of the start of the query 
  // sequence (measured as an offset from the start of the subject sequence) 
  // will be negative. Hence offset is defined as 'int' not 'unsigned int.' 
  // Clear as mud? Good. 

  // We need to define the '<' operator if we are to make use of STL
  // sorting functions
  bool operator<( const PositionInDatabase& pos) const
  {
    return (    ( sequence < pos.sequence )
             || (    ( sequence == pos.sequence )
                  && ( offset < pos.offset )   )   );
  } // ~operator<

  bool operator==( const PositionInDatabase& pos) const
  {
    return (    ( sequence == pos.sequence )
             && ( offset   == pos.offset   ) );
  } // ~operator<

};


// HitList is an aggregation of PositionInDatabase instances. We may wish to 
// try to speed things up by storing this data in esoteric ways, so this 
// interface just encapsulates the only behaviour we need at this level
// i.e. addHit so that HashTableGeneric and its subclasses can add hits, and
// printHits so that QueryManager and its subclasses can print out the
// results.

#ifdef USING_TEMPLATES_INSTEAD
class HitList
{
 public:
  virtual void addHit
    ( const PositionInDatabase& hitPos, 
      const SequenceOffset& queryPos ) = 0;
  virtual void addHit( SequenceNumber sequence, 
		       SequenceOffset offset, 
		       SequenceOffset queryPos )
    { 
      //      cout << "addHit: " << sequence << " " << offset << " " 
      //   << queryPos << endl;
      addHit( PositionInDatabase(sequence,offset),queryPos); 
    } // ~addHit
}; // ~class HitList
#endif

// Struct Name: HitInfo
// Description: This contains all info for a 'hit', i.e. when a word from
// the query sequence matches a word in one of the subject sequences. 
struct HitInfo
{
  // subjectNum: number of the sequence in the subject database
  SequenceNumber subjectNum;
  // diff: this is defined as 
  //   { position of matching word in subject sequence }
  // - { position of matching word in query sequence }
  SequenceOffset diff;
  // queryPos: this is defined as the position of the matching word in the
  // query sequence.
  SequenceOffset queryPos;

  // Simple constructor
  HitInfo( PositionInDatabase inputHitPos, SequenceOffset inputQueryPos ) :
  subjectNum( inputHitPos.sequence ),
  diff( inputHitPos.offset - inputQueryPos ),
  queryPos( inputQueryPos + 1 ) {}

  // Another simple constructor
  HitInfo( SequenceNumber inputSubjectNum, 
	   SequenceOffset inputSubjectPos,
	   SequenceOffset inputQueryPos ) :
  subjectNum( inputSubjectNum ),
  diff( inputSubjectPos - inputQueryPos ),
  queryPos( inputQueryPos + 1 ) {}

  // '<' operator must be defined to enable vectors of HitInfo instances
  // to be sorted
  bool operator<( const HitInfo& hit) const
  {
    return (    ( subjectNum < hit.subjectNum )
             || (    ( subjectNum == hit.subjectNum )
                  && (    ( diff < hit.diff )      
                       || (    ( diff == hit.diff )      
                            && ( queryPos < hit.queryPos )  )  )  )  );
  } // ~operator<

  bool operator==( const HitInfo& hit) const
  {
    return (    ( subjectNum == hit.subjectNum )
             && ( diff == hit.diff   ) 
             && ( queryPos == hit.queryPos )   );
  } // ~operator<

};

// Class Name : HitListVector
// Description: This class is a basic storage class for database hit
// information. Inherits interface from HitList and implementation
// from the STL vector class.
class HitListVector : 
public vector<HitInfo>
{
 public:
  void addHit( const PositionInDatabase& hitPos, 
               const SequenceOffset& queryPos ) 
  { 
    push_back( HitInfo( hitPos,queryPos ) ); 
  }
  void addHit( SequenceNumber sequence, 
		       SequenceOffset offset, 
		       SequenceOffset queryPos )
  { 
    push_back( HitInfo(sequence, offset, queryPos ) ); 
    //      addHit( PositionInDatabase(sequence,offset),queryPos); 
  } // ~addHit

}; // ~HitListVector

typedef HitListVector HitList;


// Class Name :
// Description: 

class MachineInfo
{
  typedef unsigned long Offset;
  public:
  static MachineInfo* getInfo( void )
  {
    if ( info_ == 0 ) info_ = new MachineInfo;
    return info_;
  } // ~getInfo

  int  getNumBits( void ) { return numBits_; }
  bool getIsLeastSigByteFirst( void ) { return isLeastSigByteFirst_; }
  void report( ostream& os )
  {
    os << "This is a " << numBits_ << " bit machine with "
       << ( (isLeastSigByteFirst_) ? "least" : "most" )
       << "-significant-byte-first ordering" << endl;
  } // ~report

  friend ostream& operator<<( ostream& os, MachineInfo& myInfo )
  {
    myInfo.report(os);
    return os;
  }

  protected:
  MachineInfo( void )
  {

  numBits_ = 8 * sizeof(Offset);
  
    Offset checkByteOrder = 1;
    Offset* offsetAddress = &checkByteOrder;
    char* firstByte = ( (char*) offsetAddress );

    offsetAddress++;
    char* lastByte =  (char*) offsetAddress;
    lastByte--;
    isLeastSigByteFirst_ = ( *firstByte == 1 );

  } // ~constructor

  private:
  static MachineInfo* info_;
  int numBits_;
  bool isLeastSigByteFirst_;
}; 

class SSAHAException : public std::exception
{
public:
  SSAHAException( const string& message = "" ) : message_( message ) {}
  virtual ~SSAHAException() throw() {}
  virtual const char* what() const throw() 
  {
    return message_.c_str();
  } // ~what
protected:
  const string message_;

}; // ~SSAHAException

// Class Name:  ofstreamSSAHA
// Description: overrides standard ofstream to allow more than
// (2^31)-1 (approx 2Gb) bytes to be written at a time
class ofstreamSSAHA : public std::ofstream
{
public:
  static const unsigned long maxChunkSize = ( ((unsigned long)1<<31)-1 ); 
  //  static const unsigned long maxChunkSize
  //  ( ((unsigned long)1<<31)-1 ); 

  ofstreamSSAHA
  ( const char* fileName, unsigned long chunkSize = maxChunkSize )  
    : std::ofstream( fileName ), chunkSize_( chunkSize ) {}
  ostream& write( const char* p, unsigned long s )
  {
    while ( s > maxChunkSize )
    {
      std::ofstream::write(p,maxChunkSize); 
      p+=maxChunkSize;
      s-=maxChunkSize;
    } // ~while
    std::ofstream::write(p,s);
    return *this;
  } // ~write


protected:
  unsigned long chunkSize_;

}; // ~class ofstreamSSAHA

// Class Name:  ifstreamSSAHA
// Description: overrides standard ifstream to allow more than
// (2^31)-1 (approx 2Gb) bytes to be read at a time
class ifstreamSSAHA : public std::ifstream
{
  static const unsigned long maxChunkSize = ( ((unsigned long)1<<31)-1 ); 
  //  static const unsigned long maxChunkSize
  //  ( ((unsigned long)1<<31)-1 ); 

public:
  ifstreamSSAHA
  ( const char* fileName, unsigned long chunkSize = maxChunkSize ) 
    : std::ifstream( fileName ), chunkSize_( chunkSize ) {}

  istream& read( const char* p, unsigned long s )
  {
    bytesSoFar_=0;
    while ( s > maxChunkSize )
    {
      std::ifstream::read((char*)p,maxChunkSize); 
      p+=maxChunkSize;
      s-=maxChunkSize;
      bytesSoFar_+=std::ifstream::gcount();
    } // ~while
    std::ifstream::read((char*)p,s);
    bytesSoFar_+=std::ifstream::gcount();
    //    cout << "!!!read " << bytesSoFar_ << endl; 
    return *this;
  } // ~read

  unsigned long gcount( void ) const
    { return bytesSoFar_; }


protected:
  unsigned long chunkSize_;
  unsigned long bytesSoFar_;

}; // ~class ifstreamSSAHA

// Class Name:  NullBuffer
// This can be used to suppress an output stream by doing
// NullBuffer db; mystream.rdbuf(&db); 
class NullBuffer : public std::streambuf
{
public:
  NullBuffer() {}
};

// Function Name: getTimeNow
// returns a string detailing the current date & time
char* getTimeNow(void);


// Class Name : printWord
// Description: The purpose of this class is to provide a convenient
// way to convert Word and WordSequence data to ASCII base pair format
// Example usage:
// Word w = 228 ; // = binary 11100100 = ACGT
// cout << w; // prints out '228'
// cout << printWord(w,4); // prints out 'AGCT', 4 = word length
// Alternatively, if w is used to access HashTableGeneric h (i.e. their word 
// lengths are the same) can just do
// cout << printWord(w,h); // printWord asks h for word length
// Can also print out WordSequence (the base pairs in its words appear as one
// long string) - need to pass in either word length or hash table, as above.
// WordSequence W; // ... now populate using addWord
// cout << printWord(W,10); // ... can also pass in word length directly, 
//
// Note that class name does not begin with a capital because it is 
// mimicing a function 


class PrintFromWord
{
  public:
  PrintFromWord( const Word word, int wordLength, int bitsPerSymbol, 
                 const char* tt ) 
  : pWordSeq_(0), word_( word ), length_( wordLength ),
    bitsPerSymbol_( bitsPerSymbol ), tt_(tt), 
    mask_( (1 << bitsPerSymbol) - 1 ) 
  {}

  PrintFromWord( const Word word, const HashTableGeneric& hashTable, 
		 int bitsPerSymbol, const char* tt ); 

  PrintFromWord( const WordSequence& wordSeq, int wordLength, 
		 int bitsPerSymbol, const char* tt ) 
  : pWordSeq_( &wordSeq ), length_( wordLength ), 
    bitsPerSymbol_( bitsPerSymbol ), tt_( tt ),
   mask_( (1 << bitsPerSymbol) - 1 ) 
  {}

  PrintFromWord( const WordSequence& wordSeq, const HashTableGeneric& hashTable, 
		 int bitsPerSymbol, const char* tt ); 


  void doPrint( ostream& os );

  void doWord( ostream& os, const Word word )
    { doWord(os, word, length_ ); }
  void doWord( ostream& os, const Word word, int length );

  friend ostream& operator<<( ostream& os, PrintFromWord pw)
  {
    pw.doPrint(os);
    return os;
  } // ~operator<<
  protected:
  const WordSequence*  pWordSeq_;
  Word                 word_;
  int                  length_;
  int                  bitsPerSymbol_;
  const char*          tt_;
  Word                 mask_;

}; // ~class PrintFromWord

class printBase : public PrintFromWord
{
 public:
  printBase( const Word word, int wordLength ) :
    PrintFromWord( word, wordLength, gBaseBits, gBaseNames ) {}
  printBase( const Word word, const HashTableGeneric& hashTable ) :
    PrintFromWord( word, hashTable, gBaseBits, gBaseNames ) {}
  printBase( const WordSequence& wordSeq, int wordLength ) :
    PrintFromWord( wordSeq, wordLength, gBaseBits, gBaseNames ) {}
  printBase( const WordSequence& wordSeq, const HashTableGeneric& hashTable ) :
    PrintFromWord( wordSeq, hashTable, gBaseBits, gBaseNames ) {}
};

class printResidue : public PrintFromWord
{
 public:
  printResidue( const Word word, int wordLength ) :
    PrintFromWord( word, wordLength, gResidueBits, gResidueNames ) {}
  printResidue( const Word word, const HashTableGeneric& hashTable ) :
    PrintFromWord( word, hashTable, gResidueBits, gResidueNames ) {}
  printResidue( const WordSequence& wordSeq, int wordLength ) :
    PrintFromWord( wordSeq, wordLength, gResidueBits, gResidueNames ) {}
  printResidue( const WordSequence& wordSeq, 
		const HashTableGeneric& hashTable ) :
    PrintFromWord( wordSeq, hashTable, gResidueBits, gResidueNames ) {}
};

typedef printBase printWord; // for backwards compatibility

// ### Function Declarations ###

// Name: makeWord
// Arguments: const string&
// Returns: Word
// very basic function to encode a string into a Word
//Word makeWord(const string& s);

class MakeIntoWord
{
 public:
  MakeIntoWord( int bitsPerSymbol, const char* tt );

  Word operator()( const string& s );

  protected:
  int bitsPerSymbol_;
  const char* tt_;
  map< char, Word > map_;
};

static MakeIntoWord makeBase( gBaseBits, gBaseNames );
// next ensures backwards compatibility
static MakeIntoWord makeWord( gBaseBits, gBaseNames ); 
static MakeIntoWord makeResidue( gResidueBits, gResidueNames );

  // Function Name: reverseComplement
  // Arguments: Word (in)
  // Returns:   Word (out)
  // Returns the reverse complement of a Word
  Word reverseComplement( Word word, int wordLength );

  // Function Name: reverseComplement
  // Arguments: const WordSequence& (in), WordSequence& (out), int (in)
  // Returns:   void
  // This computes the reverse complement of seq and places it in revComp
  // Reason for parameter numBasesInLast is that if only numBasesInLast
  // (< wordLength_) valid base pairs are stored in the last element of
  // seq then obviously the last (wordLength_-numBasesInLast) base pairs of
  // seq are not valid. But we do not want our computed reverse complement 
  // to begin with the RC of these invalid pairs, so shiftSequence is called
  // to remove them.
//  void reverseComplement
//  ( const WordSequence& seq, 
//    WordSequence& revComp, 
//    int wordLength,
//    int numBasesInLast );

  void reverseComplement
  ( const WordSequence& seq, 
    WordSequence& revComp,
    int wordLength );

// Function Name: shiftSequence
// Arguments: WordSequence& (in/out), int (in)
// Returns:   void 
// This shifts a WordSequence 'i places to the left', i.e. symbol i+1 of
// the sequence becomes symbol 1. The first i symbols are lost. 
void shiftSequence
( WordSequence& sequence, int bitsPerSymbol, int wordLength, int i = 1 );

// Function Name: shiftSequence
// This shifts a WordSequence 'i places to the left', i.e. base i+1 of
// the sequence becomes base 1. The first i bases are lost. 
inline void shiftSequenceDNA
( WordSequence& sequence, int wordLength, int i = 1 )
{ 
  shiftSequence( sequence, gBaseBits, wordLength, i ); 
}

// Function Name: shiftSequence
// This shifts a WordSequence 'i places to the left', i.e. residue i+1 of
// the sequence becomes residue 1. The first i residues are lost. 
inline void shiftSequenceProtein
( WordSequence& sequence, int wordLength, int i = 1 )
{ 
  shiftSequence( sequence, gResidueBits, wordLength, i ); 
}

void loadFromFile
( const string& fileName, const char* buffer, const unsigned long numBytes,
ostream& monitoringStream_=cerr );

void saveToFile
( const string& fileName, const char* buffer, const unsigned long numBytes,
ostream& monitoringStream_=cerr );


// Class Name : Allocator
// Description: An Allocator handles all dynamic memory allocation operations
// on a pointer to memory
// Idea is to enable large chunks of memory to be obtained either
// from local memory or by memory mapping from the file system (and
// for everything outside the Allocator to be oblivious to the difference!)
template <typename T> class Allocator
{
public:
  typedef T MyType;

  Allocator( T** ptr, const string& name, ostream& monStream=cerr ) : 
    ptr_(ptr), name_(name), size_(0), isAllocated_(false), 
    monStream_( monStream )
    {}

  //  template< class T > void link( T** ptr, const string& name )
  //  {
  //    if (isLinked_) return; // can't relink once linked
  //    name_=name;
  //    ptr_=(char**)ptr;
  //    isLinked_=true;
  //  }

  virtual Allocator<T>* clone( T** ptr, 
			       const string& name, 
			       ostream& monStream )=0;

  virtual void allocate( unsigned long size ) =0;
  virtual void allocateAndZero( unsigned long size ) =0;
  virtual void load( unsigned long size ) =0;
  virtual void save() =0;
  virtual void deallocate() =0;


  virtual ~Allocator() {}
  //protected:
  T** ptr_;
  string name_;
  unsigned long size_;
  bool isAllocated_;
  ostream& monStream_;
  //  bool isLinked_;
};

// Class Name : AllocatorLocal
// Description: Handles all dynamic memory allocation operations on a pointer
// to memory. Memory is allocated from local memory (new & delete & all that)
template <typename T> class AllocatorLocal : public Allocator<T>
{
public:

  AllocatorLocal( T** ptr=NULL, 
		  const string& name = "", 
		  ostream& monStream=cerr ) : 
  Allocator<T>(ptr,name,monStream) 
  {}

  virtual Allocator<T>* clone
    ( T** ptr, const string& name, ostream& monStream=cerr )
  {
    return new AllocatorLocal<T>(ptr,name,monStream);
  }

  virtual ~AllocatorLocal() 
  { 
    deallocate(); 
  }
 
  virtual void allocate( unsigned long size )
  {
    size_=size;
    (*ptr_)=new T[size_];
    isAllocated_=true;
  }

  virtual void allocateAndZero( unsigned long size ) 
  {
    const unsigned char zero(0);
    allocate(size);
    memset( (void*)(*ptr_), zero, size_*sizeof(MyType) );
  }
  virtual void load( unsigned long size )
  {
    allocate(size);
    loadFromFile( name_, (char*)(*ptr_), size_*sizeof(MyType), monStream_ );
  }
  virtual void save()
  {
    saveToFile( name_, (char*)(*ptr_), size_*sizeof(MyType), monStream_ );
  }

  virtual void deallocate()
  {
    if (!isAllocated_) return;
    delete [] (*ptr_);
    isAllocated_=false;
  }
protected:
};


#ifdef USING_POSIX_MEMORY_MAPPING

class MemoryMapper
{
 public:
  MemoryMapper() {}

  struct Mode
  {
    Mode( int shared_, int access_, int flags_, bool delete_, bool truncate_ ):
      shared(shared_),
      access(access_),
      flags(flags_), 
      deleteFileOnExit(delete_),
      truncateFile(truncate_)
      {}
    int shared;
    int access;
    int flags;
    bool deleteFileOnExit;
    bool truncateFile;
  };

  static const Mode createMap;
  static const Mode readMap;

  void* linkToMap
    ( const Mode& mode, const string& name, unsigned long numBytes );
 protected:

  int fileDesc_;


};

// NB to get the mmap stuff working, need to change the makefile in
// following ways:
// 1. GlobalDefinitions.o from CFLAGS to CFLAGS_NOOPT
// 2. SSAHAMain.o from CFLAGS to CFLAGS_NOOPT
// 3. uncomment 'LIB = -lrt' line

// Class Name : AllocatorMapped
// Description: Handles all dynamic memory allocation operations on a 
// pointer to memory. Memory is allocated using POSIX memory mapping.
// Adapted from Compaq's guide to real time programming (on the web)
template <typename T> class AllocatorMapped : 
public Allocator<T>, private MemoryMapper
{
public:

  AllocatorMapped( T** ptr=NULL, const string& name = "" ) : 
    Allocator<T>(ptr,name), MemoryMapper(), mode_(createMap) 
  {
    if ( (name_=="") || (name_[0]=='.') )
    {
      char buf[50];
      sprintf(buf,"SSAHAMapFile-%d",getpid());
      name_=(string)buf+name_;
    }
  }

  virtual Allocator<T>* clone( T** ptr, const string& name )
  {
    return new AllocatorMapped<T>(ptr,name);
  }

  virtual ~AllocatorMapped() { if (isAllocated_) deallocate(); }

  virtual void allocate( unsigned long size )
  {
    if (isAllocated_) return;
    mode_ = MemoryMapper::createMap;
    size_ = size;
    (*ptr_) = (T*) linkToMap(mode_,name_,size_*sizeof(MyType));
    isAllocated_ = true;
  }

  virtual void allocateAndZero( unsigned long size )
  {
    allocate(size); // mapped memory is already zeroed
  }

  // because we are mapping the file into memory directly
  // don't need to load or save
  virtual void load( unsigned long size ) 
  {
    if (isAllocated_) return;
    mode_ = MemoryMapper::readMap;
    size_ = size;
    (*ptr_) = (T*) linkToMap(mode_,name_,size_*sizeof(MyType));
    isAllocated_ = true;
  }


  virtual void save() 
  {
    mode_.deleteFileOnExit=false;
  }

  virtual void deallocate()
  {
    if (!isAllocated_) return;
    if(munmap((caddr_t)(*ptr_), size_*sizeof(MyType)) < 0)
      perror("unmap error"); // don't throw - called from destructor!
    close(fileDesc_);
    if (mode_.deleteFileOnExit) shm_unlink(name_.c_str());
    isAllocated_=false;
  }

protected:
  MemoryMapper::Mode mode_;
};

#endif


// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

// End of include guard:
#endif

// End of file GlobalDefinitions.h



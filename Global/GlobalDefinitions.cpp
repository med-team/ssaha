
// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : GlobalDefinitions
// File Name    : GlobalDefinitions.cpp
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Description:

// Includes:


#include "GlobalDefinitions.h"
#include "HashTable.h"
#include <iostream>
#include <ctime>

// ### Function Definitions ###

char* getTimeNow(void)
{
  time_t now = time(NULL);
  tm* ptime = localtime(&now);
  return asctime(ptime);
}

PrintFromWord::PrintFromWord
( const Word word, const HashTableGeneric& hashTable, 
  int bitsPerSymbol, const char* tt )
  : pWordSeq_(0), word_( word ), length_( hashTable.getWordLength() ),
    bitsPerSymbol_( bitsPerSymbol ), tt_( tt ),
    mask_( (1 << bitsPerSymbol) - 1 ) 
{}


PrintFromWord::PrintFromWord
( const WordSequence& wordSeq, const HashTableGeneric& hashTable, 
  int bitsPerSymbol, const char* tt ) 
  : pWordSeq_( &wordSeq ), length_( hashTable.getWordLength() ), 
    bitsPerSymbol_( bitsPerSymbol ), tt_( tt ),
    mask_( (1 << bitsPerSymbol) - 1 ) 
{}




void PrintFromWord::doPrint( ostream& os )
  {
    if ( pWordSeq_ )
    {       
      for 
      ( 
        WordSequence::const_iterator
        i (pWordSeq_->begin() );
	i != static_cast<WordSequence::const_iterator>(&pWordSeq_->back());
        ++i 
      ) 
      {
        doWord( os, *i ); 
      } // ~for
      if (pWordSeq_->size() != 0)
      {
	if (pWordSeq_->getNumBasesInLast()==0) 
	{
	  doWord(os, pWordSeq_->back());
	}
        else
	{
	  doWord( os, 
		  (pWordSeq_->back() >> 
		   (bitsPerSymbol_*(   length_
				     - pWordSeq_->getNumBasesInLast())) ), 
		  pWordSeq_->getNumBasesInLast() );
	}
      }
    } // ~if
    else 
    {
      doWord( os, word_ );
    } // ~else
  } // ~PrintFromWord::doPrint

  void PrintFromWord::doWord( ostream& os, const Word word, int length )
  { 
    for ( int i( length - 1 ) ; i >= 0 ; i-- )
    {

      //      os << &tt_ << " " <<
      //    ( ( word & ( mask_ << (bitsPerSymbol_*i) ) ) 
      //		       >> (bitsPerSymbol_*i) ) << endl;

      os << tt_
    [ ( word & ( mask_ << (bitsPerSymbol_*i) ) ) 
    >> (bitsPerSymbol_*i) ];
    } 
  } // ~doWord

MakeIntoWord::MakeIntoWord( int bitsPerSymbol, const char* tt ):
bitsPerSymbol_( bitsPerSymbol ), tt_( tt ) 
{
  for ( unsigned int i(0) ; i < (1<<bitsPerSymbol_) ; ++i )
  {
    map_.insert( make_pair( (static_cast<char>(tolower(tt[i]))),i ) );
    map_.insert( make_pair( (static_cast<char>(toupper(tt[i]))),i ) );
  };
} // ~MakeIntoWord::MakeIntoWord( int bitsPerSymbol, const char* tt )

/*Word makeWord(const string& s)
{
  Word w(0);
  if ( s.size() > (4*sizeof(Word)) ) return w;
  for ( int i(0) ; i < s.size() ; i++ )
  {
    w <<= 2;
    if      ((s[i] == 'C') || (s[i] == 'c'))   w |= 0x1; 
    else if ((s[i] == 'G') || (s[i] == 'g'))   w |= 0x2; 
    else if ((s[i] == 'T') || (s[i] == 't'))   w |= 0x3; 
    else if ((s[i] != 'A') && (s[i] != 'a')) { w = 0; break; }
  } // ~for
  return w;
} // ~makeWord(const string& s)
*/
Word MakeIntoWord::operator()( const string& s )
{
  Word w(0);
  if ( s.size()*bitsPerSymbol_ > (8*sizeof(Word)) ) return w;
  for ( unsigned int i(0) ; i < s.size() ; i++ )
  {
    w <<= bitsPerSymbol_;
    if (map_.find(s[i]) == map_.end() ) { w=0; break; }
    else w |= map_[s[i]];
  } // ~for
  return w;
  
} // ~Word MakeIntoWord::operator()( const string& s )

  // Function Name: reverseComplement
  // Arguments: Word
  // Returns:   Word
  // Tried several ways of computing the reverse complement (including
  // use of look up tables etc.). This was the fastest. 
  Word reverseComplement( Word word, int wordLength ) 
  {
    Word revComp( 0 ), mask( 0x3 );
    const int lim ( ( wordLength - 1 )*2 );
    for ( int i( 0 ) ; i <= lim ; i+=2 )
    {  
      revComp |= (((word & mask)^mask) >> i) << lim - i;
      mask <<= 2;
    } // ~for
    //    revComp |= gCursedWord * ((word&gCursedWord)!=(Word)0);
    return revComp;
  } // ~reverseComplement( Word ) const

  // Function Name: reverseComplement
  // Arguments: const WordSequence& (in), WordSequence& (out), int (in)
  // Returns:   void
  // This computes the reverse complement of seq and places it in revComp
void reverseComplement
( const WordSequence& seq, WordSequence& revComp, 
  int wordLength )
{
  //  assert(numBasesInLast==seq.getNumBasesInLast());
  for ( WordSequence::const_reverse_iterator thisWord( seq.rbegin() ); 
        thisWord != seq.rend();
        thisWord ++ )
  {
    revComp.push_back( reverseComplement( *thisWord, wordLength ) );
  } // ~for
  shiftSequence( revComp, gBaseBits, wordLength, 
		 wordLength - seq.getNumBasesInLast() );

  revComp.setNumBasesInLast(seq.getNumBasesInLast());
} // ~reverseComplement( WordSequence& ...

//  void reverseComplement
//  ( const WordSequence& seq, 
//    WordSequence& revComp,
//    int wordLength )
//    { 
//      return reverseComplement(seq, revComp, wordLength, wordLength); 
//    } // ~reverseComplement( WordSequence& ... 


  // Function Name: shiftSequence
  // Arguments: WordSequence& (in/out), int (in)
  // Returns:   void 
  // This shifts a WordSequence 'i places to the left', i.e. base i+1 of
  // the sequence becomes base 1. The first i bases are lost. 
  void shiftSequence
  ( WordSequence& sequence, int bitsPerSymbol, int wordLength, int i )
  {
    if ( sequence.size() < 1 ) return;

    register Word oldCarry(0), thisWord; 
    register int shiftNum( bitsPerSymbol * (wordLength - i) );
    register Word carryMask
    (    ( ( (unsigned long long)1 << (bitsPerSymbol*i) ) - 1 )  << shiftNum    );

    register Word andMask( ( (unsigned long long)1 << ( bitsPerSymbol * wordLength ) ) - 1 );

    for (int j(sequence.size()-1); j>= 0; j--)
    {
      thisWord = sequence[j];
      sequence[j] 
	= ( ( ( thisWord << (bitsPerSymbol*i) ) & andMask ) | oldCarry );
         oldCarry = (thisWord & carryMask) >> shiftNum ;
      //      oldCarry = ((thisWord & carryMask) >> shiftNum )
      //	| (gCursedWord * ( (thisWord & gCursedWord) != (Word)0 ));
    } // ~for

  } // ~shiftSequence( WordSequence& sequence ) 

void loadFromFile
( const string& fileName, const char* buffer, const unsigned long numBytes,
ostream& monitoringStream_ )
{


    ifstreamSSAHA inFile( fileName.c_str() );
    
    if ( inFile.fail() )
    {
      monitoringStream_ << "Error: failed to open " 
                        << fileName << ", aborting load." << endl;
      throw SSAHAException((string)"Could not open file " + fileName);
    } // ~if

    inFile.read( buffer, numBytes );  

    if (inFile.gcount() != numBytes)
    {
      monitoringStream_ << "Error: expecting " << numBytes  
                        << " bytes, but only " << inFile.gcount()
			<< "were read.\n";
      throw SSAHAException("Insufficient data in file.");
    }

    // check for EOF
    if (inFile.peek()!=EOF)
    {
      monitoringStream_ << "Error: expecting " << numBytes  
                        << " bytes, but more were found in file.\n";
      throw SSAHAException("Too much data in file.");
    }


    monitoringStream_ << "Loaded file " << fileName << " (" 
                      << numBytes
                      << " bytes).\n";

    monitoringStream_ << "Closing file " << fileName << "\n";
    inFile.close();

} // ~loadFromFile

void saveToFile
( const string& fileName, const char* buffer, const unsigned long numBytes,
ostream& monitoringStream_ )
{

    ofstreamSSAHA outFile( fileName.c_str() );
    
    outFile.write( buffer, numBytes );

    if ( outFile.fail() )
    {
      monitoringStream_ << "Error: failed to write " 
                        << fileName << ", aborting save." << endl;
      throw SSAHAException((string)"Problem saving file " + fileName);
    } // ~if

    monitoringStream_ << "Saved file " << fileName << "." << endl;

    outFile.close();

} // ~saveToFile


#ifdef USING_POSIX_MEMORY_MAPPING

const MemoryMapper::Mode MemoryMapper::createMap = 
MemoryMapper::Mode
 ( O_CREAT|O_EXCL|O_RDWR,PROT_READ|PROT_WRITE,MAP_SHARED, true, true );

const MemoryMapper::Mode MemoryMapper::readMap =
MemoryMapper::Mode
// ( O_CREAT|O_EXCL|O_RDWR,PROT_READ|PROT_WRITE,MAP_SHARED, false, false );
 ( O_RDONLY,PROT_READ,MAP_PRIVATE, false, false );



void* MemoryMapper::linkToMap
( const Mode& mode, const string& name, unsigned long numBytes )
{

  //    caddr_t pg_addr;
 
    static const int shareMode =  S_IRWXO|S_IRWXG|S_IRWXU;
 
    // Create a file 
 
    fileDesc_ = shm_open(name.c_str(), mode.shared, shareMode);
    if(fileDesc_ < 0)
    {
      throw SSAHAException("Failed to open shared memory");
    }
 
    // Set the size 
 
    if (mode.truncateFile==true)
    {
      if((ftruncate(fileDesc_, numBytes)) == -1)
      {
	throw SSAHAException("Failed to truncate shared memory file");
      }
    } 

    // Map the file into the address space of the process 
    //    pg_addr = (caddr_t) 
    //   mmap(0, size, mode.access, mode.flags, fileDesc_, 0);
 
      //    if(pg_addr == (caddr_t) -1)
      //  {
      //    perror("mmap failure");
      //     exit(1);
      //   }

    //    (*ptr_)=(char*) pg_addr; 
 
    return  mmap(0, numBytes, mode.access, mode.flags, fileDesc_, 0);
    //    size_=size; 
    //  isAllocated_=true;
} // AllocatorMapped::allocate

#endif




// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

// End of file GlobalDefinitions.cpp



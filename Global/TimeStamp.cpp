
// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : TimeStamp
// File Name    : TimeStamp.cpp
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Description:

// Includes:

// ### Function Definitions ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

#include <fstream>
#include <string>
#include <time.h>
#include <stdlib.h>
#include "TimeStamp.h"


Timer::Timer( void ) 
: numStamps( 0 ), firstStamp( clock() ), lastStamp( firstStamp )
{
}

Timer::~Timer( void )
{
}


ostream& Timer::timeStamp( ostream& os )
{

  clock_t now = clock();

  os << " Time stamp: "  << ++numStamps
     << "\t Since Start: " << now - firstStamp
     << "\t Since Last: "  << now - lastStamp << endl;

  lastStamp = now;
  return os;
}

ostream& operator<<( ostream& os, Timer& timer )
{
  return timer.timeStamp(os);
}



// End of file TimeStamp.cpp






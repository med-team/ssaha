/*  Last edited: Apr 18 18:06 2002 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : TimeStamp
// File Name    : TimeStamp.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_TimeStamp
#define INCLUDED_TimeStamp

using namespace std;
// Description:


// Includes:

#include <fstream>
#include <time.h>

// ### Class Declarations ###


// Class Name : Timer
// Description: Maintains timing info during a run, and outputs it on request.

class Timer
{

  // PUBLIC MEMBER FUNCTIONS
  public:

  // Constructors and Destructors

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  Timer( void );

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  ~Timer(); 
  // (NB destructor should be virtual if class is to be derived from)

  // Manipulator Functions

  // Function Name: timeStamp
  // Arguments: ostream&
  // Returns:   ostream&
  // This functions outputs a message string to the specified output
  // stream consisting of current date and time and the elapsed time in
  // microseconds since the a) the timer was created and b) this
  // method was last called. Can be called directly, but intention is that
  // it is called by overloaded << operator defined below.
  ostream& timeStamp( ostream& os );

  // Accessor Functions
  // (NB all accessor functions should be 'const')

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  
  // PROTECTED MEMBER FUNCTIONS 
  // (visible to this class and derived classes only)
  protected:

  // PRIVATE MEMBER FUNCTIONS
  // (visible to instances of this class only)
  
  private:
  Timer( const Timer&);             // NOT IMPLEMENTED
  Timer& operator=(const Timer&);   // NOT IMPLEMENTED

  // PRIVATE MEMBER DATA
  private:
  int numStamps;
  clock_t firstStamp;
  clock_t lastStamp;

}; // Timer

// ### Function Declarations ###

// Name:      << operator
// Arguments: ostream&, Timer&  
// Returns: ostream&
// Triggers a time stamp and outputs it via os
ostream& operator<<( ostream& os, Timer& timer );

// End of include guard:
#endif

// End of file TimeStamp.h

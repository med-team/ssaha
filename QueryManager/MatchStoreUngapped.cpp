
// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : MatchStoreUngapped
// File Name    : MatchStoreUngapped.cpp
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Description:

// Includes:

#include "MatchStoreUngapped.h"
#include "MatchStoreGapped.h"
#include "SequenceReader.h"
#include "HashTable.h"
#include "GlobalDefinitions.h"
#include <algorithm>

// ### Function Definitions ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT


  // Function Name: findMatch
  // Arguments: vector<Match>& (out), int (in), int (in), int (in)
  // Returns: true if more than minToPrint bases were found in total.
  // Sorts its member HitInfo instances in the following way:
  // 1 (highest priority) by subjectNum: thus the hits for each subject
  // sequence are grouped together
  // 2 by diff: for a given subject sequence, hits which have the same 'diff' 
  // value are part of the same ungapped alignment between the subject
  // sequence and the query sequence.
  // 3 (lowest priority) by queryPos
  // After sorting, the routine looks for a region of hits which have the same
  // subjectNum and diff values but for which the queryPos value differs
  // by stepLength. Each such region denotes a consecutive series of matching 
  // base pairs between the query and subject, and the details are encoded in
  // an instance of AdjacentMatchGap 
  void MatchAlgorithmUngapped::generateMatches
    //  ( HitListVector& hitList, MatchStore& store )
  ( HitListVector& hitList, MatchAdder& addMatch ) 
  {

    //    HitListVector hitList;
    //    subjectTable.matchSequence( querySeq, hitList, numRepeats_ );
    //   subjectTable.setNumRepeats(numRepeats_);
    //  subjectTable.matchSequence( querySeq, hitList );

    if (dynamic_cast<MatchAdderCodonCodon*>(&addMatch)
        ||dynamic_cast<MatchAdderCodonProtein*>(&addMatch))
    {
      wordLength_ *= gNumReadingFrames; // %%%%%%%%%%%%%% these need to be
      stepLength_ *= gNumReadingFrames; // reset to do proteins again %%%
    }

    sort( hitList.begin(), hitList.end() );

    // for (HitListVector::iterator i(hitList.begin()); i != hitList.end();++i)
    //  DEBUG_L2( "hit "<<i->subjectNum<<" "<< i->diff << " " << i->queryPos );

    if ( hitList.size() == 0 ) return;

    HitListVector::const_iterator i ( hitList.begin() );

    int adjacentBases = wordLength_;
    SequenceNumber adjacentStart = i->queryPos;
    int basesToAdd;

    HitListVector::const_iterator lastHit( i );
    ++i;

    for (  ; i != hitList.end() ; ++i )
    {

      if (    ( i->subjectNum == lastHit->subjectNum )
           && ( i->diff == lastHit->diff ) )
      {
	/*
	if ( i->queryPos - lastHit->queryPos == stepLength_ )
	  {
	    adjacentBases += stepLength_;
	  } // ~if
	*/
	basesToAdd = i->queryPos - lastHit->queryPos;
	if (basesToAdd <= stepLength_) 
	  // %%%% chg to stepLength + x to handle gaps in query
	  {
	    adjacentBases += basesToAdd;
	  } // ~if
	
	else
	{
	  if ( adjacentBases>=minToProcess_)
	  {
	    DEBUG_L2(lastHit->subjectNum << " " << 
			  adjacentBases << " " <<
			  adjacentStart << " " <<
			  adjacentStart + adjacentBases -1 << " " <<
			  adjacentStart + lastHit->diff << " " <<
			  adjacentStart + lastHit->diff + adjacentBases -1 );

	    
	    addMatch(	  lastHit->subjectNum,
			  adjacentBases,
			  adjacentStart,
			  adjacentStart + adjacentBases -1,
			  adjacentStart + lastHit->diff,
			  adjacentStart + lastHit->diff + adjacentBases -1 );
	  }
	  adjacentStart = i->queryPos;
	  adjacentBases = wordLength_;
	} // ~else
      }
      else
      {
	if( adjacentBases>=minToProcess_)
	  {
	    DEBUG_L2(lastHit->subjectNum << " " << 
			  adjacentBases << " " <<
			  adjacentStart << " " <<
			  adjacentStart + adjacentBases -1 << " " <<
			  adjacentStart + lastHit->diff << " " <<
			  adjacentStart + lastHit->diff + adjacentBases -1 );
	  
		addMatch(	  lastHit->subjectNum,
			  adjacentBases,
			  adjacentStart,
			  adjacentStart + adjacentBases -1,
			  adjacentStart + lastHit->diff,
			  adjacentStart + lastHit->diff + adjacentBases -1 );
	  }
           adjacentStart = i->queryPos;
           adjacentBases = wordLength_;

      } // ~else
      ++lastHit;
    } // ~for
    if (adjacentBases>=minToProcess_)
      {
	    DEBUG_L2(lastHit->subjectNum << " " << 
			  adjacentBases << " " <<
			  adjacentStart << " " <<
			  adjacentStart + adjacentBases -1 << " " <<
			  adjacentStart + lastHit->diff << " " <<
			  adjacentStart + lastHit->diff + adjacentBases -1 );

	addMatch(		  lastHit->subjectNum,
			  adjacentBases,
			  adjacentStart,
			  adjacentStart + adjacentBases -1,
			  adjacentStart + lastHit->diff,
			  adjacentStart + lastHit->diff + adjacentBases -1 );
      }
    return;

  } // ~findMatch








// End of file MatchStoreUngapped.cpp



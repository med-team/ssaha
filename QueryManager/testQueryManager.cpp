
// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : testQueryManager
// File Name    : testQueryManager.cpp
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Description: test harness for the QueryManagerSSAHA

// Includes:

#include "MatchStoreGapped.h"
#include "MatchStoreUngapped.h"
#include "GenerateTestFastaFiles.h"
#include "SequenceReaderFasta.h"
#include "SequenceReaderString.h"
#include "SequenceReaderMulti.h"
#include "HashTable.h"
#include "HashTablePacked.h"
#include "HashTableTranslated.h"
#include "TimeStamp.h"
#include "MatchStore.h"
#include "MatchAligner.h"

// ### Function Definitions ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

void reverseString( string& seq )
{
  string rc;
  for ( int i(0) ; i < seq.size() ; i++ )
  {
    if  ( ( seq[i] == 'A' ) || (seq[i] == 'a') ) rc = 'T' + rc; 
    else if ( ( seq[i] == 'T' ) || (seq[i] == 't') ) rc = 'A' + rc; 
    else if ( ( seq[i] == 'G' ) || (seq[i] == 'g') ) rc = 'C' + rc; 
    else if ( ( seq[i] == 'C' ) || (seq[i] == 'c') ) rc = 'G' + rc; 
    else throw SSAHAException();
  } // ~for i

  seq = rc;

} // ~reverseString

class MatchTaskTest : public MatchTask
{
public:
  MatchTaskTest
  ( BaseGenerator& bases, int numSeqs, int seqSize ) : 
    bases_( bases ), numSeqs_( numSeqs ), seqSize_( seqSize ) {}
  void operator()(MatchStore& store );

  BaseGenerator bases_;
  int numSeqs_;
  int seqSize_;

};

void print( Alignment& alignment )
{
  cout << "AL: printing alignment data: " << alignment.size() <<endl;
  for (Alignment::iterator i(alignment.begin()); 
       i!=alignment.end();++i)
    cout << i->numMatches << (char)('A'+i->breakType);
  cout << endl << "AL: finished printing alignment data:" << endl;
}


void MatchTaskTest::operator()(MatchStore& store)
{

  store.printResult(cout);

  string queryMatch, subjectMatch;

  for (MatchStore::const_iterator i( store.begin() ) ; i!=store.end() ; ++i )
  {
    cout << store.size() << endl;

    cout << (*i)->getQueryNum() << ": " << (*i)->getQueryName()
	 << " "
     	 << (*i)->getQueryStart() << "-" << (*i)->getQueryEnd() 
	 << "\t= "
	 << (*i)->getSubjectNum() << ": " << (*i)->getSubjectName()
	 << " " 
    	 << (*i)->getSubjectStart() << "-" << (*i)->getSubjectEnd();

    if ( (*i)->isQueryForward() )
    {
      bases_.getBases
	( ( (*i)->getQueryNum() - 1 ) * seqSize_ + (*i)->getQueryStart() - 1,
	  ( (*i)->getQueryEnd() - (*i)->getQueryStart() + 1 ),
	  queryMatch  );
    } // ~if
    else
    {
          bases_.getBases
          ( ( (*i)->getQueryNum() - 1 ) * seqSize_ 
             + ( seqSize_ 
		 - ( (*i)->getQueryStart() + (*i)->getNumBases() - 1 )  ),
             (*i)->getNumBases(),
             queryMatch  );

          reverseString(queryMatch);
    }


  bases_.getBases
  ( ( (*i)->getSubjectNum() - 1 ) * seqSize_ + (*i)->getSubjectStart() - 1,
    ( (*i)->getSubjectEnd() - (*i)->getSubjectStart() + 1 ),
    subjectMatch  );

  assert (queryMatch == subjectMatch);

  //  if( queryMatch == subjectMatch ) cout << "YES!"; 
  //  else cout << "NO!";

  cout << endl;


  } // ~for



};




int main( void )
{

  try {

  int numTests = 0;

  cout << "*******************************************" << endl << endl;
  cout << "     Test of class QueryManagerSSAHA" << endl << endl;
  cout << "*******************************************" << endl << endl;

  int numSeqs = 3;
  int seqSize = 1000;
  int wordLength = 7;
  int maxHits = 50;

  // Generate a random sequence of (numSeqs*seqSize) base pairs ...
  // 1128 is the seed value for the random number generator
  BaseGenerator testBases( numSeqs * seqSize, 1103 );

  // ... make a fasta file from this sequence, breaking the sequence into
  // sequences of seqSize base pairs, with no overlaps (i.e. numSeqs of them)
  // Haven't specified a file name, so will default to test_subject.fasta 
  testBases.generateSubjectFile(seqSize,0);

  // ---

 cout << "Test " << ++numTests 
       <<": test of member function matchAllSequences\n\n";

 Timer clock;

 HashTable hashTable(cout);   

 cerr << clock;

 SequenceReaderFasta subjectReader("test_subject.fasta",cout);
 SequenceReaderFasta queryReader("test_subject.fasta",cout);
 SequenceReaderFasta subjectReader2("test_subject.fasta",cout);
 SequenceReaderFasta queryReader2("test_subject.fasta",cout);

 // #ifdef XXX
  for ( int k( wordLength) ; k > 0 ; k-- )
  {

    cout << "Checking functionality for step length of " << k << " ...\n";

    HashTable hashTable(cout);   
    HashTablePacked hashTablePacked(cout);
    HashTableFactory creator(cout);

    cout << clock;
    
    creator.createHashTable(hashTable,subjectReader,wordLength,20,k);
    creator.createHashTable(hashTablePacked,subjectReader2,wordLength,20,k);

    cout << clock;

    QueryManager queryManager( queryReader,hashTable,cout);
    QueryManager queryManager2( queryReader2,hashTablePacked,cout);

    cout << clock;

    cout << "Matching using MatchStoreUngapped ...\n";

   MatchTaskTest task_(testBases, numSeqs, seqSize);
    {

      MatchAlgorithmUngapped match_(1,0);

      cout << "matching for standard hash table...\n";
      queryManager.doQuery( match_, task_ ); 

      cout << "matching for packed hash table...\n";
      queryManager2.doQuery( match_, task_ ); 

    }
   cout << "Matching using MatchStoreGapped ...\n";
   {

     //     MatchTaskPrint task_; // %%%

     MatchAlgorithmGapped match_(0,0,1,0);

     cout << "matching for standard hash table...\n";
     queryManager.doQuery( match_, task_ ); 

     cout << "matching for packed hash table...\n";
     queryManager2.doQuery( match_, task_ ); 

   }

  } // ~for k
  //  #endif

  {

    SequenceReaderFasta subjectReader("test.fasta",cout);
    SequenceReaderFasta queryReader("test.fasta",cout);

    HashTableFactory creator(cout);
    HashTable hashTable(cout);   

    creator.createHashTable(hashTable,subjectReader,10,20,10);

    QueryManager queryManager( queryReader,hashTable,cout);

    MatchAlgorithmUngapped match(1,0);
  
    //  SortByMatchSize s1;

    //  MatchTaskSort task1(SortByMatchSize);

    CombineTask<MatchTaskSort<SortByMatchSize>,MatchTaskPrintTabbed> task1;

    cout << "sorting results by match size\n"; 

    queryManager.doQuery( match, task1, 2, 2 ); 

    //  SortBySubjectName s2;

    //  MatchTaskSort task2(s2);
    CombineTask<MatchTaskSort<SortBySubjectName>,MatchTaskPrintTabbed> task2;

    cout << "sorting results by subject name\n"; 

    queryManager.doQuery( match, task2, 2, 2 ); 

    typedef CombineSort<SortByMatchSize, SortByMatchSize, SortByMatchSize> 
      CustomSort;

    CombineTask<MatchTaskSort<CustomSort>,MatchTaskPrintTabbed> task3;

    cout << "sorting results by diff then querynum\n"; 

    queryManager.doQuery( match, task3, 2, 2 ); 

  }
  
  cout << "Test passed!\n";

  SequenceReaderStringDNA seqFwd
("cagattacagagaaaggcatttacttagggcttgcttacagtaaacaggactgtatgggcatcagaaatgtgaattttgttcaggatcca",cerr);
//123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

  SequenceReaderStringProtein transFwd0
    ("QITEKGIYLGLAYSKQDCMGIRNVNFVQDP",cerr);
  SequenceReaderStringProtein transFwd1
    ("RLQRKAFT*GLLTVNRTVWASEM*ILFRI",cerr);
  SequenceReaderStringProtein transFwd2
    ("DYRERHLLRACLQ*TGLYGHQKCEFCSGS",cerr);

  SequenceReaderStringDNA seqRev
("tggatcctgaacaaaattcacatttctgatgcccatacagtcctgtttactgtaagcaagccctaagtaaatgcctttctctgtaatctg");
  SequenceReaderStringProtein transRev0
    ("WILNKIHISDAHTVLFTVSKP*VNAFLCNL",cerr);
  SequenceReaderStringProtein transRev1
    ("GS*TKFTFLMPIQSCLL*ASPK*MPFSVI",cerr);
  SequenceReaderStringProtein transRev2
    ("DPEQNSHF*CPYSPVYCKQALSKCLSL*S",cerr);

  SequenceReaderStringDNA subSeq
    ("gtatgggcatcagaaatgtgaatttt",cerr); // bases 53-78 inclusive of seqFwd
  SequenceReaderStringProtein subSeqTrans
    ("VWASEM*I",cerr); // frame 0 codonization of subSeq

  SequenceReaderStringDNA subSeqRev
    ("aaaattcacatttctgatgcccatac",cerr); // RC of subSeq
  SequenceReaderStringProtein subSeqTransRev
    ("KIHISDAH",cerr); // frame 0 codonization of subSeqRev


  SequenceReaderMulti bothDirs(cerr);
  bothDirs.addReader(seqFwd);
  bothDirs.addReader(seqRev);
  bothDirs.addReader(subSeq);
  bothDirs.addReader(subSeqRev);

  SequenceReaderMulti allTrans(cerr);
  allTrans.addReader(transFwd0);
  allTrans.addReader(transFwd1);
  allTrans.addReader(transFwd2);
  allTrans.addReader(transRev0);
  allTrans.addReader(transRev1);
  allTrans.addReader(transRev2);
  allTrans.addReader(subSeqTrans);
  allTrans.addReader(subSeqTransRev);


  HashTableFactory creator(cout);


  // ---

  cout << "Test " << ++numTests 
       <<": test of DNA query against DNA hash table\n\n";

  {
    HashTablePacked hashTable(cout);   

    //    SequenceReaderString subjectReader(bothDirs,cout);
    //  SequenceReaderString queryReader(bothDirs,cout);

    creator.createHashTable(hashTable,bothDirs,10,10000,0);

    QueryManager queryManager( bothDirs,hashTable,cout);

    MatchTaskPrintReverse task_(cout);

    MatchAlgorithmGapped match_(0,0,1,0);

    queryManager.doQuery( match_, task_ ); 

  }
  cout << "Test passed!\n";

  // ---

  cout << "Test " << ++numTests 
       <<": test of protein query against protein hash table\n\n";
  {
    HashTablePackedProtein hashTable(cout);   

    //    SequenceReaderStringProtein subjectReader(allTrans,cout);
    //  SequenceReaderStringProtein queryReader(allTrans,cout);

    creator.createHashTable(hashTable,allTrans,4,10000,0);

    QueryManager queryManager( allTrans,hashTable,cout);

    MatchTaskPrintReverse task_(cout);

    MatchAlgorithmGapped match_(0,0,1,0);

    queryManager.doQuery( match_, task_ ); 

  }

  cout << "Test passed!\n";

  // ---

  cout << "Test " << ++numTests 
       <<": test of translated DNA query against protein hash table\n\n";
  {
    HashTablePackedProtein hashTable(cout);   

    //    SequenceReaderStringProtein subjectReader(AllTrans,cout);
    //    SequenceReaderStringDNA queryReader(bothDirs,cout);

    creator.createHashTable(hashTable,allTrans,4,10000,0);

    QueryManager queryManager( bothDirs,hashTable,cout);

    MatchTaskPrintReverse task_(cout);

    MatchAlgorithmGapped match_(0,0,1,0);

    queryManager.doQuery( match_, task_ ); 

  }

  cout << "Test passed!\n";

  // ---

  cout << "Test " << ++numTests 
       <<": test of protein query against translated DNA hash table\n\n";
  {
    HashTableTranslated hashTable(cout);   

    //    SequenceReaderStringProtein subjectReader(AllTrans,cout);
    //    SequenceReaderStringDNA queryReader(bothDirs,cout);

    creator.createHashTable(hashTable,bothDirs,4,10000,0);

    QueryManager queryManager( allTrans,hashTable,cout);

    MatchTaskPrintReverse task_(cout);

    MatchAlgorithmGapped match_(0,0,1,0);

    queryManager.doQuery( match_, task_ ); 

  }


  cout << "Test passed!\n";

  // ---

  cout
    << "Test " << ++numTests 
    << ": test of translated DNA query against translated DNA hash table\n\n";
  {
    HashTableTranslated hashTable(cout);

    creator.createHashTable(hashTable,bothDirs,4,10000,0);

    QueryManager queryManager( bothDirs,hashTable,cout);

    MatchTaskPrintReverse task_(cout);

    MatchAlgorithmGapped match_(0,0,1,0);

    queryManager.doQuery( match_, task_ ); 

  }


  cout << "Test passed!\n";
  //#endif

  // ---

  cout << "Test " << ++numTests 
       <<": test of MatchTaskAlign for DNA\n\n";

  {
    {
      Alignment check;
      MatchAlignerDNA aligner(80, 0, cout);
      MatchAlignerDNA aligner1(80, 1, cout);
      MatchAlignerDNA aligner2(80, 2, cout);

      string p1="agctctAt";
      string p2="aAgctctt";

      aligner2(p1.c_str(), 1, 8, p2.c_str(), 101, 108);
      aligner2(p2.c_str(), 101, 108, p1.c_str(), 1, 8);

      check.clear();
      aligner.createAlignment(check, p1.c_str(), 1, 8, p2.c_str(), 101, 108);
      print( check );
      //      assert(check.size()==6);

      p1 = "aaaaa";
      p2 = "AAGAAA";

      aligner2(p1.c_str(), 1, 5, p2.c_str(), 101, 106);
      aligner2(p2.c_str(), 101, 106, p1.c_str(), 1, 5);

      check.clear();
      aligner.createAlignment(check, p1.c_str(), 1, 5, p2.c_str(), 101, 106);
      print( check );


      p1 = "gcgcg";
      p2 = "gcgtcgt";
      aligner2(p1.c_str(), 1, 5, p2.c_str(), 101, 107);
      aligner2(p2.c_str(), 101, 107, p1.c_str(), 1, 5);

      check.clear();
      aligner.createAlignment(check, p1.c_str(), 1, 5, p2.c_str(), 101, 107);
      print( check );

      const char* pQuery="ctcttttttttttttttttccgagatggagtttttcactgttgttgcccaggctggagtgcaatggcgcaatctccgttcactgcaacctccgcctcccgggttcaagtgattctcctgcctcagcctcccgagtagctgggattacaggtacctgccaccatacccggctaatttttgtattttcagtagagacagggtatcaccatgttggccaggctggtctcgaactcctgatctcaggtgatccacctccctcggcctcccaaagtgctggg";
      const char* pSubject="CTCTTTTTTTTTTTTTTTTTTTTTTTTTTGAGATGGAGTCTCACTCTGTCTCCCAGGCTGGAGTGCAATGGTACAATCTCGGCTCACTGCAACCTCCGCCTTTCGGGTTCAAGCAATTCTCCTGCCTCAGCCTCCCGAGTAGCTGGGAGTACAGGCACGCACCACCCCACCCGGCTAATTTTTATTACTAGTAGAGACTGGGTTTCACCATGTTGGCCAGGCTGGTCTTGAACTCCTGACCTCAGGTGATCCGCCCACCTCGGCCTCCCAAAGTGCTGGG";
      
      for ( int i(0) ; i < 5; i++ )
      {
	MatchAlignerDNA aligner(80, i, cout);

	cout << "band extension = " << i<< endl;
	aligner(pQuery, 274, 550, pSubject, 6077, 6356);
      }

      check.clear();
      aligner.createAlignment(check, pQuery, 274, 550, pSubject, 6077, 6356);
      print( check );


    }
    {

      HashTablePacked hashTable(cout);   
      
      SequenceReaderFasta subjectReader("test.fasta",cout);
      SequenceReaderFasta queryReader("test.fasta",cout);
      SequenceReaderModeReplace mode('A');
      subjectReader.changeMode( &mode );
      queryReader.changeMode( &mode );

      creator.createHashTable(hashTable,subjectReader,10,10000,0);

      QueryManager queryManager( queryReader,hashTable,cout);

      //      MatchTaskAlignDNA task_(queryReader,subjectReader,80,cout);
      MatchTaskAlign task_
	(queryReader,subjectReader,
	 new MatchAlignerDNA( 80, 0 ),true, true);

      MatchAlgorithmGapped match_(23,3,1,0);

      queryManager.doQuery( match_, task_ ); 
    }
  }
  cout << "Test passed!\n";

  // ---

  // ---

  cout << "Test " << ++numTests 
       <<": test of MatchTaskAlign for protein\n\n";

  {
    Alignment check;

      MatchAlignerProtein aligner(80, 0, cout);
      // >P1;CBYB55 cytochrome b559 component psbE - Synechocystis sp. (strain PCC 6803)

      const char* pQuery   ="MSGTTGERPFSDIVTSIRYWVIHSITIPMLFIAGWLFVSTGLAYDAFGTPRPDEYFTQTRARNDCQEGHILKMFPSTWYVBZXq";
      const char* pSubject ="msgttgerpfsdivtsirywvihsitipmlfiagwlfvstglaydafgtprpdeyftqtrarndcqeghilkmfpstwyvbzxq";
      const char* pSubject2="msgttgerpfsWivtsirywvihsitipmlfiagDwlfvstglaydafgtprpdeyftqtrarndcqeghilkmfpstwyvbzxq";
      //                     12345678901234567890123456789012345678901234567890123456789012345678901234567890
      aligner(pQuery, 101, 184, pSubject, 5001, 5084);

      check.clear();
      aligner.createAlignment(check, pQuery, 101, 184, pSubject, 5001, 5084);
      print( check );

      aligner(pQuery, 101, 184, pSubject2, 5001, 5085);

      check.clear();
      aligner.createAlignment(check, pQuery, 101, 184, pSubject, 5001, 5085);
      print( check );

  }
  cout << "Test passed!\n";

  // ---

  cout << "Test " << ++numTests 
       <<": test of MatchAlignerTranslatedProtein \n\n";

  {

    //      const char* pProt   ="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    //      const char* pProt   ="*ACDEFGHIKLMNPQRSTVWY";
      //     const char* pDNA ="aaabbbcccdddeeefffggghhhiiijjjkkklllmmmnnnooopppqqqrrrssstttuuuvvvwwwxxxyyyzzz";
    //     const char* pDNA ="taagcatgtgatgaatttggtcacataaaacttatgaatccccagcgttcaaccgtttggtat";

      MatchAlignerTranslatedProtein aligner1(true, 60, 2, cout);
      MatchAlignerTranslatedProtein aligner2(false, 60, 2, cout);

      cout << "Test case: exact match" << endl;

      string pProt   ="*ACDEFGHIKLMNPQRSTVWY";
      string pDNA ="taagcatgtgatgaatttggtcacataaaacttatgaatccccagcgttcaaccgtttggtat";

      aligner1(pProt.c_str(), 101, 121, pDNA.c_str(), 1001, 1063);
      aligner2(pDNA.c_str(), 1001, 1063, pProt.c_str(), 101, 121);

      cout << "Test case: codon mismatch" << endl;

      // mismatched codon
      pProt   ="*ACDEFGHIKyMNPQRSTVWY";
             pDNA ="taagcatgtgatgaatttggtcacataaaacttatgaatccccagcgttcaaccgtttggtat";
      //     *..A..C..D..E..F..G..H..I..K..

      aligner1(pProt.c_str(), 101, 121, pDNA.c_str(), 1001, 1063);
      aligner2(pDNA.c_str(), 1001, 1063, pProt.c_str(), 101, 121);

      cout << "Test case: frame shift" << endl;
      
      // extra nucleotide
      pProt   ="*ACDEFGHIKLMNPQRSTVWY";
      pDNA ="taagcatgtgatgaatttggtcacataaaaActtatgaatccccagcgttcaaccgtttggtat";
      //     1..2..3..4..5..6..7..8..9..0..1..

      aligner1(pProt.c_str(), 101, 121, pDNA.c_str(), 1001, 1064);
      aligner2(pDNA.c_str(), 1001, 1064, pProt.c_str(), 101, 121);

      cout << "Test case: 2 frame shifts" << endl;
      
      // extra nucleotides
      pProt   ="*ACDEFGHIKLMNPQRSTVWY";
      pDNA 
	="taagcatgtgatgaatttggtcacataaaaActtatgaatccccagcgttcGaaccgtttggtat";
      //  1..2..3..4..5..6..7..8..9..0..1..

      aligner1(pProt.c_str(), 101, 121, pDNA.c_str(), 1001, 1065);
      aligner2(pDNA.c_str(), 1001, 1065, pProt.c_str(), 101, 121);

      cout << "Test case: 2 more frame shifts" << endl;
      
      // extra nucleotide
      pProt   ="*ACDEFGHIKLMNPQRSTVWY";
      //        01234567890123456789
      pDNA 
	="taagcatgtgCatgaatttggtcacataaaaActtatgaatccccagcgttcaaccgtttggtat";
      //     1..2..3..4..5..6..7..8..9..0..1..

      aligner1(pProt.c_str(), 101, 121, pDNA.c_str(), 1001, 1065);
      aligner2(pDNA.c_str(), 1001, 1065, pProt.c_str(), 101, 121);

      cout << "Test case: 3 frame shifts" << endl;
      
      pProt   ="*ACDEFGHIKLMNPQRSTVWY";
      //        01234567890123456789
      pDNA 
	="taagcatgCtgatgaatttggtcacataaaaActtatgaatccccagcgttcGaaccgtttggtat";
      //  0..1..2. .3..4..5..6..7..8..9.. 0..1..2..3..4..5..6. .7..8..9..0..1..

      aligner1(pProt.c_str(), 101, 121, pDNA.c_str(), 1001, 1066);
      aligner2(pDNA.c_str(), 1001, 1066, pProt.c_str(), 101, 121);

      cout << "Test case: 3 frame shifts and extra amino" << endl;
      
      // extra nucleotide
      pProt   ="*ACDEFGHIKLMNPQRSTVWYc";
      pDNA 
	="taagcatgCtgatgaatttggtcacataaaaActtatgaatccccagcgttcGaaccgtttggtat";
      //     1..2..3..4..5..6..7..8..9..0..1..

      aligner1(pProt.c_str(), 101, 122, pDNA.c_str(), 1001, 1066);
      aligner2(pDNA.c_str(), 1001, 1066, pProt.c_str(), 101, 122);

      cout << "Test case: extra amino acid" << endl;

      // extra amino acid
      pProt   ="*ACDEFGHIKyLMNPQRSTVWY";
      pDNA ="taagcatgtgatgaatttggtcacataaaacttatgaatccccagcgttcaaccgtttggtat";
      //     1..2..3..4..5..6..7..8..9..0..1..

      aligner1(pProt.c_str(), 101, 122, pDNA.c_str(), 1001, 1063);
      aligner2(pDNA.c_str(), 1001, 1063, pProt.c_str(), 101, 122);

      cout << "Test case: missing amino acid" << endl;

      pProt   ="*ACDEFGHILMNPQRSTVWY"; // got rid of K
      pDNA ="taagcatgtgatgaatttggtcacataaaacttatgaatccccagcgttcaaccgtttggtat";
      //     1..2..3..4..5..6..7..8..9..0..1..

      aligner1(pProt.c_str(), 101, 120, pDNA.c_str(), 1001, 1063);
      aligner2(pDNA.c_str(), 1001, 1063, pProt.c_str(), 101, 120);


      cout << "Test case: missing amino acids" << endl;

      pProt   ="*ACDEFGHINPQRSTVWY"; // got rid of KLM
      pDNA ="taagcatgtgatgaatttggtcacataaaacttatgaatccccagcgttcaaccgtttggtat";
      //     1..2..3..4..5..6..7..8..9..0..1..

      aligner1(pProt.c_str(), 101, 118, pDNA.c_str(), 1001, 1063);
      aligner2(pDNA.c_str(), 1001, 1063, pProt.c_str(), 101, 118);

      cout << "Test case: extra amino acids" << endl;
      pProt   ="*ACDEFGHIKyacLMNPQRSTVWY";
      pDNA ="taagcatgtgatgaatttggtcacataaaacttatgaatccccagcgttcaaccgtttggtat";
      //     1..2..3..4..5..6..7..8..9..0..1..

      aligner1(pProt.c_str(), 101, 124, pDNA.c_str(), 1001, 1063);
      aligner2(pDNA.c_str(), 1001, 1063, pProt.c_str(), 101, 124);





  }
  cout << "Test passed!\n";

  // ---

  cout << "Test " << ++numTests 
       <<": test of MatchAlignerTranslatedDNA \n\n";

  {
    MatchAlignerTranslatedDNA aligner(60, 0, cout);
    MatchAlignerTranslatedDNA aligner1(60, 1, cout);
    MatchAlignerTranslatedDNA alignerx(60, 4, cout);
    cout << "Test case: simple" << endl;
    string pDNA1 
      = "taggcatgcgatgaattcgggcatataaaactaatgaatccccaacgtagcacggtatggtac";
    string pDNA2 
      = "tgagcctgtgacgagtttggacacattaagttgatgaacccacagagatctactgtgtggtat";
    //   123456789012345678901234567890123456789012345678901234567890


    aligner(pDNA1.c_str(), 1001, 1063, pDNA2.c_str(), 1001, 1063);

    cout << "Test case: extra base" << endl;
    pDNA1 
      = "gttttttccaccacatattgtcctactactgtgtaccctgtctcagta";
    //   123456789012345678901234567890123456789012345678901234567890
    //   L..S..F..T..R..S..C..T..V..Q..E..Q..K..N..V..A..
    pDNA2 
      = "gttttttccaccacatattgtcctactactAgtgtaccctgtctcagta";

    alignerx(pDNA1.c_str(), 1001, 1048, pDNA2.c_str(), 1001, 1049);
    alignerx(pDNA2.c_str(), 1001, 1049, pDNA1.c_str(), 1001, 1048);


    cout << "Test case: 2 extra bases" << endl;
    pDNA1 
      = "gttttttccaccacatattgtcctactactgtgtaccctgtctcagta";
    //   123456789012345678901234567890123456789012345678901234567890
    //   L..S..F..T..R..S..C..T..V..Q..E..Q..K..N..V..A..
    pDNA2 
      = "gttttttccaccGacatattgtcctactactgtgtaccctAgtctcagta";

    alignerx(pDNA1.c_str(), 1001, 1048, pDNA2.c_str(), 1001, 1050);
    alignerx(pDNA2.c_str(), 1001, 1050, pDNA1.c_str(), 1001, 1048);

    cout << "Test case: another 2 extra bases" << endl;
    pDNA1 
      = "gttttttccaccacatattgtcctactactgtgtaccctgtctcagta";
    //   123456789012345678901234567890123456789012345678901234567890
    //   L..S..F..T..R..S..C..T..V..Q..E..Q..K..N..V..A..
    pDNA2 
      = "gttttttccaccacatattgtcGctactactgAtgtaccctgtctcagta";

    alignerx(pDNA1.c_str(), 1001, 1048, pDNA2.c_str(), 1001, 1050);
    alignerx(pDNA2.c_str(), 1001, 1050, pDNA1.c_str(), 1001, 1048);

    cout << "Test case: extra base each side" << endl;
    pDNA1 
      = "taggcatgcgatCgaattcgggcatataaaactaatgaatccccaacgtagcacggtatggtac";
    pDNA2 
      = "tgagcctgtgacgagtttggacacattaagttgatgaacccacagagatActactgtgtggtat";
    //   123456789012345678901234567890123456789012345678901234567890


    alignerx(pDNA1.c_str(), 1001, 1064, pDNA2.c_str(), 1001, 1064);
    alignerx(pDNA2.c_str(), 1001, 1064, pDNA1.c_str(), 1001, 1064);

    cout << "Test case: 3 extra bases" << endl;

    pDNA1 
      = "gttttttccaccacatattgtcctactAGCTTGCAAAactgtgtacGACGTAcctgtctcagta";
    //   123456789012345678901234567890123456789012345678901234567890
    //   L..S..F..T..R..S..C..T..V..Q..E..Q..K..N..V..A..
    pDNA2 
      = "gttttttccaccacatattgtcActactAGCTTGCAAAactGgtgtacGACGTAcctgCtctcagta";

    alignerx(pDNA1.c_str(), 1001, 1064, pDNA2.c_str(), 1001, 1067);
    alignerx(pDNA2.c_str(), 1001, 1067, pDNA1.c_str(), 1001, 1064);



  }
  cout << "Test passed!\n";

  // ---


  cerr << clock;

  } // ~try
  catch ( const std::exception& err )
  {
    cout << "Caught exception: " << err.what() << "\n";
  } // ~catch

  return (0);
}
// End of file testQueryManager.cpp









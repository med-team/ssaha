/*  Last edited: May 29 16:58 2002 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : QueryManagerSSAHA
// File Name    : QueryManagerSSAHA.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_MatchStoreGapped
#define INCLUDED_MatchStoreGapped

// Description:

// Includes:

#include "QueryManager.h"
#include "GlobalDefinitions.h"
#include "HashTableGeneric.h" // %%%%%
#include "MatchStore.h"
#include <vector>
#include <string>
#include <utility> // ... for 'pair'
#include <map>
#include <iosfwd>
// NB it is good practise for #include statements in header files to be
// replaced by forward declarations if at all possible


// ### Class Declarations ###

// MatchAlgorithm and subclasses

class HitListVector;
class MatchAlgorithm
{
 public:
  MatchAlgorithm( int numRepeats ) : 
    numRepeats_( numRepeats ), sortNeeded_(true) {}
  void operator()  
    ( WordSequence& querySeq, 
      MatchAdder& addMatch, 
      HashTableGeneric& subjectTable, 
      int wordLength = -1, 
      int stepLength = -1);
  void generateHits( WordSequence& querySeq, HitListVector& hitList,
		     HashTableGeneric& subjectTable );
  virtual void generateMatches  
    ( HitListVector& hitList, MatchAdder& addMatch )=0;

 protected:
  bool sortNeeded_;
  int numRepeats_;
  int wordLength_;
  int stepLength_;
};


class MatchAlgorithmGapped : public MatchAlgorithm
{
 public:
  MatchAlgorithmGapped
    ( int maxGap, int maxInsert, int minToProcess, int numRepeats ):
    maxGap_( maxGap ),
    maxInsert_( maxInsert ), 
    minToProcess_( minToProcess ),
    MatchAlgorithm( numRepeats )
    {}

  virtual void generateMatches
  ( HitListVector& hitList, MatchAdder& addMatch );

  void findMatchesInRange
  ( HitListVector::iterator first, HitListVector::iterator last,
    MatchAdder& addMatch );


 private:
  int maxGap_;
  int maxInsert_;
  int minToProcess_;
  int maxQueryDiff_;
  int minHitsForMatch_;
  // At least this number of hits are necessary to get a match
  // of length minToProcess_
  // exact figure is 1+( minToProcess_ - wordLength_ )/stepLength_


};




// ### Function Declarations ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

// End of include guard:
#endif

// End of file MatchStoreGapped.h





/*  Last edited: May 18 16:49 2001 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : MatchStoreUngapped
// File Name    : MatchStoreUngapped.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_MatchStoreUngapped
#define INCLUDED_MatchStoreUngapped

// Description:

// Includes:

#include "QueryManager.h"
#include "GlobalDefinitions.h"
#include <vector>
#include <utility> // ... for 'pair'
#include <iosfwd>
#include <string>
#include "MatchStore.h"
#include "HashTable.h"
#include "MatchStoreGapped.h"
// NB it is good practise for #include statements in header files to be
// replaced by forward declarations if at all possible


// ### Class Declarations ###






class MatchAlgorithmUngapped : public MatchAlgorithm
{
 public:
  MatchAlgorithmUngapped
    ( int minToProcess, int numRepeats ):
    minToProcess_( minToProcess ),
    MatchAlgorithm( numRepeats )
    {}

  virtual void generateMatches
  ( HitListVector& hitList, MatchAdder& addMatch ); 


 private:
  int minToProcess_;
};


// ### Function Declarations ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

// End of include guard:
#endif

// End of file MatchStoreUngapped.h

/*  Last edited: Mar 25 15:35 2002 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : QueryManager
// File Name    : QueryManager.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_QueryManager
#define INCLUDED_QueryManager

// Description:

// Includes:
#include "MatchStore.h"

// NB it is good practise for #include statements in header files to be
// replaced by forward declarations if at all possible
class SequenceReader;
class HashTableGeneric;
class HashTableTranslated;
class HashTablePackedProtein;
class MatchAlgorithm;
#include "GlobalDefinitions.h"
#include "MatchStore.h"
#include <iosfwd>
#include <string>


// ### Class Declarations ###

#ifdef MOVED_TO_GLOBAL_DEFINITIONS
// Struct Name: HitInfo
// Description: This contains all info for a 'hit', i.e. when a word from
// the query sequence matches a word in one of the subject sequences. 
struct HitInfo
{
  // subjectNum: number of the sequence in the subject database
  SequenceNumber subjectNum;
  // diff: this is defined as 
  //   { position of matching word in subject sequence }
  // - { position of matching word in query sequence }
  SequenceOffset diff;
  // queryPos: this is defined as the position of the matching word in the
  // query sequence.
  SequenceOffset queryPos;

  // Simple constructor
  HitInfo( PositionInDatabase inputHitPos, SequenceOffset inputQueryPos ) :
  subjectNum( inputHitPos.sequence ),
  diff( inputHitPos.offset - inputQueryPos ),
  queryPos( inputQueryPos + 1 ) {}

  // '<' operator must be defined to enable vectors of HitInfo instances
  // to be sorted
  bool operator<( const HitInfo& hit) const
  {
    return (    ( subjectNum < hit.subjectNum )
             || (    ( subjectNum == hit.subjectNum )
                  && (    ( diff < hit.diff )      
                       || (    ( diff == hit.diff )      
                            && ( queryPos < hit.queryPos )  )  )  )  );
  } // ~operator<

  bool operator==( const HitInfo& hit) const
  {
    return (    ( subjectNum == hit.subjectNum )
             && ( diff == hit.diff   ) 
             && ( queryPos == hit.queryPos )   );
  } // ~operator<

};
#endif

class LessThanQuery
{
  public:
  bool operator()( const HitInfo& lhs, const HitInfo& rhs ) const
  {
    return ( lhs.queryPos < rhs.queryPos ); 
    //  return ( lhs.queryPos + lhs.diff < rhs.queryPos + rhs.diff ); // %%%%%

  }
};

class LessThanSubject
{
  public:
  bool operator()( const HitInfo& lhs, const HitInfo& rhs ) const
  {
    return (    ( lhs.subjectNum < rhs.subjectNum )
             || (    ( lhs.subjectNum == rhs.subjectNum )
                  && ( lhs.diff < rhs.diff )   )   );   
  }
};



#ifdef MOVED_TO_GLOBAL_DEFINITIONS
// Class Name : HitListVector
// Description: This class is a basic storage class for database hit
// information. Inherits interface from HitList and implementation
// from the STL vector class.
class HitListVector : 
public vector<HitInfo>, public HitList
{
 public:
  virtual void addHit( const PositionInDatabase& hitPos, 
               const SequenceOffset& queryPos ) 
  { push_back( HitInfo( hitPos,queryPos ) ); }

}; // ~HitListVector
#endif

// Match Adder & subclasses

class MatchAdder
{
 public:
  MatchAdder( HashTableGeneric& subjectTable ) :
    isQueryForward_(true),
    //    isSubjectForward_(true), 
    //    readFrame_(0), 
    pStore_(NULL), 
    subjectTable_( subjectTable ) {}
  virtual void operator()(  SequenceNumber subjectNum,
			    SequenceOffset numBases,
			    SequenceOffset queryStart,
			    SequenceOffset queryEnd,
			    SequenceOffset subjectStart,
			    SequenceOffset subjectEnd )=0;
  void link( MatchStore& store ) { pStore_ = &store; }
  void setQueryForward( void ) { isQueryForward_=true; }
  void setQueryReverse( void ) { isQueryForward_=false; }
  //  void setSubjectForward( void ) { isSubjectForward_=true; }
  //  void setSubjectReverse( void ) { isSubjectForward_=false; }
  void setQuerySize( int size ) { querySize_=size; }
  //  void setReadFrame( int frame ) { readFrame_ = frame; }
protected:
  bool isQueryForward_;
  //  bool isSubjectForward_;
  //  int  readFrame_;
  int  querySize_;
  MatchStore* pStore_;
  HashTableGeneric& subjectTable_;
};


class MatchAdderImp : public MatchAdder
{
public:
  MatchAdderImp( HashTableGeneric& subjectTable ) : 
    lastSubjectNum_(0), name_(), MatchAdder( subjectTable ) {}
    virtual void operator()(  SequenceNumber subjectNum,
				     SequenceOffset numBases,
				     SequenceOffset queryStart,
				     SequenceOffset queryEnd,
				     SequenceOffset subjectStart,
				     SequenceOffset subjectEnd );


protected:
  string name_;
  SequenceNumber lastSubjectNum_;
};


// codon query against protein hash table
class MatchAdderCodonProtein : public MatchAdderImp
{
public:
  MatchAdderCodonProtein( HashTableGeneric& subjectTable );
  virtual void operator()(  SequenceNumber subjectNum,
				     SequenceOffset numBases,
				     SequenceOffset queryStart,
				     SequenceOffset queryEnd,
				     SequenceOffset subjectStart,
				     SequenceOffset subjectEnd );

};

// protein query against codon hash table
class MatchAdderProteinCodon : public MatchAdderImp
{
public:
  MatchAdderProteinCodon( HashTableTranslated& subjectTable );
  virtual void operator()(  SequenceNumber subjectNum,
				     SequenceOffset numBases,
				     SequenceOffset queryStart,
				     SequenceOffset queryEnd,
				     SequenceOffset subjectStart,
				     SequenceOffset subjectEnd );
 private:
  SequenceOffset size_;
  HashTableTranslated& subjectTable_;

};

// codon query against codon hash table
class MatchAdderCodonCodon : public MatchAdderImp
{
  public:
  MatchAdderCodonCodon( HashTableTranslated& subjectTable );
  virtual void operator()(  SequenceNumber subjectNum,
				     SequenceOffset numBases,
				     SequenceOffset queryStart,
				     SequenceOffset queryEnd,
				     SequenceOffset subjectStart,
				     SequenceOffset subjectEnd );

 private:
  SequenceOffset size_;
  HashTableTranslated& subjectTable_;
};

// MatchPolicy and subclasses

class MatchPolicy
{
 public:
  MatchPolicy( HashTableGeneric& subjectTable );
  virtual ~MatchPolicy() { delete addMatch_; }

  virtual void operator()
  ( WordSequence& querySeqFwd, MatchStore& store, 
    MatchAlgorithm& findMatch ) =0;
  int getWordLength( void ) const { return queryWordLength_; }
 protected:
  int queryWordLength_; 
  HashTableGeneric& subjectTable_;
  MatchAdder* addMatch_;

};


class MatchPolicyDNADNA : public MatchPolicy 
{
 public:
  MatchPolicyDNADNA( HashTableGeneric& subjectTable );

  virtual void operator()
  ( WordSequence& querySeqFwd, 
    MatchStore& store, MatchAlgorithm& findMatch );

};

class MatchPolicyProteinProtein : public MatchPolicy 
{
 public:
  MatchPolicyProteinProtein( HashTablePackedProtein& subjectTable );

  virtual void operator()
  ( WordSequence& querySeqFwd, 
    MatchStore& store, MatchAlgorithm& findMatch );

 protected:
  HashTablePackedProtein& subjectTable_;
};

class SequenceEncoderCodon;

class MatchPolicyDNAProtein : public MatchPolicy 
{
 public:
  MatchPolicyDNAProtein( HashTablePackedProtein& subjectTable );

  virtual void operator()
  ( WordSequence& querySeqFwd, 
    MatchStore& store, MatchAlgorithm& findMatch );


 protected:
  HashTablePackedProtein& subjectTable_;
};

class MatchPolicyDNATranslated : public MatchPolicy 
{
 public:
  MatchPolicyDNATranslated( HashTableTranslated& subjectTable );

  virtual void operator()
  ( WordSequence& querySeqFwd, 
    MatchStore& store, MatchAlgorithm& findMatch );

 protected:
  HashTableTranslated& subjectTable_;
};


class MatchPolicyProteinTranslated : public MatchPolicy 
{
 public:
  MatchPolicyProteinTranslated( HashTableTranslated& subjectTable ) ;

  virtual void operator()
  ( WordSequence& querySeqFwd, 
    MatchStore& store, MatchAlgorithm& findMatch );


 protected:
  HashTableTranslated& subjectTable_;
};


// QueryManager and subclasses

// Class Name : QueryManager
// Description: This class binds together an instance of SequenceReader and
// an instance of HashTableGeneric. 

class QueryManager
{

  // PUBLIC MEMBER FUNCTIONS
  public:

  // Constructors and Destructors

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
    QueryManager
    ( SequenceReader& querySeqs,  HashTableGeneric& subjectSeqs, 
      ostream& monitoringStream = cerr);

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  virtual ~QueryManager(); 
  // (NB destructor should be virtual if class is to be derived from)

  // Manipulator Functions

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  void doQuery
  ( MatchAlgorithm& match, 
    //  ( MatchStore& store, 
    MatchTask& task,
    int queryStart = 1, 
    int queryEnd = -1 );
  
  // Accessor Functions
  // (NB all accessor functions should be 'const')

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT

  // PROTECTED MEMBER FUNCTIONS 
  // (visible to this class and derived classes only)
  protected:

  // PRIVATE MEMBER FUNCTIONS
  // (visible to instances of this class only)
  
  private:
  QueryManager( const QueryManager&);             // NOT IMPLEMENTED
  QueryManager& operator=(const QueryManager&);   // NOT IMPLEMENTED

  public:
  HashTableGeneric& subjectTable_;       
  ostream& monitoringStream_;


  // PROTECTED MEMBER DATA
  // (visible to this class and derived classes only)
  //  protected:
 protected:  
  SequenceReader& 	queryReader_;
  MatchPolicy* policy_;

  // PRIVATE MEMBER DATA
  private:


}; // QueryManager






// ### Function Declarations ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

// End of include guard:
#endif

// End of file QueryManager.h




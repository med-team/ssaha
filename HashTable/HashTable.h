/*  Last edited: Mar 22 11:36 2002 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : HashTable
// File Name    : HashTable.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_HashTable
#define INCLUDED_HashTable

// Description:

// Includes:
#include "HashTableGeneric.h"

class WordSequenceShifted;

class HashTable : public HashTableView<PositionInDatabase,HashTable>
{
  static AllocatorLocal<PositionInHitList> defaultArrayAllocator;
  static AllocatorLocal<PositionInDatabase> defaultHitListAllocator;
 public:
  typedef void (HashTable::* MatchSequencePointer )( WordSequence&, HitList& );
  
  //  HashTable( ostream& monitoringStream=cerr) :
  //    HashTableView<PositionInDatabase,HashTable>(monitoringStream), 
  //   pMatchSequence_(&HashTable::matchSequenceStandard),
  //   numRepeats_(0){}
  HashTable( ostream& monitoringStream=cerr,
	     string name="",
	     Allocator<PositionInDatabase>& hitListAllocator 
	     = defaultHitListAllocator,
	     Allocator<PositionInHitList>& arrayAllocator 
	     = defaultArrayAllocator ):
  HashTableView<PositionInDatabase,HashTable>
    (monitoringStream, name, hitListAllocator, arrayAllocator), 
  pMatchSequence_(&HashTable::matchSequenceStandard),
  numRepeats_(0)
  {
    hitListFormat_ = gStandard;
    monitoringStream_ << "constructing HashTable\n";
  }

  inline static SequenceNumber getSequence( const_iterator i );
  inline static SequenceOffset getOffset( const_iterator i );


  // Function Name: matchWord
  // Arguments: Word (in), HitList& (out)
  // Populates hitsFound with the positions in the subject sequence database 
  // of all occurrences of the Word queryWord.
  //  void matchWord
  //  ( Word queryWord, HitList& hitsFound, int baseOffset=0 ) const;

  // Function Name: matchWord
  // Arguments: WordSequence& (in), HitList& (out)
  // Populates hitsFound with the positions in the database of all occurrences
  // of the Words in the WordSequence queryWords. baseOffset is the initial 
  // shift in base pairs to be subtracted from all hit positions (TBD explain
  // this better!)
  //  void matchWord
  // ( const WordSequence& queryWords, 
  //   HitList& hitsFound, 
  //   int baseOffset = 0 ) const;


  virtual void setNumRepeats( int numRepeats );

  virtual void matchSequence
  ( WordSequence& seq, HitList& hitListFwd )
  { (this->*pMatchSequence_)(seq, hitListFwd); }

  //  void screenRepeats
  //   ( WordSequenceShifted& seq, HitList& hitsOut, int numRepeats );

  virtual void hashWords
  ( SequenceAdapter& thisSeq, SequenceNumber seqNum );
  virtual void countWords( SequenceAdapter& thisSeq );

  // protected:
  MatchSequencePointer pMatchSequence_;
  int numRepeats_;
  // Function Name: matchSequence
  // Arguments: WordSequence& (in), HitList& (out), HitList& (out)
  // Returns: void
  // This obtains the full list of hits for a sequence in both forward
  // and reverse directions. Proceeds as follows:
  // 1. The reverse complement of the sequence is formed.
  // 2. Any hits found in the forward or reverse direction are added to the
  // appropriate list.
  // 3. The sequence and reverse complement are left-shifted by 1 base
  // Steps 2 and 3 are repeated wordLength_ times.
  // NB This function will modify seq. If you want to keep it, make a copy
  // before calling this function.
  void matchSequenceStandard
  ( WordSequence& seq, HitList& hitListFwd );

  // Function Name: matchSequence
  // Arguments: WordSequence& (in), HitList& (out), HitList& (out), int (in)
  // Returns: void
  // This obtains the full list of hits for a sequence in both forward
  // and reverse directions and masks out tandem repeats.
  void matchSequenceRepeated
  ( WordSequence& seq, 
    HitList& hitListFwd ); 
  //    int numRepeats );



}; // ~class HashTable

//SequenceNumber HashTableView<PositionInDatabase>::getSequence
SequenceNumber HashTable::getSequence
( const_iterator i ) 
{
  return i->sequence;
} // ~SequenceNumber HashTable
  
//SequenceOffset HashTableView<PositionInDatabase>::getOffset
SequenceOffset HashTable::getOffset
( const_iterator i ) 
{
  return i->offset;
} // ~SequenceOffset HashTableView<PositionInDatabase>::getOffset




// Struct Name: RepeatedHit
// Description: This contains the information that needs to be stored for
// each hit in a region of tandem repeats
struct RepeatedHit
{

  RepeatedHit( const PositionInDatabase& subjectPos_,  
               const SequenceOffset& cyclePos_ ) :
  subjectPos( subjectPos_ ), cyclePos( cyclePos_ ) {}

  RepeatedHit( void ) : subjectPos(0,0), cyclePos(0) 
  { 
    //    subjectPos.sequence = 0; 
    //   subjectPos.offset   = 0;
  }

  bool operator<( const RepeatedHit& rhs) const
  {
    return ( subjectPos < rhs.subjectPos );
  } // ~operator<

  // subjectPos: position of the hit in the subject database
  PositionInDatabase subjectPos;
  // cyclePos: hash words obtained from a tandem repeat region in the
  // query sequence will repeat every m words, where m is the length of
  // the repeating motif. cyclePos denotes the position of the current
  // word in this cycle, and takes a value from 0 to m-1.
  SequenceOffset cyclePos;
};

// Class Name:  HitListRepeated
// Description: store for a list of RepeatedHits. Made a subclass of HitList
// so that matchWord can put hits into it.
class HitListRepeated: public vector<RepeatedHit>
{
  public:
  virtual ~HitListRepeated() {}
  void addHit( const PositionInDatabase& hitPos, 
               const SequenceOffset& queryPos )
  {
    push_back( RepeatedHit( hitPos, queryPos ) );
  }
};


class HashTableFred : public HashTable
{
 public:
  HashTableFred( ostream& monitoringStream=cerr,
		 string name="" ) : 
    HashTable( monitoringStream, name )
    {
      monitoringStream_ << "making HashTableFred" << endl;
      bitsPerSymbol_ = gResidueBits;
    }

};




// ### Function Declarations ###

// makeWord moved to Global/GlobalDefintions - TC 8.3.1

// End of include guard:
#endif

// End of file HashTable.h

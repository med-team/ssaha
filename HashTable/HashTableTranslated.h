/*  Last edited: May 29 11:42 2002 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : HashTableTranslated
// File Name    : HashTableTranslated.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_HashTableTranslated
#define INCLUDED_HashTableTranslated

// Description:

// Includes:
#include "HashTablePacked.h"
//#include "SequenceReaderCodon.h"
#include "SequenceEncoder.h"

// NB it is good practise for #include statements in header files to be
// replaced by forward declarations if at all possible

// ### Class Declarations ###


// Class Name :
// Description: 


// Class Name : HashTableComponent
// Description: A HashTableTranslated contains two of these
// Each contains the hit information for all three reading frames
// of one of the two strand directions. 
class HashTableComponent : public HashTablePacked
{
 public:

  HashTableComponent( ostream& monitoringStream, string name,
 		   Allocator<PositionPacked>& hitListAllocator 
		   = defaultHitListAllocator,
		   Allocator<PositionInHitList>& arrayAllocator 
		   = defaultArrayAllocator );

  virtual void convertHits
  ( PackedHitStore& hits, HitList& hitListFwd );

  void setQueryFrame( int qf ) { queryFrame_ = qf; }
  //  void setSubjectFrame( int sf ) { subjectFrame_ = sf; }

 private:
  int queryFrame_;
  //  int subjectFrame_;


}; // ~class HashTableComponent

// Class Name : HashTablePackedProtein 
// Description: Operates in two modes, to allow both protein and
// DNA queries to be matched. A DNA query (assumed to be a WordSequence
// withg gMaxBasesPerWord bases per word) is 3 way translated and
// each translation is matched. The hit information is converted to
// the DNA frame. This means the protein positions have to be 
// converted to the protein frame again at the generateMatches stage
class HashTablePackedProtein : public HashTablePacked
{
 public:
  typedef void (HashTablePackedProtein::* MatchSequencePointer)
    (WordSequence&, HitList&);

  HashTablePackedProtein( ostream& monitoringStream, string name = "",
 		   Allocator<PositionPacked>& hitListAllocator 
		   = defaultHitListAllocator,
		   Allocator<PositionInHitList>& arrayAllocator 
		   = defaultArrayAllocator );


  virtual void convertHits
  ( PackedHitStore& packedHits, HitList& hitListFwd );

  virtual void matchSequence
    ( WordSequence& seq, HitList& hitListFwd )
  {
    (this->*pMatchSequence_)(seq, hitListFwd);
  } // ~matchSequence

  void matchSequenceProtein( WordSequence& seq, HitList& hitListFwd );
  void matchSequenceTranslatedDNA( WordSequence& seq, HitList& hitListFwd );

  void setQueryProtein( void ) 
  { 
    pMatchSequence_ = &HashTablePackedProtein::matchSequenceProtein;
    queryMult_  = 1;
    queryFrame_ = 0;
  } 
  void setQueryTranslatedDNA( void ) 
  {
    pMatchSequence_ = &HashTablePackedProtein::matchSequenceTranslatedDNA;
    queryMult_  = gNumReadingFrames;
    queryFrame_ = 0;
  }

  private:
  CodonList codons_;
  SequenceEncoderCodon codonEncoder_;

  int queryFrame_;
  int queryMult_;

  MatchSequencePointer pMatchSequence_;




}; // ~HashTablePackedProtein

// Class Name : HashTableTranslated
// Description: Stores the hit information for the 6-way translation of 
// a set of DNA sequences. Then allows either protein or 6-way translated
// DNA to be run against them.
class HashTableTranslated : public HashTableGeneric
{

  // PUBLIC MEMBER FUNCTIONS
  public:
  typedef void (HashTableTranslated::* MatchSequencePointer)
    (WordSequence&, HitList&);

  enum { eDNAWordSizeForHashing = gMaxBasesPerWord-1 };

  // Constructors and Destructors

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  HashTableTranslated( ostream& monitoringStream=cerr,
		       string name="",
		       Allocator<PositionPacked>& hitListAllocator
		       = HashTablePacked::defaultHitListAllocator,
		       Allocator<PositionInHitList>& arrayAllocator
		       = HashTablePacked::defaultArrayAllocator );


  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  virtual ~HashTableTranslated() {}
  // (NB destructor should be virtual if class is to be derived from)

  // Manipulator Functions

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT

  // Function Name: createHashTable
  // Arguments: SequenceReader& (in), int (in), int (in)
  // Reads sequence information from an instance of SequenceReader and
  // uses it to create a hash table
  //  virtual void createHashTable
  //  ( SequenceReader& sequenceReader, int wordLength, int maxNumHits, 
  //   int stepLength = 0 );


  virtual void loadHashTable( SourceReaderIndex* pSourceReader=NULL );
  virtual void saveHashTable( void );



  virtual void setupPointerArray( void );
  virtual int countWordsAndGetNames
    ( SequenceReader& sequenceReader, SequenceAdapter* seq );

  virtual void computePointerArray( void );
  virtual void setupHitList( void );
  virtual void hashAllWords
    ( SequenceReader& sequenceReader, SequenceAdapter* seq, int numSeqs );
  virtual void cleanupTempData( void );

  virtual void hashWords
    ( SequenceAdapter& thisSeq, SequenceNumber seqNum );
  virtual void countWords( SequenceAdapter& thisSeq );

  virtual void matchSequence
    ( WordSequence& seq, HitList& hitListFwd )
  {
    (this->*pMatchSequence_)(seq, hitListFwd);
  }

  void matchSequenceProtein( WordSequence& seq, HitList& hitListFwd );
  void matchSequenceTranslatedDNA( WordSequence& seq, HitList& hitListFwd );


  virtual void setNumRepeats( int nr);
  virtual void setSubstituteThreshold( int ns );

  virtual char* getHitListStart( void ) const;
  virtual int getHitTypeSize( void ) const;
  virtual void allocateHitList( unsigned long size );
  virtual void loadHitList( unsigned long size );
  virtual void saveHitList( void );
  //  virtual void savePointerArray( void );

  virtual int  getMaxNumHits() const; 

  virtual void setMaxNumHits( int mnh ); 

  // Function Name: printHashStats
  // Prints some stats about the hash table
  virtual void printHashStats( void ); 

  // Need to redefine this so that expected number of words bit
  // in SSAHAMain.cpp works properly.
  virtual unsigned long getTotalNumWords( void ) const; 


  void setForward( void ) { pHash_ = &hashFwd_; }
  void setReverse( void ) { pHash_ = &hashRev_; }

  void setQueryProtein( void ) 
  { 
    pMatchSequence_ = &HashTableTranslated::matchSequenceProtein;
  }
  void setQueryTranslatedDNA( void ) 
  {
    pMatchSequence_ = &HashTableTranslated::matchSequenceTranslatedDNA;
  }

  // Accessor Functions
  // (NB all accessor functions should be 'const')

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  bool isForward( void ) const { return (pHash_==&hashFwd_); }

  
  // PROTECTED MEMBER FUNCTIONS 
  // (visible to this class and derived classes only)
  protected:

  // PRIVATE MEMBER FUNCTIONS
  // (visible to instances of this class only)
  
  private:
  HashTableTranslated( const HashTableTranslated&);          // NOT IMPLEMENTED
  HashTableTranslated& operator=(const HashTableTranslated&);// NOT IMPLEMENTED

  // PRIVATE MEMBER DATA
  protected:
  int translatedWordLength_;

  HashTableComponent hashFwd_;
  HashTableComponent hashRev_;
  HashTableComponent* pHash_;

  CodonList codons_;
  SequenceEncoderCodon codonEncoder_;

  MatchSequencePointer pMatchSequence_;


}; // ~HashTableTranslated

// ### Function Declarations ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

// End of include guard:
#endif

// End of file HashTableTranslated.h

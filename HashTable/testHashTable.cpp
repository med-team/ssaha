
// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : testHashTable
// File Name    : testHashTable.cpp
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Description:

// Includes:

#include "HashTable.h"
#include "GlobalDefinitions.h"
#include "TimeStamp.h"
#include "assert.h"
#include <string>
#include <strstream>
#include <iostream>

// ### Function Definitions ###

// Create a dummy version of hash table 
class HashTableTest : public HashTable
{
public:
  HashTableTest( int wordLength )
  {
    wordLength_ = wordLength;
  }

  //  void createHashTable
  //  ( SequenceReader& sequenceReader, int wordLength, int maxNumHits )
  //  {
  //  }

  // Function Name: loadHashTable
  // Arguments: const string& (in)
  // Reads a pre-computed hash table into memory from a file
  void loadHashTable( const string& fileNameRoot )
  {}

  // Function Name: saveHashTable
  // Arguments: const string& (in)
  // Saves a hash table to a file (for subsequent retrieval by loadHashTable)
  void saveHashTable( const string& fileNameRoot )
  {}
  
  // Accessor Functions
  // (NB all accessor functions should be 'const')

  void getSequenceName(string& seqName, SequenceNumber seqNum) const 
  {}


  // Function Name: matchWord
  // Arguments: Word (in), HitList& (out)
  // Populates hitsFound with the positions in the subject sequence database 
  // of all occurrences of the Word queryWord.
  //  void matchWord
  // ( Word queryWord, HitList& hitsFound, int baseOffset=0 ) const
  // {}

  // Function Name: matchWord
  // Arguments: WordSequence& (in), HitList& (out)
  // Populates hitsFound with the positions in the database of all occurrences
  // of the Words in the WordSequence queryWords. baseOffset is the initial 
  // shift in base pairs to be subtracted from all hit positions (TBD explain
  // this better!)
  //  void matchWord
  // ( const WordSequence& queryWords, 
  //   HitList& hitsFound, 
  //  int baseOffset = 0 ) const
  // {}

  void countWords(SequenceAdapter &) {}
  void hashWords(SequenceAdapter &, unsigned int, unsigned int *) {}


};

//void HashTableView<PositionInDatabase>::hashWords
//void HashTable::hashWords
//(SequenceAdapter &, unsigned int, unsigned int *)
//{}
//void HashTableView<PositionInDatabase>::countWords
//void HashTable::countWords
//(SequenceAdapter &)    
//{}




int main( void )
{

  Timer clock;

  int numTests = 0;

  cout << "*************************************" << endl << endl;
  cout << "Test of functions in module HashTable" << endl << endl;
  cout << "*************************************" << endl << endl;

  const int wordLength(10);
  std::ostrstream buffer;
  string s1,s2,s3,s4,s5;
  Word w1,w2,w3,w4,w5;

  // ---

  cout << "Test " << ++numTests <<": test of function printWord" 
       << endl << endl;

  w1 = 237148; 
  // This is binary 00 11 10 01 11 10 01 01 11 00
  // encodes as     A  T  G  C  T  G  C  C  T  A

  buffer << printWord(w1,wordLength) << ends;
  s1=buffer.str();
  buffer.freeze(false); // release memory to write further stuff to buffer  
  buffer.seekp(0,ios::beg); // ensure next write is to start of buffer
 
  cout << "Word " << w1 << " encodes as "<< s1 << endl;

  assert(s1=="ATGCTGCCTA");
    
  cout << "Test passed!" << endl << endl; 

  // ---

  cout << "Test " << ++numTests <<": test of function makeWord" 
       << endl << endl;


  s2 = "ATGCTGCCTA";
  w2 = makeWord(s2);         
  cout << "String " << s2 << " encodes as " << w2 << endl;
  assert(w1==w2);

  s2 = "AtGcTgCcTA";
  w2 = makeWord(s2);         
  cout << "String " << s2 << " encodes as " << w2 << endl;
  assert(w1==w2);
    
  cout << "Test passed!" << endl << endl; 

  // ---

  cout << "Test " << ++numTests <<": test of HashTableTest creation" 
       << endl << endl;

  HashTableTest ht(wordLength);
  assert( ht.getWordLength() == wordLength );

  cout << "Test passed!" << endl << endl; 

  // ---

  cout << "Test " << ++numTests <<": test of WordSequence" 
       << endl << endl;

  s1="AGTTCGTCCA";  s2="TGCTAAGTCA";  s3="GGTCATTGCA";
  s4="CACGTGCACG";  s5="AGCTGGCTGG";

  w1 = makeWord(s1);  w2 = makeWord(s2);  w3 = makeWord(s3);
  w4 = makeWord(s4);  w5 = makeWord(s5);

  cout << "Words " << w1 << " "<< w2 << " "<< w3 << " "<< w4 << " "<< w5 
       << endl;

  WordSequence ws;

  ws.addWord(w1);  ws.addWord(w2);  ws.addWord(w3);  ws.addWord(w4);
  ws.addWord(w5);
  ws.setNumBasesInLast(10);// %%%%%

  cout << " encode as word sequence " << ws.getWord(0) << " " 
       << ws.getWord(1) << " " << ws.getWord(2) << " " 
       << ws.getWord(3) << " " << ws.getWord(4) << endl;

  assert(ws.getNumWords() == 5);
  assert(ws.getWord(0) == w1);
  assert(ws.getWord(1) == w2);
  assert(ws.getWord(2) == w3);
  assert(ws.getWord(3) == w4);
  assert(ws.getWord(4) == w5);

  buffer << printWord(ws,wordLength) << ends;
  string S(buffer.str());
  buffer.freeze(false); // release memory to write further stuff to buffer  
  buffer.seekp(0,ios::beg); // ensure next write is to start of buffer

  cout << "Words " << s1 << ","<< s2 << ","<< s3 << ","<< s4 << ","<< s5 
       << " encode to " << S << endl;
  assert(S==s1+s2+s3+s4+s5);

  cout << "Test passed!" << endl << endl; 

  // ---

  cout << "Test " << ++numTests <<": test of function reverseComplement" 
       << endl << endl;


  Word r = reverseComplement(w1,wordLength);

  cout << "Reverse complement of " << printWord(w1,ht)
       << " computed as " << printWord(r,ht) << endl;
  // NB Assumes answer is RC of s1 as defined above
  assert(r==makeWord("TGGACGAACT"));

  cout << "Reverse complement of WordSequence" << endl;

  string R;

    for ( string::reverse_iterator i(S.rbegin()) ; i != S.rend() ; i++ )
    {
           if ( *i == 'A' ) R += "T";
      else if ( *i == 'T' ) R += "A";
      else if ( *i == 'G' ) R += "C";
      else if ( *i == 'C' ) R += "G";
    } // ~for

   WordSequence rs;
   reverseComplement(ws,rs,wordLength);

   cout << "Original string: " << printWord(ws,ht) << endl
        << "Computed RC    : " << printWord(rs,ht) << endl
        << "Should be      : " << R << endl;

  buffer << printWord(rs,ht) << ends;
  assert(R==buffer.str());  
  buffer.freeze(false); // release memory to write further stuff to buffer  
  buffer.seekp(0,ios::beg); // ensure next write is to start of buffer

  cout << "Reverse complement of WordSequence, "
       << "assuming only 7 valid base pairs in last Word" << endl;

  WordSequence ws2(ws);
  ws2.back()=makeWord("AGCTGGCAAA");
  ws2.setNumBasesInLast(7);

  rs.clear();
  reverseComplement(ws2,rs,wordLength); //,7);
  R.erase(R.begin());  R.erase(R.begin());  R.erase(R.begin());
  //  R+="AAA";

  cout << "Original string: " << printWord(ws2,ht) << endl
       << "Computed RC    : " << printWord(rs,ht) << endl
       << "Should be      : " << R << endl;

  buffer << printWord(rs,ht) << ends;
  assert(R==buffer.str());  
  buffer.freeze(false); // release memory to write further stuff to buffer  
  buffer.seekp(0,ios::beg); // ensure next write is to start of buffer

  cout << "Test passed!" << endl << endl; 

  // ---

  cout << "Test " << ++numTests <<": test of function shiftSequence" 
       << endl << endl;

  cout << "Shift sequence by one base pair" << endl;

  S.erase(S.begin());
  S+=("A");

  shiftSequence(ws,gBaseBits,wordLength);
  buffer << printWord(ws,wordLength) << ends;

  cout << "Shifted sequence: " << printWord(ws,wordLength) << endl
       << "should be:        " << S << endl; 

  assert(S==buffer.str());  
  buffer.freeze(false); // release memory to write further stuff to buffer  
  buffer.seekp(0,ios::beg); // ensure next write is to start of buffer

  cout << "Shift sequence by another three base pairs" << endl;

  S.erase(S.begin());
  S.erase(S.begin());
  S.erase(S.begin());
  S+=("AAA");

  shiftSequence(ws,gBaseBits,wordLength,3);
  buffer << printWord(ws,wordLength) << ends;

  cout << "Shifted sequence: " << printWord(ws,wordLength) << endl
       << "should be:        " << S << endl; 

  assert(S==buffer.str());  
  buffer.freeze(false); // release memory to write further stuff to buffer  
  buffer.seekp(0,ios::beg); // ensure next write is to start of buffer

    
  cout << "Test passed!" << endl << endl; 

  cout << clock;

  for ( int i(0) ; i < 1000000 ; i ++ ) shiftSequence(ws,gBaseBits,3);

  cout << clock;

  cout << "*************************************" << endl << endl;
  cout << "           End of last test          " << endl << endl;
  cout << "*************************************" << endl << endl;

 
  return (0);

}




// End of file testHashTable.cpp


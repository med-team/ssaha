
// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : GenerateTestFastaFiles
// File Name    : GenerateTestFastaFiles.cpp
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Description:

// Includes:

#include "GenerateTestFastaFiles.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>

// ### Function Definitions ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

BaseGenerator::BaseGenerator( int numBases, int seed ) 
: numBases_( numBases )
{
  const char baseNames[] = "AGCT";

  if ( seed != -9999 ) srand(seed);
  bases_.reserve( numBases_ );
  int base;

  for ( int i(0) ; i < numBases_; i++ )
  {
    base = rand() & 0x3;
    bases_ += baseNames[base];
  } // ~for i
} // constructor



// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

void BaseGenerator::generateSubjectFile
(  int seqSize, int basesOverlap, const char* fileName )
{
  ofstream subjectFile(fileName,ios::out);

  int numSeqs = 1 + ( numBases_ - seqSize ) / ( seqSize - basesOverlap );
  int seqSpacing = seqSize - basesOverlap;

  cout << "Num seqs:" << numSeqs << endl;

  for ( int i(0) ; i < numSeqs  ; i ++ )
  {

    subjectFile << ">SubjectSequence_" << i+1 << " - bases "
                << ( i * seqSpacing ) << " to "
                << ( i * seqSpacing ) + seqSize - 1
                << endl;

    subjectFile << bases_.substr( (i*seqSpacing), seqSize ) << endl;

    subjectFile << endl;

  } // ~for i
    
} // ~BaseGenerator::generateSubjectFile

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

void BaseGenerator::generateSubjectFileFastq
(  int seqSize, int basesOverlap, const char* fileName )
{
  ofstream subjectFile(fileName,ios::out);

  int numSeqs = 1 + ( numBases_ - seqSize ) / ( seqSize - basesOverlap );
  int seqSpacing = seqSize - basesOverlap;

  cout << "Num seqs:" << numSeqs << endl;

  for ( int i(0) ; i < numSeqs  ; i ++ )
  {

    subjectFile << "@SubjectSequence_" << i+1 << " - bases "
                << ( i * seqSpacing ) << " to "
                << ( i * seqSpacing ) + seqSize - 1
                << endl;

    subjectFile << bases_.substr( (i*seqSpacing), seqSize ) << endl;

    subjectFile << "+SubjectSequence_" << i+1 << " - bases "
                << ( i * seqSpacing ) << " to "
                << ( i * seqSpacing ) + seqSize - 1
                << endl;

    for ( int j(0) ; j < seqSize ; j++ ) subjectFile << "99 ";
    subjectFile << endl;
    

  } // ~for i
    
} // ~BaseGenerator::generateSubjectFileFastq

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

void BaseGenerator::generateQueryFile
( int baseStart, int seqSize, const char* fileName )
{

  if ( baseStart + seqSize > numBases_ )
  {
    cerr << "Error: requested query sequence exceeds total data size" << endl;
    return;
  } //~if

  ofstream queryFile(fileName,ios::out);
  queryFile << ">QuerySequence "  
              << baseStart << " to " 
              << baseStart + seqSize << endl;

  queryFile << bases_.substr( baseStart, seqSize );
 
  queryFile << endl;



} // ~BaseGenerator::generateQueryFile

void BaseGenerator::getBases
( int baseStart, int seqSize, string& outputString )
{
  if ( baseStart + seqSize > numBases_ )
  {
    cerr << "Error: requested query sequence exceeds total data size" << endl;
    return;
  } //~if

  outputString =  bases_.substr( baseStart, seqSize );
  
}





// End of file GenerateTestFastaFiles.cpp


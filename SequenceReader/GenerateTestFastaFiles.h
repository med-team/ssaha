/*  Last edited: Apr 19 10:08 2002 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : GenerateTestFastaFiles
// File Name    : GenerateTestFastaFiles.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_GenerateTestFastaFiles
#define INCLUDED_GenerateTestFastaFiles

// Description:

// Includes:

#include <string>
using namespace std;

// NB it is good practise for #include statements in header files to be
// replaced by forward declarations if at all possible

// ### Function Declarations ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

class BaseGenerator
{
  public:
  BaseGenerator( int numBases, int seed = -9999 );

  void generateSubjectFile
  ( int seqSize, int basesOverlap, 
    const char* fileName = "test_subject.fasta");

  void generateSubjectFileFastq
  ( int seqSize, int basesOverlap, 
    const char* fileName = "test_subject.fastq");

  void generateQueryFile
  ( int baseStart, int seqSize,
    const char* fileName = "test_query.fasta");

  void getBases
  ( int baseStart, int seqSize, string& outputString );

  protected:
  string bases_;
  int numBases_;
};


// End of include guard:
#endif

// End of file GenerateTestFastaFiles.h




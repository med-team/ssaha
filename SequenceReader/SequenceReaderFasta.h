/*  Last edited: Feb 21 18:19 2002 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : SequenceReaderFasta
// File Name    : SequenceReaderFasta.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_SequenceReaderFasta
#define INCLUDED_SequenceReaderFasta

// Description:

// Includes:

class WordSequence;
//class SequenceEncoder;
#include "SequenceReader.h"
#include "SequenceEncoder.h"
#include <string>

// NB it is good practise for #include statements in header files to be
// replaced by forward declarations if at all possible

// ### Class Declarations ###


// -----------------------------
// Class Name : SequenceReaderFileState
// Description: This preserves the state (ie position in a file) of
// a SequenceReader
class SequenceReaderFileState : public SequenceReaderState
{
 public:
  SequenceReaderFileState( SequenceNumber lsn, std::streampos fp ) :
  filePos_(fp),  SequenceReaderState(lsn) {}
  // no point in making this private as it's const
  const std::streampos filePos_;
};



// Class Name :
// Description: 
class SequenceReaderFile : public SequenceReader
{

  // PUBLIC MEMBER FUNCTIONS
  public:

  // Constructors and Destructors

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  SequenceReaderFile
  ( const char* fileName, 
    char seqStartChar,
    char seqStopChar,
    SequenceEncoder* pEncoder,
    ostream& monitoringStream = cerr );

  SequenceReaderFile
  ( istream& inputStream, 
    char seqStartChar,
    char seqStopChar,
    SequenceEncoder* pEncoder,
    ostream& monitoringStream = cerr );

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  SequenceReaderFile( const SequenceReaderFile& rhs); 

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  virtual ~SequenceReaderFile(); 
  // (NB destructor should be virtual if class is to be derived from)

  // Manipulator Functions
  virtual SequenceReader* clone( void ) 
  { return new SequenceReaderFile( *this ); }

  // Function Name: changeMode
  // Arguments: const SequenceReaderMode&
  // Makes a copy of mode and uses it to handle mismatch character reads
  virtual void changeMode( SequenceReaderMode* pMode );

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT

  // Accessor Functions
  // (NB all accessor functions should be 'const')

  // Function Name: rewind
  // Arguments: void
  // Returns:   void
  // Rewind to the start of the data file, so that getNextSequence will
  // return the first sequence in the file
  void rewind( void );

  // Function Name: findSequence
  // Arguments: SequenceNumber (in)
  // Returns:   void
  // Winds the input file stream to the start of sequence number seqNum. 
  // Throws an exception if seqNum exceeds the number of sequences in
  // the file.
  virtual bool findSequence( SequenceNumber seqNum );

  // Function Name: getNextSequence
  // Arguments: WordSequence& (out), int (in)
  // Returns:   bool
  // Read the next set of sequence information from the file and parse it
  // into WordSequence format
  virtual int getNextSequence( WordSequence& nextSeq, int wordLength );

  // Function Name: getSequence
  // Arguments: WordSequence& (out), SequenceNumber (in), int (in)
  // Returns:   int
  // Read the sequenceNumber-th set of sequence information from the file and 
  // parse it into WordSequence format
  virtual int getSequence
  ( WordSequence& nextSeq, SequenceNumber sequenceNumber, int wordLength );

  // Function Name: getLastSequenceName
  // Arguments: string& (out)
  // Returns:   void
  // Fills the string with the name of the last sequence read 
  virtual void getLastSequenceName( string& seqName ) const;

  // Function Name: getBitsPerSymbol
  // Arguments: none
  // Returns:   int
  // Returns number of bits per symbol used in encoding
  virtual int getBitsPerSymbol ( void ) const;

  // Function Name: getSourceDataType
  // Arguments: none
  // Returns:   SourceDataType
  // Returns type of data being encoded (protein or DNA)
  virtual SourceDataType getSourceDataType( void ) const;

  // Function Name: printName
  // Arguments: ostream& (out), SequenceNumber (in)
  // Returns:   void
  // Sends the name of the requested sequence to the output stream.
  virtual bool printName( ostream& os, SequenceNumber seqNum );

  // Function Name: printSideInfo
  // Arguments: ostream& (out), SequenceNumber (in)
  // Returns:   void
  // Sends the side info (e.g. clone name) for the requested sequence 
  // to the output stream.
  virtual bool printSideInfo( ostream& os, SequenceNumber seqNum );

  // Function Name: printSource
  // Arguments: ostream& (out), SequenceNumber (in)
  // Returns:   void
  // Send to the output stream the source data (in general, ASCII) from which 
  // the requested sequence was decoded.
  virtual bool printSource( ostream& os, SequenceNumber seqNum );

  // Function Name: extractSource
  // This extracts the source data for bases seqStart to seqEnd inclusive
  // of sequence seqNum and places it in source
  virtual void extractSource( char** pSource,//vector<char>& source, 
		     SequenceNumber seqNum,
		     SequenceOffset seqStart,
		     SequenceOffset seqEnd );

  virtual void saveIndexImp
    ( ostream& fileFile, 
      ostream& indexFile, 
      int& fileNumber );


  // Function Name: saveState
  // Arguments: void
  // Returns:   SequenceReaderState*
  // saves the state (ie current file position) of a SequenceReader for future
  // restoration
  virtual SequenceReaderState* saveState( void ) const
  {
    return new SequenceReaderFileState
      ( lastSequenceNumber_, pInputFileStream_->tellg() );
  }

  // Function Name: restoreState
  // Arguments:   SequenceReaderState*
  // Returns:     void
  // restores the state (ie current file position) of a SequenceReader
  // then (NB!!) deletes *pState;
  virtual void restoreState( SequenceReaderState* pState )
  {
    SequenceReaderFileState* p
      (dynamic_cast<SequenceReaderFileState*>(pState));
    assert(p!=NULL);
    lastSequenceNumber_ = p->lastSequenceNumber_;
    pInputFileStream_->seekg( p->filePos_, ios::beg );
    delete pState;
  }

  // PROTECTED MEMBER FUNCTIONS 
  // (visible to this class and derived classes only)
  protected:

  // Function Name: computeNumSequencesInFile
  // Arguments: void
  // Returns:   SequenceNumber
  // Returns the number of sequences in the file (will be done by lazy 
  // initialization, i.e. will only be calculated if asked for. NB this
  // will lose the current place in the file)
  virtual SequenceNumber computeNumSequencesInFile( void );


  // PRIVATE MEMBER FUNCTIONS
  // (visible to instances of this class only)
  
  private:
  SequenceReaderFile& operator=(const SequenceReaderFile&);// NOT IMPLEMENTED

  // PROTECTED DATA:
  // (visible to this class and derived classes only)
  protected:
    enum Constants{ sideInfoBufferSize_ = 20000, inputBufferSize_ = 20000 };
    char inputBuffer_[ inputBufferSize_ ];
    //    char sideInfoBuffer_[ sideInfoBufferSize_ ];
  //    string inputBuffer_;
    string sideInfoBuffer_;
  char seqStartChar_;
  char seqStopChar_;

  istream* pInputFileStream_;

  //  ifstream* pInputFileStream_;
  string fileName_;
  SequenceEncoder* pEncoder_;

  vector<std::streampos> seqPositions_;

  // PRIVATE MEMBER DATA
  private:

}; // SequenceReaderFile

class SequenceReaderFasta : public SequenceReaderFile
{

  // PUBLIC MEMBER FUNCTIONS
  public:

  // Constructors and Destructors


  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  SequenceReaderFasta
  ( const char* fileName, 
    SequenceEncoder* pEncoder,
    ostream& monitoringStream = cerr );
  SequenceReaderFasta
  ( const char* fileName, 
    ostream& monitoringStream = cerr );

}; // SequenceReaderFasta

typedef SequenceReaderFasta SequenceReaderFastaDNA; 

class SequenceReaderFastaProtein : public SequenceReaderFasta
{

  // PUBLIC MEMBER FUNCTIONS
  public:

  // Constructors and Destructors

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  SequenceReaderFastaProtein( const char* fileName, 
			      ostream& monitoringStream = cerr );

}; // SequenceReaderFastaProtein





// ### Function Declarations ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

// End of include guard:
#endif

// End of file SequenceReaderFile.h











/*  Last edited: Jan 14 13:32 2002 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : SequenceReaderString
// File Name    : SequenceReaderString.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_SequenceReaderString
#define INCLUDED_SequenceReaderString

// Description:

// Includes:

#include "SequenceReader.h"
#include <string>
// NB it is good practise for #include statements in header files to be
// replaced by forward declarations if at all possible
class WordSequence;
//class SequenceEncoder;
#include "SequenceEncoder.h"
// ### Class Declarations ###


// Class Name : SequenceReaderStringBase
// Description: 

class SequenceReaderStringBase : public SequenceReader
{

  // PUBLIC MEMBER FUNCTIONS
  public:

  // Constructors and Destructors

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
SequenceReaderStringBase
( const string& sequenceString, 
  // NB SequenceReaderStringBase takes ownership of *pEncoder
  SequenceEncoder* pEncoder,
  ostream& monitoringStream = cerr ) :
  sequenceString_( sequenceString ), 
  pEncoder_( pEncoder ),
  SequenceReader( monitoringStream )
{
  monitoringStream_ << "constructing SequenceReaderStringBase" << endl;
} // constructor


  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
SequenceReaderStringBase( const SequenceReaderStringBase& rhs ) :
sequenceString_( rhs.sequenceString_ ), 
pEncoder_( rhs.pEncoder_->clone() ),
SequenceReader( rhs.monitoringStream_ )
{
  monitoringStream_ << "copy constructing SequenceReaderStringBase" << endl;
} // copy constructor



  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
virtual ~SequenceReaderStringBase()
{
  monitoringStream_ << "destructing SequenceReaderStringBase" << endl;
  delete pEncoder_;
} // destructor

  // (NB destructor should be virtual if class is to be derived from)

  // Manipulator Functions
  virtual SequenceReader* clone( void ) 
  { return new SequenceReaderStringBase( *this ); }

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT

  // Function Name: changeMode
  // Arguments: const SequenceReaderMode&
  // Makes a copy of mode and uses it to handle mismatch character reads
virtual void changeMode( SequenceReaderMode* pMode )
{
  pEncoder_->changeMode( pMode );
  //  pEncoder_->changeMode( pMode );
}





  // Accessor Functions
  // (NB all accessor functions should be 'const')

  // Function Name: rewind
  // Arguments: void
  // Returns:   void
  // Rewind to the start of the data file, so that getNextSequence will
  // return the first sequence in the file
  void rewind( void )
  { lastSequenceNumber_ = 0; } // ~rewind

  // Function Name: findSequence
  // Arguments: SequenceNumber (in)
  // Returns:   void
  // Winds the input file stream to the start of sequence number seqNum. 
  // Returns false if seqNum exceeds the number of sequences in
  // the file.
virtual bool findSequence( SequenceNumber seqNum ) 
{
  return (seqNum == 1);
} 


  // Function Name: getNextSequence
  // Arguments: WordSequence& (out), int (in)
  // Returns:   int
  // Read the set of sequence information from the string and parse it
  // into WordSequence format. Returns -1 if a problem, else the number of
  // valid base pairs in the final word of the sequence
virtual int getNextSequence( WordSequence& nextSeq, int wordLength );



// Function Name: getSequence
// Arguments: WordSequence& (out), SequenceNumber (in), int (in)
// Returns:   int
// Read the sequenceNumber-th set of sequence information from the file and 
// parse it into WordSequence format
virtual int getSequence
( WordSequence& nextSeq, SequenceNumber sequenceNumber, int wordLength )
{
  // only one sequence in a string, so just ignore sequenceNumber
  return getNextSequence( nextSeq, wordLength );
} // ~getSequence
 
  // Function Name: getLastSequenceName
  // Arguments: string& (out)
  // Returns:   void
  // Fills the string with the name of the last sequence read 
void getLastSequenceName( string& seqName ) const
{
  seqName = "UnnamedSequence";
} // ~SequenceReaderStringBase::getLastSequenceName

  // Function Name: getBitsPerSymbol
  // Arguments: none
  // Returns:   int
  // Returns number of bits per symbol used in encoding
virtual int getBitsPerSymbol ( void ) const
{
  return pEncoder_->getBitsPerSymbol();
} // ~SequenceReaderString::getBitsPerSymbol ( void ) const

  // Function Name: getSourceDataType
  // Arguments: none
  // Returns:   SourceDataType
  // Returns type of data being encoded (protein or DNA)
virtual SourceDataType getSourceDataType( void ) const
{
  return pEncoder_->getSourceDataType();
}





  // Function Name: printName
  // Arguments: ostream& (out), SequenceNumber (in)
  // Returns:   void
  // Sends the name of the requested sequence to the output stream
virtual bool printName( ostream& os, SequenceNumber seqNum )
{
  os << "UnnamedSequence" << endl;
  return true;
} // ~SequenceReaderStringBase::printName



  // Function Name: printSideInfo
  // Arguments: ostream& (out), SequenceNumber (in)
  // Returns:   void
  // Sends the side info for the requested sequence to the output stream
virtual bool printSideInfo( ostream& os, SequenceNumber seqNum )
{
  return true;
} // ~SequenceReaderStringBase::printName


  // Function Name: printSource
  // Arguments: string& (out), SequenceNumber (in)
  // Returns:   void
  // Sends the source data (e.g. ASCII) for the requested sequence
  // to the output stream
virtual bool printSource( ostream& os, SequenceNumber seqNum )
{
  os << sequenceString_;
  return true;
} // ~SequenceReaderStringBase::printName


  // Function Name: computeNumSequencesInFile
  // Arguments: void
  // Returns:   SequenceNumber
  // Returns the number of sequences in the string - always 1!!!
  SequenceNumber computeNumSequencesInFile( void ) { return 1; } 


  // PROTECTED MEMBER FUNCTIONS 
  // (visible to this class and derived classes only)
  protected:

  // PRIVATE MEMBER FUNCTIONS
  // (visible to instances of this class only)
  
  private:

  SequenceReaderStringBase& operator=(const SequenceReaderStringBase&);   
  // NOT IMPLEMENTED

  // PRIVATE MEMBER DATA
  private:
  const string sequenceString_;     
//  ENCODER encoder_;
SequenceEncoder* pEncoder_;

}; // SequenceReaderStringBase

//typedef SequenceReaderStringBase<SequenceEncoderDNA> 
//SequenceReaderString;
//typedef SequenceReaderStringBase<SequenceEncoderDNA> 
//SequenceReaderStringDNA;
//typedef SequenceReaderStringBase<SequenceEncoderProtein> 
//SequenceReaderStringProtein;


class SequenceReaderStringDNA : public SequenceReaderStringBase
{
 public:
SequenceReaderStringDNA
  ( const string& sequenceString, ostream& monitoringStream = cerr ) :
  SequenceReaderStringBase( sequenceString, 
			    new SequenceEncoderDNA(), 
			     monitoringStream ) {}
};
class SequenceReaderStringProtein : public SequenceReaderStringBase
{
 public:
SequenceReaderStringProtein
  ( const string& sequenceString, ostream& monitoringStream = cerr ) :
  SequenceReaderStringBase( sequenceString, 
			    new SequenceEncoderProtein(), 
			     monitoringStream ) {}
};

typedef SequenceReaderStringDNA SequenceReaderString;

//class SequenceReaderStringProtein : public SequenceReaderStringBase
//{
// public:
//SequenceReaderStringProtein
//( const string& sequenceString,  ostream& monitoringStream = cerr );
//};
//



// ### Function Declarations ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

// End of include guard:
#endif

// End of file SequenceReaderString.h



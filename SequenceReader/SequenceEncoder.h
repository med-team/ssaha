/*  Last edited: May 29 14:29 2002 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : SequenceEncoder
// File Name    : SequenceEncoder.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_SequenceEncoder
#define INCLUDED_SequenceEncoder

// Description:

// Includes:
#include "GlobalDefinitions.h"

// NB it is good practise for #include statements in header files to be
// replaced by forward declarations if at all possible
class SequenceReaderMode;

// ### Class Declarations ###


static const int numPossibleChars(1<<8);
//static const uchar maxPossibleChar(0xFF);
typedef Word TranslationTable[numPossibleChars]; 
typedef Word ExpandedTranslationTable[numPossibleChars*numPossibleChars];

static const Word nv(0xFF);
static const Word firstCharInvalid(1<<31);
static const Word secondCharInvalid(1<<30);
static const Word someCharInvalid(firstCharInvalid|secondCharInvalid);

static const Word maskBase(0x3);
static const Word mask2Bases((maskBase<<gBaseBits)|maskBase);
static const Word maskCodon((mask2Bases<<gBaseBits)|maskBase);

// flagged char must have value > 63 to avoid being confused
static const char flaggedChar('^');


static const TranslationTable ttDNA = 
{
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv, /* next is 'A' */ 
00,nv,01,nv,nv,nv,02,nv,nv,nv,nv,nv,nv,nv,nv, /* next is 'P' */
nv,nv,nv,nv,03,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv, /* next is 'a' */ 
00,nv,01,nv,nv,nv,02,nv,nv,nv,nv,nv,nv,nv,nv, /* next is 'p' */
nv,nv,nv,nv,03,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv
};

#ifdef OLD_PROTEIN_TRANSLATION_TABLE
static const TranslationTable ttProtein = 
{
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv, 0,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv, /* next is 'A' */ 
 1,nv, 2, 3, 4, 5, 6, 7, 8,nv, 9,10,11,12,nv, /* next is 'P' */
13,14,15,16,17,18,19,20,nv,21,nv,nv,nv,nv,nv,nv,
nv, /* next is 'a' */ 
 1,nv, 2, 3, 4, 5, 6, 7, 8,nv, 9,10,11,12,nv, /* next is 'p' */
13,14,15,16,17,18,19,20,nv,21,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv
};
#endif

// changed 20.3.2 AJC 
// coding for U removed, extra encoding for X added. New encoding:
// 0  *
// 1  A
// 2  C
// 3  D
// 4  E
// 5  F
// 6  G
// 7  H
// 8  I
// 9  K
// 10 L
// 11 M
// 12 N
// 13 P
// 14 Q
// 15 R
// 16 S
// 17 T
// 18 V
// 19 W
// 20 X
// 21 Y
// Changes: U was 18, now treated as an X
//          V was 19 now 18
//          W was 20 now 19
//          X is 20, not there before
// B (= D or N) now treated as D
// Z (= E or Q) now treated as E

static const TranslationTable ttProtein = 
{
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv, 0,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv, /* next is 'A' */ 
 1, 3, 2, 3, 4, 5, 6, 7, 8,nv, 9,10,11,12,nv, /* next is 'P' */
13,14,15,16,17,20,18,19,20,21, 4,nv,nv,nv,nv,nv,
nv, /* next is 'a' */ 
 1, 3, 2, 3, 4, 5, 6, 7, 8,nv, 9,10,11,12,nv, /* next is 'p' */
13,14,15,16,17,20,18,19,20,21, 4,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv
};


static const TranslationTable ttCodon    = 
{
  9 , // K', // AAA
  12, // N', // AAC
  9 , // K', // AAG
  12, // N', // AAT
  17, // T', // ACA
  17, // T', // ACC
  17, // T', // ACG
  17, // T', // ACT
  15, // R', // AGA
  16, // S', // AGC
  15, // R', // AGG
  16, // S', // AGT
  8 , // I', // ATA
  8 , // I', // ATC
  11, // M', // ATG
  8 , // I', // ATT
  14, // Q', // CAA
  7 , // H', // CAC
  14, // Q', // CAG
  7 , // H', // CAT
  13, // P', // CCA
  13, // P', // CCC
  13, // P', // CCG
  13, // P', // CCT
  15, // R', // CGA
  15, // R', // CGC
  15, // R', // CGG
  15, // R', // CGT
  10, // L', // CTA
  10, // L', // CTC
  10, // L', // CTG
  10, // L', // CTT
  4 , // E', // GAA
  3 , // D', // GAC
  4 , // E', // GAG
  3 , // D', // GAT
  1 , // A', // GCA
  1 , // A', // GCC
  1 , // A', // GCG
  1 , // A', // GCT
  6 , // G', // GGA
  6 , // G', // GGC
  6 , // G', // GGG
  6 , // G', // GGT
  18, // 19, // V', // GTA
  18, // 19, // V', // GTC
  18, // 19, // V', // GTG
  18, // 19, // V', // GTT
  0 , // *', // TAA
  21, // Y', // TAC
  0 , // *', // TAG
  21, // Y', // TAT
  16, // S', // TCA
  16, // S', // TCC
  16, // S', // TCG
  16, // S', // TCT
  0 , // *', // TGA
  2 , // C', // TGC
  19, // 20, // W', // TGG
  2 , // C', // TGT
  10, // L', // TTA
  5 , // F', // TTC
  10, // L', // TTG
  5 , // F', // TTT
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,20,nv,nv,nv,nv,nv,nv,nv, // 'X'=20
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv
};

// ttCodonReverse gives the amino acid code for the reverse of a codon
// saves taking the RC of a sequence and using ttCodon on that
static const TranslationTable ttCodonReverse = 
{
  5 , // K', // AAA
  19, // N', // AAC
  10, // K', // AAG
  8 , // N', // AAT
  2 , // T', // ACA
  6 , // T', // ACC
  15, // T', // ACG
  16, // T', // ACT
  16, // R', // AGA
  1 , // S', // AGC
  13, // R', // AGG
  17, // S', // AGT
  21, // I', // ATA
  3 , // I', // ATC
  7 , // M', // ATG
  12, // I', // ATT
  10, // Q', // CAA
  19, // H', // CAC
  10, // Q', // CAG
  11, // H', // CAT
  20, // P', // CCA
  6 , // P', // CCC
  15, // P', // CCG
  15, // P', // CCT
  16, // R', // CGA
  1 , // R', // CGC
  13, // R', // CGG
  17, // R', // CGT
  0 , // L', // CTA
  4 , // L', // CTC
  14, // L', // CTG
  9 , // L', // CTT
  5 , // E', // GAA
  19, // D', // GAC
  10, // E', // GAG
  8 , // D', // GAT
  2 , // A', // GCA
  6 , // A', // GCC
  15, // A', // GCG
  16, // A', // GCT
  16, // G', // GGA
  1 , // G', // GGC
  13, // G', // GGG
  17, // G', // GGT
  21, // V', // GTA
  3 , // V', // GTC
  7 , // V', // GTG
  12, // V', // GTT
  10, // *', // TAA
  19, // Y', // TAC
  10, // *', // TAG
  8 , // Y', // TAT
  0 , // S', // TCA
  6 , // S', // TCC
  15, // S', // TCG
  15, // S', // TCT
  16, // *', // TGA
  1 , // C', // TGC
  13, // W', // TGG
  17, // C', // TGT
  0 , // L', // TTA
  4 , // F', // TTC
  14, // L', // TTG
  9 , // F', // TTT
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,
nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv,nv
};

typedef vector<char> CodonList;


// Class Name :
// Description: 
class SequenceEncoder
{

  // PUBLIC MEMBER FUNCTIONS
  public:

  // Constructors and Destructors

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  SequenceEncoder
  ( const TranslationTable* tt, 
    SourceDataType sourceData,
    int bitsPerSymbol, 
    int wordLength,
    ostream& monitoringStream = cerr);

  SequenceEncoder( const SequenceEncoder& rhs );

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  virtual ~SequenceEncoder();
  // (NB destructor should be virtual if class is to be derived from)

  virtual SequenceEncoder* clone( void )
  {
    return new SequenceEncoder(*this);
  }

  // Manipulator Functions

  // Function Name: changeMode
  // Arguments: const SequenceReaderMode&
  // Makes a copy of mode and uses it to handle mismatch character reads
  void changeMode( SequenceReaderMode* pMode );

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  void linkSeq( WordSequence& seq )
    { pSeq_=&seq;
      pSeq_->clear();
      pSeq_->push_back(0); 
      pSeq_->setNumBasesInLast(0); }
     

  virtual void encode( const char* data, int numChars ); 
  void encode( const string& data, int numChars=-1 )
    { encode(data.c_str(), 
	     (numChars!=-1)?numChars:data.size()); }
  void encode( const CodonList& data, int numChars=-1 )
    { encode( (const char*)&data[0], 
	     (numChars!=-1)?numChars:data.size()); }



  void encodeChar
    ( uchar thisChar, Word& thisWord, Word& wordFlag, int& basesInLast );

  void addWord
    ( Word& thisWord, Word& thisFlag, int& basesInLast );


  //  void fastEncode
  //    ( uchar* udata, Word& thisWord, int& i, const int& lastFastEncode );


  void expandTranslationTable
    ( ExpandedTranslationTable& ett );


  void unlinkSeq( void );

  void setWordLength( int wordLength )
  { 
    //    cout << "Encoder: " << bitsPerSymbol_ << " " << wordLength << endl;
    if ( ( bitsPerSymbol_*wordLength ) > (int)(8*sizeof(Word)) )
    {
      int * fred(NULL); fred[0] = 9999; // forces segv so can look at core
      throw SSAHAException("Symbol data exceeds capacity of Word!\n");
    }    
    wordLength_ = wordLength;
    //   numSymbolPairs_ = wordLength_ >> 1;
    //    oddNumSymbols_ = ( (wordLength_&1) == 1 );
  }

  // Accessor Functions
  // (NB all accessor functions should be 'const')

  SourceDataType getSourceDataType( void ) const { return sourceData_; }
  int getWordLength( void ) const { return wordLength_; }
  int getBitsPerSymbol( void ) const { return bitsPerSymbol_; }
  


  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  
  // PROTECTED MEMBER FUNCTIONS 
  // (visible to this class and derived classes only)
  protected:


  // PRIVATE MEMBER FUNCTIONS
  // (visible to instances of this class only)
  
  private:
  SequenceEncoder& operator=(const SequenceEncoder&);   // NOT IMPLEMENTED

  // PRIVATE MEMBER DATA
 protected:
  ostream& monitoringStream_;
  const TranslationTable* tt_;
  const ExpandedTranslationTable* ett_;
  const SourceDataType sourceData_;
  const int bitsPerSymbol_;
  const Word symbolMask_;
  int wordLength_;
  WordSequence* pSeq_;
  SequenceReaderMode* pState_;
  // According to the mode, wordFlag_ may be set to indicate
  // that a word is invalid 
  Word wordFlag_;
  int  numSymbolPairs_;
  //  bool oddNumSymbols_;
  const int  doubleBitShift_;

}; // SequenceEncoder

class SequenceEncoderDNA : public SequenceEncoder
{
public:
  SequenceEncoderDNA( int wordLength=10, ostream& monStream=cerr );

 protected:
  static ExpandedTranslationTable ettSource_;
  static bool isExpanded_;
};

class SequenceEncoderProtein : public SequenceEncoder
{
public:
  SequenceEncoderProtein( int wordLength=5, ostream& monStream=cerr );

 protected:
  static ExpandedTranslationTable ettSource_;
  static bool isExpanded_;

};

class SequenceEncoderCodon : public SequenceEncoder
{
public:
  SequenceEncoderCodon( int wordLength=5, ostream& monStream=cerr );

// void setForward( void ) { tt_ = &ttCodon; } 
// void setReverse( void ) { tt_ = &ttCodonReverse; } 
 protected:
  static ExpandedTranslationTable ettSource_;
  static bool isExpanded_;

};



ostream& operator<<( ostream& os, CodonList& c );



// Function Name: codonize
// Arguments: WordSequence& (in), vector<char> out 
// Returns:   void
// Splits a WordSequence of 2 bit per base DNA into codons
// Assumes the DNA has a word size of gMaxBasesPerWord
void codonize
( const WordSequence& in, CodonList& codons, int readingFrame );

// Function: GetCodonFromWord
// Grabs 3 bases (=6bits) from a specified position in a word
// ... unless the gCursedWord bit is set. In this case, the
// character flaggedChar (='^') is returned. When SequenceEncoder
// tries to encode this character using ttCodon it gets nv and 
// (provided its mode is set to SequenceReaderModeFlagReplace('X') ) 
// replaces it with 'X'. The 'X' *character* is then encoded by
// ttCodon to 20, which is the *code* for the X *codon*


template <int CODON_SHIFT, int BASE_SHIFT > 
Word getCodonFromWord( const Word& w )
{
  return 
  (
     (w&gCursedWord) 
     ? flaggedChar
     : (    ( w & ( maskCodon << ((CODON_SHIFT*gCodonBits) 
				  + (BASE_SHIFT*gBaseBits)   ) ) )
	 >> ((CODON_SHIFT*gCodonBits) + (BASE_SHIFT*gBaseBits) ) ) 
  );


} //~template <int CODON_SHIFT, int BASE_SHIFT > Word getCodonFromWord




// Function Name: codonizeAndFlag
// Arguments: WordSequence& (in), vector<char> out 
// Returns:   void
// Splits a WordSequence of 2 bit per base DNA into codons
// Used only when creating a HashTableTranslated
// Assumes the DNA has a word size of 15
// This leaves the MSB of a Word free to act as a flag
// If an entry of codons is equal to '!' then the flag bit
// was set in the DNA Word(s) it came from, meaning the original DNA
// test was flagged, typically because it contained Ns or -s
// If codons are then encoded using a SequenceEncoderCodon
// with mode set to SequenceReaderModeFlagReplace('X'), this '!'
// character is not recoginzed and the Word it is in is flagged
// Point of all this is that repeat masked DNA can be excluded from
// the HashTableTranslated
void codonizeAndFlag
( const WordSequence& in, CodonList& codons, int readingFrame );

// Function Name: codonizeAndFlag
// Arguments: WordSequence& (in), vector<char> out 
// Returns:   void
// Codonizes the reverse complement of a WordSequence as described
// for codonizeAndFlag above. Reverse complement is not generated,
// the codonizations are generated directly from the forward
// strand using ttCodonReverse
void codonizeAndFlagReverse
( const WordSequence& in, CodonList& codons, int readingFrame );







// ### Function Declarations ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT
void print( const TranslationTable& tt );



// End of include guard:
#endif

// End of file SequenceEncoder.h



// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : SequenceReaderLocal
// File Name    : SequenceReaderLocal.cpp
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Description:

// Includes:

#include "SequenceReaderLocal.h"
#include "SequenceReader.h"

// ### Function Definitions ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

  // Function Name: Constructor
  // Arguments: ostream&
  // Takes the data from seqFile and places it in seqData_, seqBasesInLast_
  // and seqNames_
SequenceReaderLocal::SequenceReaderLocal
( SequenceReader& seqFile, int wordLength, ostream& monitoringStream ) :
sourceData_( seqFile.getSourceDataType() ),
wordLength_( wordLength ),
bitsPerSymbol_( seqFile.getBitsPerSymbol() ),
SequenceReader( monitoringStream )
{
  monitoringStream_ 
    << "constructing SequenceReaderLocal from SequenceReader" << endl;
    seqFile.rewind();
    const pair< WordSequence, std::string> dummy; 

    seqData_.push_back(dummy);
    //    seqNames_.push_back();

    while(  seqFile.getNextSequence( seqData_.back().first, wordLength_ ) != -1 ) 
    {      
      seqFile.getLastSequenceName( seqData_.back().second );
      seqData_.push_back(dummy);
    } // ~while
      seqData_.pop_back();
    //    numSequencesInFile_ = seqFile.getNumSequencesInFile();
    rewind();
    seqFile.rewind();
} // ~constructor

SequenceReaderLocal::SequenceReaderLocal
( int wordLength, int bitsPerSymbol, ostream& monitoringStream ) :
sourceData_( gUnknownData ),
wordLength_( wordLength ),
bitsPerSymbol_( bitsPerSymbol ),
SequenceReader( monitoringStream )
{
  monitoringStream_ 
    << "constructing empty SequenceReaderLocal" << endl;
}


  // Function Name: Copy constructor
  // Arguments:
  // NB This is potentially slow. 
// %%%% not properly implemented - don't try to copy! %%%%%
SequenceReaderLocal::SequenceReaderLocal( const SequenceReaderLocal& rhs )
{
  monitoringStream_ << "copy constructing SequenceReaderLocal" << endl;
} // ~destructor

  // Function Name: Destructor
  // Arguments:
SequenceReaderLocal::~SequenceReaderLocal()
{
  monitoringStream_ << "destructing SequenceReaderLocal" << endl;
} // ~destructor

  // Function Name: getNextSequence
  // Arguments: WordSequence& (out), int (in)
  // Returns:   int
  // Read the next set of sequence information from the file and parse it
  // into WordSequence format. Returns -1 if there has been a problem with
  // reading the sequence, else returns the number of valid base pairs 
  // contained within the final word of the sequence.
  int SequenceReaderLocal::getNextSequence( WordSequence& nextSeq, int wordLength )
  {
    if ( lastSequenceNumber_ == getNumSequencesInFile() )
    {
      monitoringStream_ << "End of file has been reached." << endl;
      return (-1);
    } // if
    if ( wordLength != wordLength_ )
    {
      monitoringStream_ 
      << "Error: sequence data word length is fixed (" 
      << wordLength_ << " base pairs)." << endl;
      throw SSAHAException("Wrong word length for SequenceReaderLocal");
    } // if
    // NB sequences are numbered 1 ... n but seqData_ is numbered
    // 0 ... n-1
    lastSequenceNumber_++;
    nextSeq = seqData_[lastSequenceNumber_-1].first; 
    //    nextSeq.numBasesInLast
    //   = seqData_[lastSequenceNumber_-1].first.numBasesInLast;
    return (seqData_[lastSequenceNumber_-1].first.getNumBasesInLast());
  } // ~SequenceReaderLocal::getNextSequence

  // Function Name: getSequence
  // Arguments: WordSequence& (out), SequenceNumber (in), int (in)
  // Returns:   bool
  // Read the sequenceNumber-th set of sequence information from the file and 
  // parse it into WordSequence format
  int SequenceReaderLocal::getSequence
  ( WordSequence& nextSeq, SequenceNumber sequenceNumber, int wordLength )
  {

    if ( sequenceNumber > numSequencesInFile_ )
    {
      monitoringStream_ << "End of file has been reached." << endl;
      return (-1);
    } // if
    if ( wordLength != wordLength_ )
    {
      monitoringStream_ 
      << "Error: sequence data word length is fixed (" 
      << wordLength_ << " base pairs)." << endl;
      throw SSAHAException("Wrong word length for SequenceReaderLocal");
    } // if
    // NB sequences are numbered 1 ... n but seqData_ is numbered
    // 0 ... n-1
    nextSeq = seqData_[sequenceNumber - 1].first; 
    //    nextSeq.numBasesInLast=seqData_[sequenceNumber-1].first.numBasesInLast;
    return nextSeq.getNumBasesInLast();
  } // ~getSequence

  // Function Name: findSequence
  // Arguments: SequenceNumber (in)
  // Returns:   void
  // Winds the input file stream to the start of sequence number seqNum. 
  // Returns false if seqNum exceeds the number of sequences in
  // the file.
  bool SequenceReaderLocal::findSequence( SequenceNumber seqNum )
  {

    if (( seqNum <= seqData_.size()) && ( seqNum != 0 ))
    {
      lastSequenceNumber_ = seqNum - 1;
      return true;
    } // ~if
    else
    {
      return false;
    } // ~else
  }


  // Function Name: getLastSequenceName
  // Arguments: string& (out)
  // Returns:   void
  // Fills the string with the name of the last sequence read 
  void SequenceReaderLocal::getLastSequenceName( string& seqName ) const
  {
    seqName = seqData_[lastSequenceNumber_ - 1].second;
  }

  // Function Name: getSequenceName
  // Arguments: string& (out), SequenceNumber (in)
  // Returns:   void
  // Fills a string with the name of the requested sequence
  bool SequenceReaderLocal::printName( ostream& os, SequenceNumber seqNum) 
  {
    if ( seqNum > numSequencesInFile_ )
    {
      monitoringStream_ << "Error: requested sequence number (" << seqNum
			<< ") exceeds num seqs in file ("
                        << numSequencesInFile_ << ")." << endl;
      return false;
    } // if
    os <<  seqData_[seqNum - 1].second << endl;
    return true;
  }  

  // Function Name: printSideInfo
  // Arguments: string& (out), SequenceNumber (in)
  // Returns:   void
  // Fills a string with the name of the requested sequence
  bool SequenceReaderLocal::printSideInfo
  ( ostream& os, SequenceNumber seqNum )
  {
    return false;
  }

  // Function Name: printSource
  // Arguments: string& (out), SequenceNumber (in)
  // Returns:   void
  // Fills a string with the name of the requested sequence
  bool SequenceReaderLocal::printSource
  ( ostream& os, SequenceNumber seqNum )
  {
    return false;
  }



// End of file SequenceReaderLocal.cpp


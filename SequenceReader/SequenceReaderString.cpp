
// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : SequenceReaderString
// File Name    : SequenceReaderString.cpp
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Description:

// Includes:

#include "SequenceReaderString.h"
#include "SequenceEncoder.h"
#include <string>

// ### Function Definitions ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

  // Function Name: getNextSequence
  // Arguments: WordSequence& (out), int (in)
  // Returns:   int
  // Read the set of sequence information from the string and parse it
  // into WordSequence format. Returns -1 if a problem, else the number of
  // valid base pairs in the final word of the sequence
int SequenceReaderStringBase::getNextSequence
( WordSequence& nextSeq, int wordLength )
{

      if (lastSequenceNumber_ != 0) 
      {
        monitoringStream_ 
	  << "String already read - call rewind before trying to reread" 
          << endl;
        return -1;
      } // ~if

      lastSequenceNumber_++;
 
      pEncoder_->setWordLength(wordLength);
      pEncoder_->linkSeq(nextSeq);

      pEncoder_->encode
      ( sequenceString_ );

      pEncoder_->unlinkSeq();
    // If the last word is not completely full (as will be the case if
    // wordLength does not divide numBases) then shift the word so the
    // valid bases occupy the most significant bits of the word.


    lastSequenceNumber_ = 1;

    return nextSeq.getNumBasesInLast();

} // ~getNextSequence

// Everything now in SequenceReaderString.h! (made into a template)
// TC 10.4.1
// Everything now back in SequenceReaderString.cpp! 
// Templates on gcc are pathetic! TC 14.1.2

// End of file SequenceReaderString.cpp


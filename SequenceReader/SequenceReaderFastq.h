/*  Last edited: Sep  6 13:48 2001 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : SequenceReaderFastq
// File Name    : SequenceReaderFastq.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_SequenceReaderFastq
#define INCLUDED_SequenceReaderFastq

// Description:

// Includes:

class WordSequence;
#include "SequenceReaderFasta.h"
#include <string>

// NB it is good practise for #include statements in header files to be
// replaced by forward declarations if at all possible

// ### Class Declarations ###


// Class Name :
// Description: 
class SequenceReaderFastq : public SequenceReaderFile
{

  // PUBLIC MEMBER FUNCTIONS
  public:

  // Constructors and Destructors

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  //  SequenceReaderFastq( const char* fileName, ostream& monitoringStream = cerr);
  SequenceReaderFastq
  ( const char* fileName, 
    SequenceEncoder* pEncoder,
    ostream& monitoringStream = cerr );
  SequenceReaderFastq
  ( const char* fileName, 
    ostream& monitoringStream = cerr );

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  SequenceReaderFastq( const SequenceReaderFastq& rhs);

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  ~SequenceReaderFastq(); 
  // (NB destructor should be virtual if class is to be derived from)

  // Manipulator Functions
  virtual SequenceReader* clone( void ) 
  { 
    return new SequenceReaderFastq( *this ); 
  }


  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT

  // Accessor Functions
  // (NB all accessor functions should be 'const')

  // Function Name: getNextSequence
  // Arguments: WordSequence& (out), int (in)
  // Returns:   int
  // Read the next set of sequence information from the file and parse it
  // into WordSequence format. Returns -1 if a problem, else the number of
  // valid base pairs in the final word of the sequence
  virtual int getNextSequence( WordSequence& nextSeq, int wordLength );

  // PROTECTED MEMBER FUNCTIONS 
  // (visible to this class and derived classes only)
  protected:

  // PRIVATE MEMBER FUNCTIONS
  // (visible to instances of this class only)
  
  private:
  SequenceReaderFastq& operator=(const SequenceReaderFastq&);// NOT IMPLEMENTED

  // PROTECTED DATA:
  // (visible to this class and derived classes only)
  protected:

  // PRIVATE MEMBER DATA
  private:

}; // SequenceReaderFastq

typedef SequenceReaderFastq SequenceReaderFastqDNA; 

class SequenceReaderFastqProtein : public SequenceReaderFastq
{

  // PUBLIC MEMBER FUNCTIONS
  public:

  // Constructors and Destructors

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  SequenceReaderFastqProtein( const char* fileName, 
			      ostream& monitoringStream = cerr );

}; // SequenceReaderFastqProtein



// ### Function Declarations ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

// End of include guard:
#endif

// End of file SequenceReaderFastq.h

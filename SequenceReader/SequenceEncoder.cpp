
// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : SequenceEncoder
// File Name    : SequenceEncoder.cpp
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Description:

// Includes:

#include "SequenceEncoder.h"
#include "SequenceReader.h"

// ### Function Definitions ###
void print( const TranslationTable& tt )
{
  for ( int i(0) ; i < numPossibleChars ; ++ i )
  {
    cout << i << ":";
    if (isgraph((uchar)i)) cout << " (" << (uchar)i << ") ";
    cout << "\t " << (int)tt[i];
    if (isgraph(tt[i])) cout << " (" << tt[i] << ") ";
    cout << endl;
  }
}

SequenceEncoder::SequenceEncoder
  ( const TranslationTable* tt, 
    SourceDataType sourceData,
    int bitsPerSymbol, 
    int wordLength,
    ostream& monitoringStream):
    monitoringStream_( monitoringStream ),
    tt_(tt), sourceData_( sourceData ), bitsPerSymbol_(bitsPerSymbol),
    pState_( new SequenceReaderModeIgnore( monitoringStream ) ),
    wordFlag_(0),
    doubleBitShift_(bitsPerSymbol<<1),
    symbolMask_((1<<bitsPerSymbol)-1)
  //    numSymbolPairs_(wordLength>>1),
  //   oddNumSymbols_(wordLength%1==1),
  { 
    monitoringStream_ << "constructing SequenceEncoder" << endl;
    setWordLength( wordLength );
  }

SequenceEncoder::SequenceEncoder( const SequenceEncoder& rhs ) :
  monitoringStream_( rhs.monitoringStream_ ),
  tt_( rhs.tt_ ),
  ett_( rhs.ett_ ),
  sourceData_( rhs.sourceData_ ),
  bitsPerSymbol_( rhs.bitsPerSymbol_ ),
  wordLength_( rhs.wordLength_ ),
    //  pSeq_( rhs.pSeq_ ),// don't want 2 encoders linking to same seq
  pState_( rhs.pState_->clone() ),
  wordFlag_(0),
  //  numSymbolPairs_(rhs.numSymbolPairs_),
  //  oddNumSymbols_(rhs.oddNumSymbols_),
  symbolMask_(rhs.symbolMask_),
  doubleBitShift_(rhs.doubleBitShift_)
  {
    monitoringStream_ << "copy constructing SequenceEncoder" << endl;
  }

  SequenceEncoder::~SequenceEncoder()
  {
    monitoringStream_ << "destructing SequenceEncoder" << endl;
    delete pState_;
  }

SequenceEncoderProtein::SequenceEncoderProtein
( int wordLength, ostream& monStream ) :
  SequenceEncoder
    ( &ttProtein, gProteinData, gResidueBits, wordLength, monStream ) 
{
  monitoringStream_ << "constructing SequenceEncoderProtein" << endl;
  ett_ = &ettSource_;
  if (!isExpanded_)
  {
    expandTranslationTable( ettSource_ );
    isExpanded_ = true;
  }
}

SequenceEncoderDNA::SequenceEncoderDNA
( int wordLength, ostream& monStream ) :
  SequenceEncoder( &ttDNA, gDNAData, gBaseBits, wordLength, monStream ) 
{
  monitoringStream_ << "constructing SequenceEncoderDNA" << endl;
  ett_ = &ettSource_;
  if (!isExpanded_)
  {
    expandTranslationTable( ettSource_ );
    isExpanded_ = true;
  }
}

SequenceEncoderCodon::SequenceEncoderCodon
( int wordLength, ostream& monStream ) :
  SequenceEncoder
( &ttCodon, gDNAData, gResidueBits, wordLength, monStream ) 
{
  monitoringStream_ << "constructing SequenceEncoderCodon" << endl;
  ett_ = &ettSource_;
  if (!isExpanded_)
  {
    expandTranslationTable( ettSource_ );
    isExpanded_ = true;
  }
}



bool SequenceEncoderDNA::isExpanded_ = false;
bool SequenceEncoderProtein::isExpanded_ = false;
bool SequenceEncoderCodon::isExpanded_ = false;
ExpandedTranslationTable SequenceEncoderDNA::ettSource_;
ExpandedTranslationTable SequenceEncoderProtein::ettSource_;
ExpandedTranslationTable SequenceEncoderCodon::ettSource_;


  // Function Name: changeMode
  // Arguments: const SequenceReaderMode&
  // Makes a copy of mode and uses it to handle mismatch character reads
  void SequenceEncoder::changeMode( SequenceReaderMode* pMode )
  {
    monitoringStream_ << "SequenceReader::changeMode\n";
    delete pState_;
    pState_ = pMode->clone();
  }


void SequenceEncoder::encode( const char* data, int numChars ) 
{
  if (numChars==0) return;

  ushort* p( (ushort*) data );
  // the numChars%2 bit avoids unaligned access
  const ushort* lastp( (ushort*) &data[ numChars - (numChars%2) ] );
  uchar*  pTemp;

  int basesInLast(pSeq_->getNumBasesInLast());
  //  cout << basesInLast << " encoding " << numChars << endl;
  int splitWord( wordLength_-1 );

  Word thisWord(pSeq_->back()), thisCode(0);
  pSeq_->pop_back();

  while (p < lastp)
  {
    //    cout << "XXX:" << ((char*)p)-data << " " << (unsigned int)*((char*)p)
    // << " " << (unsigned int)*(((char*)p)+1) << " " << basesInLast << endl;
    thisCode = (*ett_)[ *p ];
    if ( (thisCode & someCharInvalid) == 0 )
    {
      //      cout << basesInLast << " both valid" << endl;
      if ( basesInLast != splitWord )
      {
	thisWord <<= doubleBitShift_;
	thisWord |= thisCode;
	//	cout << basesInLast << " double shift " 
	//     << printWord( thisWord, wordLength_ ) << endl;
	basesInLast +=2;
	if (basesInLast==wordLength_)
	  addWord( thisWord, wordFlag_, basesInLast );
      }
      else
      {
	thisWord <<= bitsPerSymbol_;

	thisWord |= (( thisCode >> bitsPerSymbol_ ) & symbolMask_ );
	//	cout << basesInLast << " split word " 
	//   << printWord( thisWord, wordLength_ )<< " "  
	//	     << printWord( thisCode, wordLength_ )<< endl;
	addWord( thisWord, wordFlag_, basesInLast );
	thisWord = thisCode & symbolMask_;
	//	cout << basesInLast << " after split " 
	//   << printWord( thisWord, wordLength_ ) << endl;
	basesInLast = 1;
      }
    }
    else
    {
      //      cout << basesInLast << " not both valid" << endl;
      pTemp = (uchar*) p;
      encodeChar( *pTemp, thisWord, wordFlag_, basesInLast );
      ++pTemp;
      encodeChar( *pTemp, thisWord, wordFlag_, basesInLast );
      
    } // ~else
    p++;
  } // ~while

  //  cout << "got to end.." << ((uchar*)p-(uchar*)data) << " " << numChars << endl;
  assert(p==lastp);
  if (numChars%2==1)
  {
    pTemp = (uchar*)p;
    //    cout << basesInLast << " doing odd char at end " << *pTemp << endl;
    encodeChar( *p, thisWord, wordFlag_, basesInLast );
  }

  pSeq_->setNumBasesInLast(basesInLast);
  pSeq_->push_back(thisWord);


  //  cout << basesInLast << " leaving " 
  //   << printWord(thisWord, wordLength_) << endl;
}

void SequenceEncoder::encodeChar
( uchar thisChar, Word& thisWord, Word& thisFlag, int& basesInLast )
{
  Word thisCode;
  //  cout << basesInLast << " encodeChar... " << thisChar << endl;
  if ( (*tt_)[ thisChar ] == nv ) 
      pState_->mismatch( thisChar, thisFlag );
  
  //  cout << basesInLast << " ... converted to " << thisChar << endl;
  thisCode = (*tt_)[ thisChar ];

  if ( thisCode == nv ) 
  {
    //    cout << basesInLast << " ... not valid " << thisChar << endl;
    return;
  }
  thisWord <<= bitsPerSymbol_;
  thisWord |= thisCode;
  //  cout << basesInLast << " encode char " 
  //   << printWord( thisWord, wordLength_ ) << endl;
   
  basesInLast++;
  if (basesInLast==wordLength_) addWord( thisWord, thisFlag, basesInLast );

}

void SequenceEncoder::addWord
( Word& thisWord, Word& thisFlag, int& basesInLast )
{

  thisWord |= thisFlag;
  pSeq_->push_back(thisWord);
  //  cout << basesInLast << " adding Word " 
  //   << printWord( thisWord, wordLength_) << " " << printResidue( thisWord, wordLength_ ) << " " << thisFlag << endl;
  thisFlag=0;
  thisWord=0;
  basesInLast=0;
}



void SequenceEncoder::unlinkSeq( void )
{
  if (pSeq_->getNumBasesInLast() != 0)
    {
      // Store the stage of the flag ...
      Word curseFlag = ( pSeq_->back() & gCursedWord );

      pSeq_->back() 
      	<<= (bitsPerSymbol_*(wordLength_-pSeq_->getNumBasesInLast()));

      // ... and restore it after the shift
      pSeq_->back() |= curseFlag;
	   
    }
  //  else pSeq_->pop_back();
  pSeq_ = NULL;
}


void SequenceEncoder::expandTranslationTable
( ExpandedTranslationTable& ett )
{
  monitoringStream_ << "expanding the ttable" << endl;
  uchar a[2];
  unsigned short* b = (unsigned short*) &a[0];
  a[0]=0; a[1]=0;
  Word w[2];

  do  
  {
    do
    {
      //      cout << (unsigned int) a[0] << " " 
      //   << ( &((*tt_)[a[0]]) - &((*tt_)[0])) << endl;	      
      //      cout << (unsigned int) a[0] << " " << (unsigned int) a[1] 
      //	   << " " << *b << endl;
      //      ett[ *b ] = (*tt_)[ (unsigned int)a[0] ] == nv ) 
      w[ 0 ] = (*tt_)[ a[0] ];
      w[ 1 ] = (*tt_)[ a[1] ];
      if ( ( w[0] == nv ) || ( w[1] == nv ) )
      {
      	ett[ *b ] = (w[0]==nv)*firstCharInvalid;
      	ett[ *b ] |= (w[1]==nv)*secondCharInvalid;
      }
      else
      {	
	ett[ *b ] = w[0];
	ett[ *b ] <<= bitsPerSymbol_;
	ett[ *b ] |= w[1];
	
      }
    } // ~for j
    while( ++a[1] != 0 );
  } // ~for i
  while( ++a[0] != 0 );
  monitoringStream_ << "done expanding" << endl;


}



  // Function Name: codonize
  // Arguments: WordSequence& (in), vector<char> out 
  // Returns:   void
  // Splits in into codons, one codon per char 
  void codonize
    ( const WordSequence& in, CodonList& codons, int readingFrame )
  {
    //    cout << "Codonizing for reading frame " << readingFrame << endl;
    static const Word mask((1<<gCodonBits)-1);

    int shift( gBitsPerWord-gCodonBits-gBaseBits*readingFrame );


    WordSequence::const_iterator i(in.begin());
    //    vector<char> codons;
    int codonsSoFar(0);
    int totalCodons
      ( (gMaxBasesPerWord*(in.size()-1) 
	  + in.getNumBasesInLast() - readingFrame)/gBasesPerCodon );

    Word thisCodon;

    while( 1 )
    {
    
      if (shift>=0)
      {
	thisCodon = 0;
      }
      else
      {
	thisCodon = (*i<<-shift);
	i++;
	shift += gBitsPerWord;
      } 
      thisCodon |= (*i>>shift);
      thisCodon &= mask;

      codons.push_back(thisCodon);
      if (++codonsSoFar==totalCodons) break;
      if (shift == 0) { shift = gBitsPerWord; i++; }
      shift -= gCodonBits;

    } // ~while
  
    //    pEncoder_->encode( codons );


  } // ~SequenceReaderCodon::codonize

// Function Name: codonizeAndFlag
// Arguments: WordSequence& (in), vector<char> out 
// Returns:   void
// Splits a WordSequence of 2 bit per base DNA into codons
// Used only when creating a HashTableTranslated
// Assumes the DNA has a word size of 15
// This leaves the MSB of a Word free to act as a flag
// If an entry of codons is equal to '!' then the flag bit
// was set in the DNA Word(s) it came from, meaning the original DNA
// test was flagged, typically because it contained Ns or -s
// If codons are then encoded using a SequenceEncoderCodon
// with mode set to SequenceReaderModeFlagReplace('X'), this '!'
// character is not recognized and the Word it is in is flagged
// Point of all this is that repeat masked DNA can be excluded from
// the HashTableTranslated

void codonizeAndFlag
( const WordSequence& in, CodonList& codons, int readingFrame )
{

  static const int DNAWordSizeForHashing(gMaxBasesPerWord-1);

  int codonsInLast((in.getNumBasesInLast()-readingFrame)/gNumReadingFrames);

  //  cout << "blx: " << in.size() << " " << in.getNumBasesInLast() << " " << codonsInLast << endl;
  int numCodons
    (   ( (in.size() == 0) 
	  ? 0
	  : ((in.size()-1)*DNAWordSizeForHashing)+in.getNumBasesInLast()
	  -readingFrame )
      / gNumReadingFrames );

  codons.resize(numCodons);
  char* pCodon(static_cast<char*>(&(*codons.begin())));
  char* pEnd(static_cast<char*>(&(*codons.end())));
  WordSequence::const_iterator i(in.begin());
  WordSequence::const_iterator lastWord(in.end()-1);
  Word toCarry(~0);
  Word lastWordFlag(0);

  //  const Word maskBase(0x3);
  //  const Word maskCodon((0x3>>4)|(0x3>>2)|(0x3>>4));
  

  if (readingFrame==0)
  {
    for( ; i!=lastWord ; ++i )
    {

      *(pCodon++) = getCodonFromWord<4,0>(*i);
      *(pCodon++) = getCodonFromWord<3,0>(*i);
      *(pCodon++) = getCodonFromWord<2,0>(*i);
      *(pCodon++) = getCodonFromWord<1,0>(*i);
      *(pCodon++) = getCodonFromWord<0,0>(*i);

    } // ~for

    if (codonsInLast>=1)
      *(pCodon++) = getCodonFromWord<4,0>(*i);
    if (codonsInLast>=2)
      *(pCodon++) = getCodonFromWord<3,0>(*i);
    if (codonsInLast>=3)
      *(pCodon++) = getCodonFromWord<2,0>(*i);
    if (codonsInLast==4)
      *(pCodon++) = getCodonFromWord<1,0>(*i);
  } // ~if 
  else if (readingFrame==1)
  {

    for( ; i!=lastWord ; ++i )
    {
      if (toCarry!=~0)
      (*pCodon++) = ( ((*i)&gCursedWord)|lastWordFlag )
	? flaggedChar
	: ( toCarry | (((*i) >> (4*gCodonBits + 2*gBaseBits))&maskBase ));

      *(pCodon++) = getCodonFromWord<3,2>(*i);
      *(pCodon++) = getCodonFromWord<2,2>(*i);
      *(pCodon++) = getCodonFromWord<1,2>(*i);
      *(pCodon++) = getCodonFromWord<0,2>(*i);

      lastWordFlag = (*i)&gCursedWord;
      toCarry = ((*i)& mask2Bases) << gBaseBits;
 
    } // ~for
    if (in.getNumBasesInLast() >= readingFrame )
    {
      *(pCodon++) = ( ((*i)&gCursedWord)|lastWordFlag )
	? flaggedChar
	: ( toCarry | (((*i) >> (4*gCodonBits + 2*gBaseBits))&maskBase ));
    }
    if (codonsInLast>=1)
      *(pCodon++) = getCodonFromWord<3,2>(*i);
    if (codonsInLast>=2)
      *(pCodon++) = getCodonFromWord<2,2>(*i);
    if (codonsInLast>=3)
      *(pCodon++) = getCodonFromWord<1,2>(*i);
    if (codonsInLast>=4)
      *(pCodon++) = getCodonFromWord<0,2>(*i);

  } // ~else if
  else // if (readingFrame==2)
  {

    for( ; i!=lastWord ; ++i )
    {
      if (toCarry!=~0)
      (*pCodon++) = ( ((*i)&gCursedWord)|lastWordFlag )
	? flaggedChar
	: ( toCarry | (((*i) >> (4*gCodonBits + gBaseBits))&mask2Bases ));

      *(pCodon++) = getCodonFromWord<3,1>(*i);
      *(pCodon++) = getCodonFromWord<2,1>(*i);
      *(pCodon++) = getCodonFromWord<1,1>(*i);
      *(pCodon++) = getCodonFromWord<0,1>(*i);

      lastWordFlag = (*i)&gCursedWord;
      toCarry = ((*i)& maskBase) << (2*gBaseBits);
 
    } // ~for
    if (in.getNumBasesInLast() >= readingFrame )
    {
    
      *(pCodon++) = ( ((*i)&gCursedWord)|lastWordFlag )
	? flaggedChar
	: ( toCarry | (((*i) >> (4*gCodonBits + gBaseBits))&mask2Bases ));
    }
    if (codonsInLast>=1)
      *(pCodon++) = getCodonFromWord<3,1>(*i);
    if (codonsInLast>=2)
      *(pCodon++) = getCodonFromWord<2,1>(*i);
    if (codonsInLast>=3)
      *(pCodon++) = getCodonFromWord<1,1>(*i);
    if (codonsInLast>=4)
      *(pCodon++) = getCodonFromWord<0,1>(*i);

  } // ~else if

  //  cout << "codons fwd: " << codons << endl;
  assert(pCodon==pEnd);

} // ~void codonizeAndFlag


// Function Name: codonizeAndFlagReverse
// Arguments: WordSequence& (in), vector<char> out 
// Returns:   void
// Codonizes the reverse complement of a WordSequence as described
// for codonizeAndFlag above. Extra step at the end to reverse
// each of the codons
void codonizeAndFlagReverse
( const WordSequence& in, CodonList& codons, int readingFrame )
{

  static const int DNAWordSizeForHashing(gMaxBasesPerWord-1);

  int codonsInLast((in.getNumBasesInLast()-readingFrame)/gNumReadingFrames);


  int numCodons
    (   ( (in.size() == 0) 
	  ? 0
	  : ((in.size()-1)*DNAWordSizeForHashing)+in.getNumBasesInLast()
	  -readingFrame )
      / gNumReadingFrames );

  readingFrame = (in.getNumBasesInLast()-readingFrame+gNumReadingFrames)%gNumReadingFrames;



  //  cout << "blx: " << in.size() << " " << in.getNumBasesInLast() << " " << readingFrame << " " << codonsInLast << " " << numCodons << endl;
  codons.resize(numCodons);
  char* pCodon(static_cast<char*>(&(*codons.begin())));
  char* pEnd(static_cast<char*>(&(*codons.end())));
  WordSequence::const_iterator i(in.end()-1);
  WordSequence::const_iterator lastWord(in.begin());
  Word toCarry(~0);
  Word lastWordFlag(0);

  //  const Word maskBase(0x3);
  //  const Word maskCodon((0x3>>4)|(0x3>>2)|(0x3>>4));
  

  if (readingFrame==0)
  {
    if (codonsInLast==4)
      *(pCodon++) = getCodonFromWord<1,0>(*i);
    if (codonsInLast>=3)
      *(pCodon++) = getCodonFromWord<2,0>(*i);
    if (codonsInLast>=2)
      *(pCodon++) = getCodonFromWord<3,0>(*i);
    if (codonsInLast>=1)
      *(pCodon++) = getCodonFromWord<4,0>(*i);

    do
    {
      i--;
      *(pCodon++) = getCodonFromWord<0,0>(*i);
      *(pCodon++) = getCodonFromWord<1,0>(*i);
      *(pCodon++) = getCodonFromWord<2,0>(*i);
      *(pCodon++) = getCodonFromWord<3,0>(*i);
      *(pCodon++) = getCodonFromWord<4,0>(*i);

    } 
    while (i!=lastWord);

  } // ~if 
  else if (readingFrame==1)
  {
    //    cout << "rf1" << endl;
    if (codonsInLast==4)
      *(pCodon++) = getCodonFromWord<0,2>(*i);
    if (codonsInLast>=3)
      *(pCodon++) = getCodonFromWord<1,2>(*i);
    if (codonsInLast>=2)
      *(pCodon++) = getCodonFromWord<2,2>(*i);
    if (codonsInLast>=1)
      *(pCodon++) = getCodonFromWord<3,2>(*i);

    if (in.getNumBasesInLast()>=readingFrame)
    {
      lastWordFlag = (*i)&gCursedWord;
      toCarry = ((*i) >> (4*gCodonBits + 2*gBaseBits))&maskBase ;
    } // ~if

    do
    {
      i--;
      if (toCarry!=~0)
      (*pCodon++) = ( ((*i)&gCursedWord)|lastWordFlag )
	? flaggedChar
	: ( toCarry | ( ((*i) & mask2Bases ) << gBaseBits ) );

      *(pCodon++) = getCodonFromWord<0,2>(*i);
      *(pCodon++) = getCodonFromWord<1,2>(*i);
      *(pCodon++) = getCodonFromWord<2,2>(*i);
      *(pCodon++) = getCodonFromWord<3,2>(*i);
      lastWordFlag = (*i)&gCursedWord;
      toCarry = ((*i) >> (4*gCodonBits + 2*gBaseBits))&maskBase ;


    } 
    while (i!=lastWord);


  } // ~else if
  else // if (readingFrame==2)
  {
    //    cout << "rf2" << endl;

    if (codonsInLast==4)
      *(pCodon++) = getCodonFromWord<0,1>(*i);
    if (codonsInLast>=3)
      *(pCodon++) = getCodonFromWord<1,1>(*i);
    if (codonsInLast>=2)
      *(pCodon++) = getCodonFromWord<2,1>(*i);
    if (codonsInLast>=1)
      *(pCodon++) = getCodonFromWord<3,1>(*i);

    if (in.getNumBasesInLast()>=readingFrame)
    {
      lastWordFlag = (*i)&gCursedWord;
      toCarry = ((*i) >> (4*gCodonBits + gBaseBits))&mask2Bases ;
    } // ~if

    do
    {
      i--;
      if (toCarry!=~0)
      (*pCodon++) = ( ((*i)&gCursedWord)|lastWordFlag )
	? flaggedChar
	: ( toCarry | ( ((*i) & maskBase ) << (2*gBaseBits) ) );

      *(pCodon++) = getCodonFromWord<0,1>(*i);
      *(pCodon++) = getCodonFromWord<1,1>(*i);
      *(pCodon++) = getCodonFromWord<2,1>(*i);
      *(pCodon++) = getCodonFromWord<3,1>(*i);
      lastWordFlag = (*i)&gCursedWord;
      toCarry = ((*i) >> (4*gCodonBits + gBaseBits))&mask2Bases ;

    } 
    while (i!=lastWord);


  } // ~else if


  assert(pCodon==pEnd);
  //  cout << "codons unrev: " << codons << endl;

  for (CodonList::iterator j(codons.begin()); j!=codons.end(); ++j)
  {
    
    *j = 
      ( ((*j)==flaggedChar) 
	? flaggedChar 
	:
	( ((*j)&maskBase^maskBase) << (2*gBaseBits) )
	| ((*j)&(maskBase<<gBaseBits)^(maskBase<<gBaseBits))
	| ( ((*j)&(maskCodon^mask2Bases)^(maskCodon^mask2Bases)) 
	    >> (2*gBaseBits) )  );
  } // ~for
  //  cout << "codons rev: " << codons << endl;

} // ~void codonizeAndFlagReverse



ostream& operator<<( ostream& os, CodonList& c )
{
  for (vector<char>::iterator i(c.begin()); i!=c.end(); i++ )
    os << (unsigned int)*i << "-" << 
      ((*i==flaggedChar) ? '!' : gCodonNames[(unsigned int)*i]) << " ";
  return os;
} // ~ostream& operator<<( ostream& os, CodonList& c )





// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

// End of file SequenceEncoder.cpp


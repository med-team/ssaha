/*  Last edited: Apr 18 17:49 2002 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : SequenceReaderFilter
// File Name    : SequenceReaderFilter.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_SequenceReaderFilter
#define INCLUDED_SequenceReaderFilter

// Description:

// Includes:

#include "SequenceReader.h"
#include "GlobalDefinitions.h"
//#include <set>
//#include <hash_set>
// NB it is good practise for #include statements in header files to be
// replaced by forward declarations if at all possible

// ### Class Declarations ###

struct HashFunctor
{
  size_t operator()(const char* s) const 
  { 
    unsigned long h = 0; 
    for ( ; *s; ++s)
      h = 5*h + *s;
    return size_t(h);
  } // ~operator()
}; // ~struct HashFunctor


class StringHash : public vector<string>
{
public:
  HashFunctor H_;

  void makeBins( int numBins );

  size_t makeNumBins( void ) const;

  void makeBins( void )
  {
    makeBins(makeNumBins());
  }

  bool isPresent( const string& s )
  {
    vector<string*>* myBin = &bins_[ H_(s.c_str()) % numBins_ ];
    for (vector<string*>::iterator i(myBin->begin()); i!= myBin->end();++i)
      if (**i == s ) return true;
    return false;
  }

  size_t max( void ) const 
  { 
    int max(0);
    for (vector<vector<string*> >::const_iterator i(bins_.begin()); i!= bins_.end(); i++ )
      if (i->size()>max) max=i->size();
    return max;
  } 

  double loadFactor( void ) const { return size()/numBins_; }

  double effic( void ) const { return max()/loadFactor(); }

  size_t numBins( void ) const { return numBins_; }

private:

  size_t numBins_;
  vector<vector<string*> > bins_;

}; // ~class StringHash


// -----------------------------
// Class Name : SequenceReaderFilterState
// Description: This preserves the state (ie position in a file) of
// a SequenceReaderFilter
class SequenceReaderFilterState : public SequenceReaderState
{
 public:
  SequenceReaderFilterState
    ( SequenceNumber lsn, SequenceReader* ps ) :
    pState_(ps->saveState()), SequenceReaderState(lsn) {}
  virtual ~SequenceReaderFilterState() {} //delete pState_;
  // no point in making this private as it's const
  // this is state info for *ps, whatever it is
  SequenceReaderState* pState_;
};




// Class Name : SequenceReaderFilter
// Description: Takes a pointer to another SequenceReader plus a list of
// sequence names to exclude
 

class SequenceReaderFilter : public SequenceReader
{


  // PUBLIC MEMBER FUNCTIONS
  public:
  // Constructors and Destructors

  // Function Name: Constructor
  // Arguments: SequenceReader*, int, ostream&
  // NB SequenceReaderFilter takes ownership of pSeq, ie
  // it deletes it when it itself is destructed
  // SequenceReaderWhatever s( ... );
  // SequenceReader* p = new SequenceReaderWhatever( ... );
  // SequenceReaderFilter f( p, "name" ); // OK
  // SequenceReaderFilter f( s->clone(), "name" ); // OK - takes a copy
  // SequenceReaderFilter f( &s, "name" ); // don't do this ... bad

  SequenceReaderFilter
  ( SequenceReader* pSeq, 
    ifstream* pFilterSource,
    ostream& monitoringStream = cerr );


  SequenceReaderFilter
  ( SequenceReader* pSeq, 
    const char* filterFileName, 
    ostream& monitoringStream = cerr );


  // Function Name: Copy constructor
  // Arguments:
  SequenceReaderFilter( const SequenceReaderFilter& rhs );


  // Function Name: Destructor
  // Arguments:
  ~SequenceReaderFilter(); 
  // (NB destructor should be virtual if class is to be derived from)

  // Manipulator Functions
  virtual SequenceReader* clone( void ) 
  { 
    return new SequenceReaderFilter( *this ); 
  }

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT

  // Function Name: readFilterNames
  // Arguments: ifstream*
  // Returns: void
  // Reads in a list of names to be filtered from an ifstream*
  // into set<string>*. Once done, deletes the ifstream
  void readFilterNames( ifstream* pFilterSource );


  // Function Name: changeMode
  // Arguments: const SequenceReaderMode&
  // Makes a copy of mode and uses it to handle mismatch character reads
  // Does nothing here because any character reading necessary will have 
  // been done by the SequenceReader this class is constructed from 
  virtual void changeMode( SequenceReaderMode* pMode ) 
  {
    pSeq_->changeMode( pMode );
  }

  // Accessor Functions
  // (NB all accessor functions should be 'const')

  // Function Name: rewind
  // Arguments: void
  // Returns:   void
  // Rewind to the start of the data file, so that getNextSequence will
  // return the first sequence in the file
  virtual void rewind( void ); 

  // Function Name: getNextSequence
  // Arguments: WordSequence& (out), int (in)
  // Returns:   int
  // Read the next set of sequence information from the file and parse it
  // into WordSequence format. Returns -1 if there has been a problem with
  // reading the sequence, else returns the number of valid base pairs 
  // contained within the final word of the sequence.
  virtual int getNextSequence( WordSequence& nextSeq, int wordLength );

  // Function Name: getSequence
  // Arguments: WordSequence& (out), SequenceNumber (in), int (in)
  // Returns:   bool
  // Read the sequenceNumber-th set of sequence information from the file and 
  // parse it into WordSequence format
  virtual int getSequence
  ( WordSequence& nextSeq, SequenceNumber sequenceNumber, int wordLength );

  // Function Name: getLastSequenceName
  // Arguments: string& (out)
  // Returns:   void
  // Fills the string with the name of the last sequence read 
  virtual void getLastSequenceName( string& seqName ) const;

  // Function Name: getBitsPerSymbol
  // Arguments: none
  // Returns:   int
  // Returns number of bits per symbol used in encoding
  virtual int getBitsPerSymbol ( void ) const 
  {
    return pSeq_->getBitsPerSymbol();
  }

  // Function Name: getSourceDataType
  // Arguments: none
  // Returns:   SourceDataType
  // Returns type of data being encoded (protein or DNA)
  virtual SourceDataType getSourceDataType( void ) const
  {
    return pSeq_->getSourceDataType();
  }

  // Function Name: getNumFiltered
  // Arguments: none
  // Returns:   int
  // Returns number of sequences excluded so far from pSeq_
  // if (areAllSequencesRead()==true) then this is the final number
  int getNumFiltered( void ) const
  {
    return numFiltered_;
  }

  // Function Name: printName
  // Arguments: string& (out), SequenceNumber (in)
  // Returns:   void
  // Fills a string with the name of the requested sequence
  virtual bool printName( ostream& os, SequenceNumber seqNum );

  // Function Name: printSideInfo
  // Arguments: string& (out), SequenceNumber (in)
  // Returns:   void
  // Fills a string with the name of the requested sequence
  virtual bool printSideInfo( ostream& os, SequenceNumber seqNum );

  // Function Name: printSource
  // Arguments: string& (out), SequenceNumber (in)
  // Returns:   void
  // Fills a string with the name of the requested sequence
  virtual bool printSource( ostream& os, SequenceNumber seqNum );

  virtual bool findSequence( SequenceNumber seqNum );


  // Function Name: extractSource
  // This extracts the source data for bases seqStart to seqEnd inclusive
  // of sequence seqNum and places it in source
  virtual void extractSource( char** pSource,//vector<char>& source, 
		     SequenceNumber seqNum,
		     SequenceOffset seqStart,
		     SequenceOffset seqEnd );

  // Function Name: saveIndexImp
  // This makes and saves the index for *this, by kinda monkeying around
  // with the index for *pSeq_ 
  virtual void saveIndexImp
    ( ostream& fileFile, 
      ostream& indexFile, 
      int& fileNumber );

  // Function Name: saveState
  // Arguments: void
  // Returns:   SequenceReaderState*
  // saves the state (ie current file position) of a SequenceReader for future
  // restoration
  virtual SequenceReaderState* saveState( void ) const
  {
    return new SequenceReaderFilterState( lastSequenceNumber_, pSeq_ );
  }

  // Function Name: restoreState
  // Arguments:   SequenceReaderState*
  // Returns:     void
  // restores the state (ie current file position) of a SequenceReader
  // then (NB!!) deletes *pState;
  virtual void restoreState( SequenceReaderState* pState ) 
  {
    SequenceReaderFilterState* p
      (dynamic_cast<SequenceReaderFilterState*>(pState));
    assert(p!=NULL);
    lastSequenceNumber_ = p->lastSequenceNumber_;
    pSeq_->restoreState( p->pState_ );
    delete pState;
  }

  // PROTECTED MEMBER FUNCTIONS 
  // (visible to this class and derived classes only)
  protected:

  // Function Name: computeNumSequencesInFile
  // Arguments: void
  // Returns:   SequenceNumber
  // Returns the number of sequences in the file - called by 
  // getNumSequencesInFile. 
  virtual SequenceNumber computeNumSequencesInFile( void );

  // PRIVATE MEMBER FUNCTIONS
  // (visible to instances of this class only)
  
  private:
  SequenceReaderFilter& operator=(const SequenceReaderFilter&);   // NOT IMPLEMENTED

  // PROTECTED DATA:
  // (visible to instances of this class only)
  protected:
  SequenceReader* pSeq_;
  //  string filterFileName_;
  //  set<string>* pFilterNames_;
  StringHash* pFilterNames_;
  // filterNums_ - element i of filterNums_ is the number of the sequence
  // in *pSeq corresponding to sequence i of *this
  // (so filterNums_[0] is not used)
  vector<SequenceNumber> filterNums_;
  int numFiltered_;

  //  int wordLength_;
  //  int bitsPerSymbol_;
  //  SourceDataType sourceData_;

  // PRIVATE MEMBER DATA
  private:

}; // ~class SequenceReaderFilter



// ### Function Declarations ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

// End of include guard:
#endif

// End of file SequenceReaderFilter.h

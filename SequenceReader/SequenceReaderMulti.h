/*  Last edited: Feb 21 18:18 2002 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : SequenceReaderMulti
// File Name    : SequenceReaderMulti.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_SequenceReaderMulti
#define INCLUDED_SequenceReaderMulti

// Description:

// #pragma interface
// Includes:
#include "SequenceReader.h"
#include <memory>
#include <algorithm>
#include <vector>

// NB it is good practise for #include statements in header files to be
// replaced by forward declarations if at all possible

// ### Class Declarations ###

  struct SeqReaderInfo
  {
    SequenceReader* ptr_;
    int             size_;
    bool            allSeqsRead_;
    SeqReaderInfo( SequenceReader* ptr ) : 
      ptr_( ptr ), size_( 0 ), allSeqsRead_( false ) 
      {
      } 
    SeqReaderInfo( void ) : 
      ptr_( NULL ), size_( 0 ), allSeqsRead_( false )
      {
      } 
    SeqReaderInfo( const SeqReaderInfo& rhs ) : 
      //      ptr_( rhs.ptr_->clone() ), size_( rhs.size_ ), allSeqsRead_( rhs.allSeqsRead_ ) {} 
      ptr_( rhs.ptr_ ), size_( rhs.size_ ), allSeqsRead_( rhs.allSeqsRead_ ) 
      {
      } 
    ~SeqReaderInfo() 
      { 
      }

  };


// -----------------------------
// Class Name : SequenceReaderMultiState
// Description: This preserves the state (ie position in a file) of
// a SequenceReaderMulti
class SequenceReaderMultiState : public SequenceReaderState
{
 public:
  SequenceReaderMultiState
    ( SequenceNumber lsn, 
      vector<SeqReaderInfo>::iterator tr,
      SequenceReaderState* ps ) :
  thisReader_(tr),  
    pState_(ps), SequenceReaderState(lsn) {}
  virtual ~SequenceReaderMultiState() {} //delete pState_;
  // no point in making this private as it's const
  const vector<SeqReaderInfo>::iterator thisReader_;
  // this is state info for *thisReader, whatever it is
  SequenceReaderState* pState_;
};


// Class Name : SequenceReaderMulti
// Description: The purpose of this class is to enable multiple 
// SequenceReaders to 'act as one.' This enables for example a directory
// of fasta files to be processed in the same way as a single one.
class SequenceReaderMulti : public SequenceReader
{


  // PUBLIC MEMBER FUNCTIONS
  public:

  // Constructors and Destructors

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  SequenceReaderMulti( ostream& monitoringStream = cerr );

  SequenceReaderMulti( const SequenceReaderMulti& rhs); 


  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT
  virtual ~SequenceReaderMulti(); 
  // (NB destructor should be virtual if class is to be derived from)

  // Manipulator Functions
  virtual SequenceReader* clone( void ) 
  { return new SequenceReaderMulti( *this ); }


  // Function Name: addReader
  // Arguments: SequenceReader&
  // A copy of seq is made (which is why SequenceReader and its subclasses
  // need to have a copy constructor) and a pointer to it is placed in 
  // allReaders. 
  void addReader( SequenceReader& seq );

  // Function Name: addReader
  // Arguments: SequenceReader*
  // pSeq is placed in allReaders_. NB no copy of pSeq is made: use by
  // creating a new SequenceReader and calling this. The SeqReaderMulti
  // takes ownership of *pSeq and is responsible for its destruction
  void addReader( SequenceReader* pSeq );


  // Function Name: changeMode
  // Arguments: const SequenceReaderMode&
  // Makes a copy of mode and uses it to handle mismatch character reads
  virtual void changeMode( SequenceReaderMode* pMode );


  // Accessor Functions
  // (NB all accessor functions should be 'const')

  int getNumReaders( void )
  { 
    return allReaders_.size();
  }

  // Function Name: findReader
  // Arguments: SequenceNumber& (in/out)
  // Given an input sequence number, adjusts seqNum and thisReader_
  // so that the seqNum'th sequence of *this is obtained by passing
  // the adjusted value of seqNum to *thisReader_
  // Returns true if the find was successful, else returns false
  // (in which case thisReader_ and seqNum are not adjusted)
  bool findReader( SequenceNumber& seqNum );
  
  // Function Name: rewind
  // Arguments: void
  // Returns:   void
  // Rewind to the start of the data file, so that getNextSequence will
  // return the first sequence in the file
  virtual void rewind( void );

  // Function Name: findSequence
  // Arguments: SequenceNumber (in)
  // Returns:   void
  // Winds the input file stream to the start of sequence number seqNum. 
  // Returns false if seqNum exceeds the number of sequences in
  // the file.
  virtual bool findSequence( SequenceNumber seqNum );

  // Function Name: getNextSequence
  // Arguments: WordSequence& (out), int (in)
  // Returns:   int
  // Read the next set of sequence information from the file and parse it
  // into WordSequence format. Returns -1 if there has been a problem with
  // reading the sequence, else returns the number of valid base pairs 
  // contained within the final word of the sequence
  virtual int getNextSequence( WordSequence& nextSeq, int wordLength );

  // Function Name: getSequence
  // Arguments: WordSequence& (out), SequenceNumber (in), int (in)
  // Returns:   bool
  // Read the sequenceNumber-th set of sequence information from the file and 
  // parse it into WordSequence format
  virtual int getSequence
    ( WordSequence& nextSeq, SequenceNumber sequenceNumber, int wordLength );

  // Function Name: getLastSequenceName
  // Arguments: string& (out)
  // Returns:   void
  // Fills the string with the name of the last sequence read 
  virtual void getLastSequenceName( string& seqName ) const;

  // Function Name: getBitsPerSymbol
  // Arguments: none
  // Returns:   int
  // Returns number of bits per symbol used in encoding
  virtual int getBitsPerSymbol ( void ) const;

  // Function Name: getSourceDataType
  // Arguments: none
  // Returns:   SourceDataType
  // Returns type of data being encoded (protein or DNA)
  virtual SourceDataType getSourceDataType( void ) const;

  // Function Name: printName
  // Arguments: string& (out), int (in)
  // Returns:   void
  // Fills a string with the name of the requested sequence
  virtual bool printName( ostream& os, SequenceNumber seqNum );

  // Function Name: printSideInfo
  // Arguments: string& (out), SequenceNumber (in)
  // Returns:   void
  // Fills a string with the name of the requested sequence
  virtual bool printSideInfo( ostream& os, SequenceNumber seqNum );

  // Function Name: printSource
  // Arguments: string& (out), SequenceNumber (in)
  // Returns:   void
  // Fills a string with the name of the requested sequence
  virtual bool printSource( ostream& os, SequenceNumber seqNum );

  // Function Name: extractSource
  // This extracts the source data for bases seqStart to seqEnd inclusive
  // of sequence seqNum and places it in source
  virtual void extractSource( char** pSource,//vector<char>& source, 
		     SequenceNumber seqNum,
		     SequenceOffset seqStart,
		     SequenceOffset seqEnd );

  // Function Name: saveIndexImp
  // Actually save the indexing data to disk. Implemented
  // for SequenceReaderFile and SequenceReaderMulti, not
  // for SourceReaderIndex
  virtual void saveIndexImp
    ( ostream& fileFile, 
      ostream& indexFile, 
      int& fileNumber );


  // Function Name: saveState
  // Arguments: void
  // Returns:   SequenceReaderState*
  // saves the state (ie current file position) of a SequenceReader for future
  // restoration
  virtual SequenceReaderState* saveState( void ) const
  {
    return new SequenceReaderMultiState
      ( lastSequenceNumber_, 
	thisReader_, 
	(thisReader_==allReaders_.end()) 
	? NULL 
	: thisReader_->ptr_->saveState() );
  }

  // Function Name: restoreState
  // Arguments:   SequenceReaderState*
  // Returns:     void
  // restores the state (ie current file position) of a SequenceReader
  // then (NB!!) deletes *pState;
  virtual void restoreState( SequenceReaderState* pState ) 
  {
    SequenceReaderMultiState* p
      (dynamic_cast<SequenceReaderMultiState*>(pState));
    assert(p!=NULL);
    lastSequenceNumber_ = p->lastSequenceNumber_;
    thisReader_->ptr_->rewind();
    thisReader_=p->thisReader_; 
    if (thisReader_!=allReaders_.end())
      thisReader_->ptr_->restoreState( p->pState_ ); 
    delete pState;
  }

  // PROTECTED MEMBER FUNCTIONS 
  // (visible to this class and derived classes only)
  protected:

  // Function Name: computeNumSequencesInFile
  // Arguments: void
  // Returns:   SequenceNumber
  // Returns the number of sequences in the file (will be done by lazy 
  // initialization, i.e. will only be calculated if asked for. NB this
  // will lose the current place in the file)
  virtual SequenceNumber computeNumSequencesInFile( void );


  // PRIVATE MEMBER FUNCTIONS
  // (visible to instances of this class only)
  
  private:
  SequenceReaderMulti( const SequenceReader&);             // NOT IMPLEMENTED
  SequenceReaderMulti& operator=(const SequenceReader&);   // NOT IMPLEMENTED

  // PROTECTED DATA:
  // (visible to instances of this class only)
  protected:

  vector<SeqReaderInfo> allReaders_;
  //  vector<SequenceNumber>             numSeqs_;
  vector<SeqReaderInfo>::iterator thisReader_;
  SequenceNumber currentSeqNum_;

  bool isFirstSeq_;
  int  bitsPerSymbol_;
  SourceDataType sourceDataType_;

  // PRIVATE MEMBER DATA
  private:

}; // SequenceReaderMulti








// ### Function Declarations ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

// End of include guard:
#endif

// End of file SequenceReaderMulti.h

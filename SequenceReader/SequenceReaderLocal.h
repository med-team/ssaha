/*  Last edited: Jan 14 13:25 2002 (ac2) */

// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : SequenceReaderLocal
// File Name    : SequenceReaderLocal.h
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Include guard:
#ifndef INCLUDED_SequenceReaderLocal
#define INCLUDED_SequenceReaderLocal

// Description:

// Includes:

#include "SequenceReader.h"
#include "GlobalDefinitions.h"
#include <vector>
// NB it is good practise for #include statements in header files to be
// replaced by forward declarations if at all possible

// ### Class Declarations ###

// Class Name : SequenceReaderLocal
// Description: Classes such as SequenceReaderFasta essentially scan through a
// file sequentially. This is not always a problem, but may be slow if random
// access to sequences is required. SequenceReaderLocal takes sequence data
// from another instance of SequenceReader and holds it in local memory
// for fast random access. Downside is that a) more memory is used and
// b) the number of base pairs per word is fixed at construction time (the
// latter could be remedied but I'm not convinced it's worth it)
class SequenceReaderLocal : public SequenceReader
{


  // PUBLIC MEMBER FUNCTIONS
  public:
  typedef pair< WordSequence, std::string> SequenceInfo;

  // Constructors and Destructors

  // Function Name: Constructor
  // Arguments: SequenceReader&, int, ostream&
  // Takes the data from seqFile and places it in seqData, seqBasesInLast
  // and seqNames
  SequenceReaderLocal
  ( SequenceReader& seqFile, 
    int wordLength, 
    ostream& monitoringStream = cerr );

  // Function Name: Constructor
  // Creates an empty SequenceReaderLocal
  SequenceReaderLocal
  ( int wordLength, int bitsPerSymbol, ostream& monitoringStream = cerr );

  // Function Name: Copy constructor
  // Arguments:
  // NB This is potentially slow. 
  SequenceReaderLocal( const SequenceReaderLocal& rhs );


  // Function Name: Destructor
  // Arguments:
  ~SequenceReaderLocal(); 
  // (NB destructor should be virtual if class is to be derived from)

  // Manipulator Functions
  virtual SequenceReader* clone( void ) 
  { return new SequenceReaderLocal( *this ); }

  // Function Name:
  // Arguments:
  // TYPE  NAME  IN/OUT COMMENT
  // Returns: TYPE COMMENT

  // Function Name: changeMode
  // Arguments: const SequenceReaderMode&
  // Makes a copy of mode and uses it to handle mismatch character reads
  // Does nothing here because any character reading necessary will have 
  // been done by the SequenceReader this class is constructed from 
  virtual void changeMode( SequenceReaderMode* pMode ) {}

  // Accessor Functions
  // (NB all accessor functions should be 'const')

  // Function Name: rewind
  // Arguments: void
  // Returns:   void
  // Rewind to the start of the data file, so that getNextSequence will
  // return the first sequence in the file
  virtual void rewind( void ) 
  {
    lastSequenceNumber_ = 0;
    // don't need to do anything else, because we're set up for random access
  } // ~rewind 

  // Function Name: findSequence
  // Arguments: SequenceNumber (in)
  // Returns:   void
  // Winds the input file stream to the start of sequence number seqNum. 
  // Returns false if seqNum exceeds the number of sequences in
  // the file.
  virtual bool findSequence( SequenceNumber seqNum ); 

  // Function Name: getNextSequence
  // Arguments: WordSequence& (out), int (in)
  // Returns:   int
  // Read the next set of sequence information from the file and parse it
  // into WordSequence format. Returns -1 if there has been a problem with
  // reading the sequence, else returns the number of valid base pairs 
  // contained within the final word of the sequence.
  virtual int getNextSequence( WordSequence& nextSeq, int wordLength );

  // Function Name: getSequence
  // Arguments: WordSequence& (out), SequenceNumber (in), int (in)
  // Returns:   bool
  // Read the sequenceNumber-th set of sequence information from the file and 
  // parse it into WordSequence format
  virtual int getSequence
  ( WordSequence& nextSeq, SequenceNumber sequenceNumber, int wordLength );

  // Function Name: getLastSequenceName
  // Arguments: string& (out)
  // Returns:   void
  // Fills the string with the name of the last sequence read 
  virtual void getLastSequenceName( string& seqName ) const;

  // Function Name: getBitsPerSymbol
  // Arguments: none
  // Returns:   int
  // Returns number of bits per symbol used in encoding
  virtual int getBitsPerSymbol ( void ) const 
  {
    return bitsPerSymbol_;
  }

  // Function Name: getSourceDataType
  // Arguments: none
  // Returns:   SourceDataType
  // Returns type of data being encoded (protein or DNA)
  virtual SourceDataType getSourceDataType( void ) const
  {
    return sourceData_;
  }

  // Function Name: printName
  // Arguments: string& (out), SequenceNumber (in)
  // Returns:   void
  // Fills a string with the name of the requested sequence
  virtual bool printName( ostream& os, SequenceNumber seqNum );

  // Function Name: printSideInfo
  // Arguments: string& (out), SequenceNumber (in)
  // Returns:   void
  // Fills a string with the name of the requested sequence
  virtual bool printSideInfo( ostream& os, SequenceNumber seqNum );

  // Function Name: printSource
  // Arguments: string& (out), SequenceNumber (in)
  // Returns:   void
  // Fills a string with the name of the requested sequence
  virtual bool printSource( ostream& os, SequenceNumber seqNum );

  SequenceInfo& back( void ) { return seqData_.back(); }
  void push_back( void )     { seqData_.push_back(SequenceInfo());   }
  void pop_back( void )      { seqData_.pop_back();    }
  const SequenceInfo& operator[]( vector<SequenceInfo>::size_type i ) 
  { 
    if ( i >= seqData_.size() )
      throw SSAHAException
	("Tried to access non-existent sequence in SequenceReaderLocal"); 
    else return seqData_[i]; 
  }
  


  // PROTECTED MEMBER FUNCTIONS 
  // (visible to this class and derived classes only)
  protected:

  // Function Name: computeNumSequencesInFile
  // Arguments: void
  // Returns:   SequenceNumber
  // Returns the number of sequences in the file - called by 
  // getNumSequencesInFile. This is a null function because for 
  // SequenceReaderLocal this is determined as part of the construction 
  // process.
  virtual SequenceNumber computeNumSequencesInFile( void )
  { return seqData_.size(); }

  // PRIVATE MEMBER FUNCTIONS
  // (visible to instances of this class only)
  
  private:
  SequenceReaderLocal& operator=(const SequenceReaderLocal&);   // NOT IMPLEMENTED

  // PROTECTED DATA:
  // (visible to instances of this class only)
  protected:
  vector<SequenceInfo> seqData_;

  int wordLength_;
  int bitsPerSymbol_;
  SourceDataType sourceData_;

  // PRIVATE MEMBER DATA
  private:

}; // ~class SequenceReaderLocal



// ### Function Declarations ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

// End of include guard:
#endif

// End of file SequenceReaderLocal.h





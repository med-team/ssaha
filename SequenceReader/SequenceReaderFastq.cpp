
// #######################################################################

// SSAHA : Sequence Search and Alignment by Hashing Algorithm
// Version 3.2, released 1st March 2004
// Copyright (c) Genome Research 2002

// SSAHA is free software; you can redistribute it and/or modify 
// it under the terms of version 2 of the GNU General Public Licence
// as published by the Free Software Foundation.
 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public Licence for more details.
 
// You should have received a copy of the GNU General Public Licence
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// or see the on-line version at http://www.gnu.org/copyleft/gpl.txt

// #######################################################################

// Module Name  : SequenceReaderFastq
// File Name    : SequenceReaderFastq.cpp
// Language     : C++
// Module Author: Anthony J. Cox (ac2@sanger.ac.uk)

// Description:

// Includes:

#include "SequenceReader.h"
#include "SequenceReaderFastq.h"
#include "SequenceEncoder.h"
#include <strstream>
#include <fstream>

// ### Function Definitions ###

// Name:
// Arguments:
// TYPE  NAME  IN/OUT COMMENT
// Returns: TYPE COMMENT

SequenceReaderFastq::SequenceReaderFastq
( const char* fileName, 
  SequenceEncoder* pEncoder,
  ostream& monitoringStream  ) : 
SequenceReaderFile
( fileName, '@', '+', pEncoder->clone(), monitoringStream )
{
  monitoringStream_ << "constructing SequenceReaderFastq" << endl;
} // ~constructor

SequenceReaderFastq::SequenceReaderFastq
( const char* fileName, ostream& monitoringStream ) : 
SequenceReaderFile
( fileName, '@', '+', new SequenceEncoderDNA(12), monitoringStream )
{
  monitoringStream_ << "constructing SequenceReaderFastq" << endl;
} // ~constructor

SequenceReaderFastq::SequenceReaderFastq( const SequenceReaderFastq& rhs):
SequenceReaderFile((SequenceReaderFile)rhs)
{
  monitoringStream_ << "copy constructing SequenceReaderFastq" << endl;
}




SequenceReaderFastq::~SequenceReaderFastq()
{
  monitoringStream_ << "destructing SequenceReaderFastq" << endl;
} // ~destructor

SequenceReaderFastqProtein::SequenceReaderFastqProtein
( const char* fileName, ostream& monitoringStream ) :
SequenceReaderFastq
( fileName, new SequenceEncoderProtein(5), monitoringStream )
{
  monitoringStream_ << "constructing SequenceReaderFastqProtein" << endl;
} // ~constructor



  // Function Name: getNextSequence
  // Arguments: WordSequence& (out), int (in)
  // Returns:   bool
  // Read the next set of sequence information from the file and parse it
  // into WordSequence format
  int SequenceReaderFastq::getNextSequence
  ( WordSequence& nextSeq, int wordLength )
  {
    DEBUG_L2( "SequenceReaderFastq::getNextSequence" );
    if (SequenceReaderFile::getNextSequence( nextSeq, wordLength ) == -1 )
    {
      return -1;
    } // ~if

    // Now spool past the quality info 
    
    //    char firstOfLine;
    // `Interesting' standard library quirk: even though we are exclusively
    // reading chars firstOfLine must be an int (cos EOF is an int not a char)
    int firstOfLine;
    while (1==1)
    {
      firstOfLine = pInputFileStream_->peek();
      if (    ( firstOfLine == EOF ) 
           || ( (char)firstOfLine == seqStartChar_)) 
      {      
	return nextSeq.getNumBasesInLast();
      } // ~if 
      pInputFileStream_->getline( inputBuffer_, inputBufferSize_, '\n' );

    } // ~while
    return nextSeq.getNumBasesInLast(); 

  } // ~SequenceReaderFastq::getNextSequence




// End of file SequenceReaderFastq.cpp




